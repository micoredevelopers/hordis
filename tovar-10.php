<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Автоматика для въездных ворот
            </p>
            <p class="goods_k_container_box_desc mb-1">
              Компания Hörmann, уже давно занимает лидирующие позиции в Европе
              на рынке дверных автоматизированных систем. Приводы для въездных
              ворот не только отличаются надежностью, безопасностью и
              долговечностью, но и максимально доступны в управлении.
              Инновационные технологии Hörmann обеспечивают:
            </p>
            <ul class="pl-4">
              <li class="goods_k_container_box_desc  mb-1">
                - плавное и безопасное закрытие и открытие въездных ворот;
              </li>
              <li class="goods_k_container_box_desc  mb-1">
                - автоматическую остановку, при распознавании малейшего
                препятствия;
              </li>
              <li class="goods_k_container_box_desc  mb-1">
                - надежную блокировку и защиту от взлома;
              </li>
              <li class="goods_k_container_box_desc  mb-1">
                - автоматическое закрытие или открытие ворот, в точно
                установленное время;
              </li>
              <li class="goods_k_container_box_desc  mb-1">
                - дистанционное управление и контроль с любой точки земного
                шара;
              </li>
              <li class="goods_k_container_box_desc  ">
                - частичное открытие, если нет необходимости заезда автомобиля.
              </li>
            </ul>
          </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/outter/main.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-lg-12 col-12">
            <p class="goods_k_container_box_desc">
              Кроме того, компания предоставляет возможность самостоятельно
              выбирать отдельные функции для комплектации привода. Например -
              RotaMatic и VersaMatic, можно заказать с энергосберегающим
              аккумулятором и дополнительной солнечной батареей. Hörmann
              обеспечивает идеальный комфорт и безопасность своих клиентов,
              благодаря автоматическим приводам для въездных ворот. На
              сегодняшний день, компания является абсолютным гарантом качества и
              надежности.
              <br />
              <br />
              Автоматика для въездных ворот Hörmann в Одессе, есть в наличии в
              открытом ШОУ-РУМе, компании “Hördis”.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog/PrivodyVjezdniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Привод распашных ворот RotaMatic
            </p>
            <p class="goods_k_container_box_desc">
              Мощный двигатель распашных ворот RotaMatic выполнен в лучшем стиле
              Hörmann. Отменное качество в сочетании с профессиональным
              функционалом и элегантным дизайном, делает привод практически
              совершенным. RotaMatic выпускается со встроенным блоком управления
              и радиоуправляемой платой, что позволяет контролировать систему не
              только с помощью пульта ДУ, а и смартфона или планшета. Hörmann
              также решает проблему - отсутствия подключения к электросети с
              помощью дистанционного аккумулятора привода RotaMatic Akku.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/outter/rota-matic.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container  row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/outter/versa-matic.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Привод распашных ворот VersaMatic
            </p>
            <p class="goods_k_container_box_desc">
              Современный привод VersaMatic специально разработан компанией
              Hörmann для нестандартных монтажных решений установки въездных
              ворот. Он с легкостью крепится на стойку ворот что избавляет от
              необходимости устанавливать дополнительные устройства. Механизм
              предусматривает три различных типа монтажа, которые позволяют
              VersaMatic открывать ворота под углом до 130 °. Это дает
              возможность подобрать максимально подходящий вариант для
              индивидуального стиля и дизайна въездных ворот. Привод оснащен
              всеми высокотехнологичными инновациями немецкого производителя. С
              его помощью, скоростью дверной системы управлять легко и
              комфортно, тяжелые ворота он открывает плавно и тихо. На случай
              отсутствия доступа к электросети Hörmann предусмотрела аккумулятор
              VersaMatic Akku, который можно заказать с дистанционной солнечной
              батареей.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Привода откатных ворот LineaMatic
            </p>
            <p class="goods_k_container_box_desc">
              Привод LineaMatic предназначен для максимально комфортного
              управления раздвижными воротами. Он оснащен всеми
              высокотехнологичными атрибутами продукции Hörmann. Особенностью
              LineaMatic является его уникальный оцинкованный корпус, высоту
              которого можно регулировать.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/outter/linea-matic.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/outter/sta.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Привод откатных ворот STA 400
            </p>
            <p class="goods_k_container_box_desc">
              Привод STA 400 производится специально для управления мощных
              откатных ворот большого размера. Его уникальное программное
              обеспечение B460 FU позволяет открывать и закрывать массивные
              ворота без рывков, плавно и максимально безопасно.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="sales_k">
      <div class="sales_k_container">
        <img src="img/sale1.jpg" alt="" class="sales_k_container_image" />
        <div class="sales_k_container_box">
          <p class="sales_k_container_box_title">Акционные предложения</p>
          <p class="sales_k_container_box_desc">
            Дом – это то место, где мы чувствуем себя в безопасности. Ворота и
            двери от № 1 в Европе по выгодной цене: сэкономьте до 30%!
          </p>
          <a href="stock.php" class="sales_k_container_box_btn">Посмотреть</a>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Сертифицированная радиосистема BiSecur для безопасности.
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_BiSecur_f7235aad97.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Радиосистема BiSecur – это новаторская технология. Она создана
                для высоко-комфортного удобства контроля автоматических
                механизмов гаражных и въездных ворот, освещения, дверей и
                другого функционала общей системы. Эта разработка компании
                Hörman прошла тестирования и в итоге - сертифицирована, что дает
                чувство уверенности и безопасности с данной системой.
                Радиосигнал не поддается чтению и скопирован другими
                устройствами - быть не может.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                И снова об уникальной радиосистеме
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/guter_grund_perfekt_abgestimmt_400x280.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Все устройства радиосистемы BiSecur абсолютно совместимы. С
                помощью пульта ДУ легко и просто управлять приводами дверей,
                гаражными и въездными воротами, которые оборудованы приводами
                Hörmann. <br />
                Большим плюсом радиосистемы BiSecur есть возможность контроля
                входной двери, либо гаражными, а также въездными воротами, через
                смартфон или планшет.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Функция запроса о положении ворот, без отвлечений
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_Abfrage_08c43643f9.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Современный комфорт предполагает наименьшее отвлечение от
                основных занятий. Достаточно использовать пару клавиш на пульте
                HS 5 BS, и по cветодиоду, а именно по его цвету - можно узнать в
                каком положении ворота. Более простого управления воротами –
                просто нет!
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Стильный дизайн</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_Design_b6a558976c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Оригинальные пульты ДУ BiSecur имеют не только красивый
                благородный черный и белый цвет, но и имеют элегантную форму. Он
                выглядит не просто банально, а говорит об изысканном вкусе
                владельца.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Сертифицированы</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_Zertifiziert_d571060c5c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Все дверные системы компании Hörmann прежде, чем выпустится -
                проходят испытания и сертифицируются специальными независимыми
                организациями. Приводы въездных ворот отвечают всем требованиям
                европейских институтов качества и удостоены сертификата
                стандарта DIN EN 13241-1.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Безопасность</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_USP_Abschaltautomatik_Einfahrtstorantrieb_6129d2e1c4.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Запатентованная уникальная продукция Hörmann, отличается на
                рынке своей надежностью и безопасностью. При возникновении
                малейшего препятствия, привод останавливает ворота в любой
                рабочей фазе. Гарантию автоматической мгновенной остановки,
                обеспечивает дополнительная установка световых барьеров, которые
                фиксируют расположение предметов или людей в опасной зоне
                закрытия дверей. Они легко монтируются в предохранительные
                дверные стойки при въезде, элегантно выглядят и максимально
                обеспечивают надежную, не заключающую в себе опасности, работу
                въездных дверей.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Надежность</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_USP_zuverlaessig_Sanft_Anlauf_58967a19b6.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Автоматические приводы Hörmann созданы, чтобы обеспечить
                максимально комфортную и безопасную работу въездных ворот.
                Благодаря высочайшему качеству ворота открываются и закрываются
                практически бесшумно, не нарушая спокойствия соседей. С
                приводами Hörmann въездные ворота двигаются плавно, без рывков,
                что положительно сказывается на сохранности механизмов, и
                основательно продлевает срок эксплуатации.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Прочность</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/inner/advantages/csm_USP_Robust_hochwertige_Qualitaet_3be3bd598c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Выдерживать климатические нагрузки и неблагоприятные погодные
                условия приводам ворот Hörmann, помогает превосходный материал и
                безупречное качество. Благодаря надежности и качеству материала,
                приводы демонстрируют отличную прочность и долговечность. Для
                наиболее суровых регионов с холодным климатом подойдет RotaMatic
                – привод с подогревом.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/s72U23NHptA/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Надежно, удобно, эксклюзивно: радиосистема BiSecur компании
              Хёрманн
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/k8lXfJWSgmg/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Привод гаражных ворот SupraMatic – Дуэль
            </p>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <!--modal window-->

       
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
