<?php
class Mail
{
    private $data = ['send_charset' => 'utf-8', 'data_charset' => 'utf-8'];

    public function __construct()
    {
    }

    public static function getObject($registry = [])
    {
        return new self();
    }

    /**
     * @param $name
     * @return $this
     * имя отправителя
     */
    public function setNameFrom($name)
    {
        $this->data['name_from'] = $name;

        return $this;
    }

    /**
     * @param $email
     * @return $this
     * email отправителя
     */
    public function setEmailFrom($email)
    {
        $this->data['email_from'] = $email;

        return $this;
    }

    /**
     * @param $nameTo
     * @return $this
     * имя получателя
     */
    public function setNameTo($nameTo)
    {
        $this->data['name_to'] = $nameTo;

        return $this;
    }

    /**
     * @param $emailTo
     * @return $this
     * email получателя
     */
    public function setEmailTo($emailTo)
    {
        $this->data['email_to'] = $emailTo;

        return $this;
    }

    /**
     * @param string $charset
     * @return $this
     * кодировка переданных данных
     */
    public function setDataCharset($charset = 'utf-8')
    {
        $this->data['data_charset'] = $charset;

        return $this;
    }

    /**
     * @param string $charset
     * @return $this
     * кодировка письма
     */
    public function setSendCharset($charset = 'utf-8')
    {
        $this->data['send_charset'] = $charset;

        return $this;
    }

    /**
     * @param $subject
     * @return $this
     *  тема письма
     */
    public function setSubject($subject)
    {
        $this->data['subject'] = $subject;

        return $this;
    }

    /**
     * @param $body
     * @return $this
     *  текст письма
     */
    public function setBody($body)
    {
        $this->data['body'] = $body;

        return $this;
    }

    /**
     * @return bool
     * Отправка письма
     */
    public function sendMail($dumpAdmin = false)
    {
        $return = Mail::send(
            $this->data['name_from'],
            $this->data['email_from'],
            $this->data['name_to'],
            $this->data['email_to'],
            $this->data['data_charset'],
            $this->data['send_charset'],
            $this->data['subject'],
            $this->data['body']
        );
        $this->data = ['send_charset' => 'utf-8', 'data_charset' => 'utf-8'];

        return $return;
    }

    static function send($name_from,// имя отправителя
                         $email_from,// email отправителя
                         $name_to,// имя получателя
                         $email_to,// email получателя
                         $data_charset,// кодировка переданных данных
                         $send_charset,// кодировка письма
                         $subject,// тема письма
                         $body // текст письма
    )
    {
        $email_to = str_replace("&#044;", ",", $email_to);
        $email_cnt = explode(",", $email_to);
        $email_to = "";
        for ($i = 0; $i <= count($email_cnt) - 1; $i++) {
            if ($i != 0) $email_to .= ",";
            $email_to .= "< {$email_cnt[$i]} >";
        }
        $to = Mail::mime_header_encode($name_to, $data_charset, $send_charset) . $email_to;
        $subject = Mail::mime_header_encode($subject, $data_charset, $send_charset);
        $from = Mail::mime_header_encode($name_from, $data_charset, $send_charset) . ' <' . $email_from . '>';
        if ($data_charset != $send_charset) $body = iconv($data_charset, $send_charset, $body);
        $headers = "From: $from\r\nReply-To: $from\r\nContent-type: text/html;charset=$send_charset\r\n";

        return mail($to, $subject, $body, $headers, "-f info@" . $_SERVER['HTTP_HOST']);
    }

    static function mime_header_encode($str, $data_charset, $send_charset)
    {
        if ($data_charset != $send_charset) $str = iconv($data_charset, $send_charset, $str);

        return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
    }

    static function errorMail($text)
    {
        $contact_mail = email_error;
        $url = (string)$_SERVER['REQUEST_URI'];
        $refer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER']: '';
        $ip_user = (string)$_SERVER['REMOTE_ADDR'];
        $br_user = (string)$_SERVER['HTTP_USER_AGENT'];
        $header = "From: $contact_mail\r\nReply-To: $contact_mail\r\nReturn-Path: $contact_mail\r\nContent-type: text/plain;charset=UTF-8";
        $subject = 'Отладка ошибок в системе SkyCms:' . $_SERVER['SERVER_NAME'] . ' ' . $_SERVER['HTTP_HOST'];
        $body = "SERVER_NAME:" . (string)$_SERVER['SERVER_NAME'] . "страница: $url \nREFER страница: $refer \nIP пользователя: $ip_user \nбраузер пользователя: $br_user \n----------------------------------------- \n$text";
        mail($contact_mail, $subject, $body, $header);
    }
}

header('Content-Type: text/html; charset=utf-8');

$name = trim($_POST["name"]);
$phone = trim($_POST["phone"]);
$mail = isset($_POST["email"]) ? trim($_POST["email"]) : '';
$desc = isset($_POST["desc"]) ? trim($_POST["desc"]) : '';
$form_name = isset($_POST["form_name"]) ? trim($_POST["form_name"]) : 'Обратная связь';

$resmsg = '';
$status = 'error';
$send_mail = new Mail();
$send_mail->setNameFrom('Инфо центр');
$send_mail->setEmailFrom('info@'.$_SERVER['HTTP_HOST']);
$send_mail->setNameTo($name);
$send_mail->setEmailTo( 'office.hordis@gmail.com');
$send_mail->setSubject($form_name);
$message = "<h2> Имя: ".$name."</h2><h2>Телефон: ".$phone."</h2>";
if ($mail) {
    $message .= "<h2>Почта: ".$mail."</h2>";
}
if ($desc){
    $message .= "<h2>Описание: ".$desc."</h2>" ;
}
$send_mail->setBody($message);
try{


    if($send_mail->sendMail()){
        $status = 'success';
        $resmsg = 'Сообщение успешно отправлено';
    }
} catch (Exception $e){
    $resmsg = "Ошибка!";
}
$data = [
    'message' => $resmsg,
    'status' => $status
];
header('Content-Type: application/json');
echo json_encode($data);

