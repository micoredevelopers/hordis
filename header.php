

<!--navbar section-->
<section>
    <nav class="navbar navbar-expand-xl my-navbar fixed-top bg-white">
        <a class="navbar-brand" href="index.php"><img class="navbar-brand-img" src="img/logo_svg.svg"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse header-menu flex-md-wrap justify-content-md-center "
             id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown-position">
                    <div class="menu-link-wrap">
                        <a href="category.php" class="nav-link my-nav-link menu-link">Частные строения</a>
                        <nav class="my-nav-link-btn ">
                            <span></span>
                            <span></span>
                            <span></span>
                        </nav>
                    </div>
                    <div class="drop-menu-wrap container">
                        <nav class="drop-menu left_drop">
                            <a id="1" href="pod-category.php" class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/0_1 Гаражные вороты.svg" alt="">
                                Гаражные ворота
                            </a>
                            <a id="2" href="pod-category-1.php" class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/0_2 Двери.svg" alt="">
                                Двери
                            </a>
                            <a id="3" href="pod-category-2.php"
                               class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/0_3 Приводы.svg" alt="">
                                Приводы
                            </a>
                        </nav>
                        <div class="sub-menu-wrap">
                            <nav class="sub-menu drop-menu">
                                <a data-id="1" href="tovar.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/1_1%20Гаражные%20секционные%20ворота.svg" alt="">
                                    Гаражные секционные ворота
                                </a>
                                <a data-id="1" href="tovar-1.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/1_2%20Гаражные%20рулонные%20ворота%20RollMatic.svg" alt="">
                                    Гаражные рулонные ворота RollMatic
                                </a>
                                <a data-id="1" href="tovar-2.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/1_3%20Гаражные%20боковые%20двери.svg" alt="">
                                    Гаражные боковые двери
                                </a>
                            </nav>
                            <nav class="sub-menu drop-menu">
                                <a data-id="2" href="tovar-3.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/2_1%20Входные%20двери%20Thermo65%20и%20ThermoPro.svg" alt="">
                                    Входные двери Thermo65 и ThermoPro
                                </a>
                                <a data-id="2" href="tovar-4.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/2_2 Входные двери ThermoSafe и ThermoCarbon.svg" alt="">
                                    Входные двери ThermoSafe и ThermoCarbon
                                </a>
                                <a data-id="2" href="tovar-5.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/2_3 Межкомнатные двери.svg" alt="">
                                    Межкомнатные двери
                                </a>
                                <a data-id="2" href="tovar-6.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/FireResistance.svg" alt="">
                                    Огнестойкие/Дымозащитные двери
                                </a>
                                <!--<a data-id="2" href="tovar-7.php" class="drop-menu-link door">-->
                                <!--<img class="svg" src="img/menu-icons/2_5 Звукоизоляционные двери.svg" alt="">-->
                                <!--Звукоизоляционные двери-->
                                <!--</a>-->
                                <a data-id="2" href="tovar-8.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/2_6 Внутренние двери ZK.svg" alt="">
                                    Внутренние двери ZK
                                </a>
                            </nav>
                            <nav class="sub-menu drop-menu">
                                <a data-id="3" href="tovar-9.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/3_1 Гаражных ворот.svg" alt="">
                                    Для гаражных ворот
                                </a>
                                <a data-id="3" href="tovar-10.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/3_2 Въездные ворота.svg" alt="">
                                    Для въездных ворот
                                </a>
                                <a data-id="3" href="tovar-11.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/InnerDoors.svg" alt="">
                                    Для внутренних дверей
                                </a>
                            </nav>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown-position">
                    <div class="menu-link-wrap">
                        <a href="category-prom.php" class="nav-link my-nav-link menu-link">Промышленность
                        </a>
                        <nav class="my-nav-link-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </nav>
                    </div>

                    <div class="drop-menu-wrap container">
                        <nav class="drop-menu">
                            <a id="4" href="pod-category-3.php" class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/4_1%20Промышленные%20секционные%20ворота.svg" alt="">
                                Промышленные ворота
                            </a>
                            <a id="5" href="pod-category-4.php" class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/0_2%20Двери.svg" alt="">
                                Двери
                            </a>
                            <a id="6" href="pod-category-5.php" class="drop-menu-item drop-menu-link door menu-line">
                                <img class="svg" src="img/menu-icons/0_3%20Приводы.svg" alt="">
                                Приводы
                            </a>
                        </nav>
                        <div class="sub-menu-wrap">
                            <nav class="sub-menu drop-menu">
                                <a data-id="4" href="tovar-12.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/4_1%20Промышленные%20секционные%20ворота.svg" alt="">
                                    Промышленные секционные ворота
                                </a>
                                <a data-id="4" href="tovar-13.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/4_2%20Скоростные%20ворота.svg" alt="">
                                    Скоростные ворота
                                </a>
                                <a data-id="4" href="tovar-14.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/4_3%20Рулонные%20ворота%20и%20рулонные%20решетки.svg" alt="">
                                    Рулонные ворота и рулонные решетки
                                </a>
                                <a data-id="4" href="tovar-15.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/4_4%20Перегрузочное%20оборудование.svg" alt="">
                                    Перегрузочное оборудование
                                </a>
                            </nav>
                            <nav class="sub-menu drop-menu">
                                <a data-id="5" href="tovar-16.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/5_1_FireResistance.svg" alt="">
                                    Огнестойкие/Дымозащитные двери
                                </a>
                                <!--<a data-id="5" href="tovar-17.php" class="drop-menu-link door">-->
                                <!--<img class="svg" src="img/menu-icons/5_2%20Звукоизоляционные%20двери.svg" alt="">-->
                                <!--Звукоизоляционные двери-->
                                <!--</a>-->
                                <a data-id="5" href="tovar-18.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/5_3%20Внутренние%20двери%20ZK.svg" alt="">
                                    Внутренние двери ZK
                                </a>
                            </nav>
                            <nav class="sub-menu drop-menu">
                                <a data-id="6" href="tovar-19.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/6_1%20Гаражных%20ворот.svg" alt="">
                                    Для гаражных ворот
                                </a>
                                <a data-id="6" href="tovar-20.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/6_2%20Въездные%20ворота.svg" alt="">
                                    Для въездных ворот
                                </a>
                                <a data-id="6" href="tovar-21.php" class="drop-menu-link door">
                                    <img class="svg" src="img/menu-icons/6_3_InnerDoors.svg" alt="">
                                    Для внутренних дверей
                                </a>
                            </nav>
                        </div>
                    </div>
                </li>
                <li class="nav-item ">
                    <a class="nav-link  menu-link" href="stock.php">Акции</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link  menu-link" href="all-news.php">Новости</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link  menu-link" href="about-us.php">О нас</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link  menu-link" href="gallery.php">Галерея</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link menu-link" href="contact.php">Контакты</a>
                </li>
            </ul>
            <div class="header-link-wrap">
                <a href="tel:+38 (096) 320 12 99" class="call-link menu-link"> +38 (096) 320 12 99</a>
                <!--<a href="#" class="leng-link menu-link">RU</a>-->
            </div>
        </div>
    </nav>
</section>
<!--navbar mobile section-->
<section>
    <nav class="navbar navbar-expand-xl navbar-light bg-white fixed-top my-navbar-mob">
        <div class="my-navbar-mob-link-wrap">
            <button class="navbar-toggler main-menu-btn-wrap" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="Toggle navigation">
                        <span class="mob-sub-menu-button main-menu-btn ">
                    <span></span>
                    <span></span>
                </span>
            </button>
            <a class="navbar-brand align-self-sm-center navbar-brand-mob" href="index.php">
                <img class="navbar-brand-img" src="img/logo_svg.svg">
            </a>
            <a  href="tel:+38 (096) 320 12 99">
                <img src="img/phone-icone.svg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item mob-menu-item">
                    <div class="supper-button-wrap">
                        <a href="category.php" class="nav-link mob-menu-link">Частные строения</a>
                        <nav class="mob-sub-menu-button supper-btn">
                            <span></span>
                            <span></span>
                        </nav>
                    </div>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category.php" class="nav-link mob-menu-link mar-30">Гаражные ворота</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                                секционные ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-1.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                                рулонные ворота RollMatic</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-2.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                                боковые двери</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-1.php" class="nav-link mob-menu-link mar-30">Двери</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-3.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Входные
                                двери Thermo65 и ThermoPro</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-4.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Входные
                                двери ThermoSafe и ThermoCarbon</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-5.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Межкомнатные
                                двери</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-6.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Огнестойкие/Дымозащитные
                                двери</a></li>
                        <!--<li class="mob-drop-menu-item">-->
                        <!--<a href="tovar-7.php" class="nav-link mob-menu-link mob-drop-menu-link">-->
                        <!--Звукоизоляционные двери-->
                        <!--</a>-->
                        <!--</li>-->
                        <li class="mob-drop-menu-item"><a href="tovar-8.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Внутренние
                                двери ZK</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-2.php" class="nav-link mob-menu-link mar-30">Приводы</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-9.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для гаражных
                                ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-10.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для въездные
                                ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-11.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для внутренних
                                дверей</a></li>
                    </ul>
                </li>
                <li class="nav-item mob-menu-item">
                    <div class="supper-button-wrap">
                        <a href="category-prom.php" class="nav-link mob-menu-link">Промышленные</a>
                        <nav class="mob-sub-menu-button supper-btn">
                            <span></span>
                            <span></span>
                        </nav>
                    </div>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-3.php" class="nav-link mob-menu-link mar-30">Ворота</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-12.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Промышленные
                                секционные ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-13.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Скоростные
                                ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-14.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Рулонные
                                ворота и рулонные решетки</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-15.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Перегрузочное
                                оборудование</a>
                        </li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-4.php" class="nav-link mob-menu-link mar-30">Двери</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-16.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Огнестойкие/Дымозащитные
                                двери</a></li>
                        <!--<li class="mob-drop-menu-item"><a href="tovar-17.php"-->
                        <!--class="nav-link mob-menu-link mob-drop-menu-link">Звукоизоляционные-->
                        <!--двери</a></li>-->
                        <li class="mob-drop-menu-item"><a href="tovar-18.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Внутренние
                                двери ZK</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-5.php" class="nav-link mob-menu-link mar-30">Приводы</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-19.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для гаражных
                                ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-20.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для въездных
                                ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-21.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для внутренних
                                дверей</a></li>
                    </ul>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="stock.php">Акции</a></li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="all-news.php">Новости</a>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="about-us.php">О нас</a></li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="gallery.php">Галерея</a>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="contact.php">Контакты и
                        сервис</a></li>
            </ul>
            <!--<a href="#" class="leng-link-mob">English</a>-->
        </div>
    </nav>
    <div class="alert alert-success" role="alert">
        <p class="text-center">Ваша заявка отправлена</p>
    </div>
</section>
