<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Автоматика для гаражных ворот
            </p>
            <p class="goods_k_container_box_desc">
              Комфорт и безопасность для Вашего дома. <br />
              <br />
              Компания Hörmann уже много лет заботится о максимальном комфорте и
              безопасности своих потребителей. Необходимость выходить из машины
              или дома, чтобы открыть гараж, практически исчезла, благодаря
              новаторской системе - автоматического функционала ворот.
              Электрический привод дает возможность без особого усилия управлять
              гаражными воротами, с помощью практичного и многофункционального
              дистанционного пульта. Достоинства автоматических дверных систем
              Hörmann прекрасно демонстрируют приводы SupraMatic, ProMatic, и не
              требующий стационарного подключения к сети привод ProMatic Akku.
            </p>
            </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/garage/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/garage/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/drives/garage/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/drives/garage/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-lg-12 mt-3 col-12">
            Все приводы оснащены радиоуправляемой системой BiSecur, которая
            гарантирует постоянный контроль и надежную обратную связь.
            Благодаря инновационным технологиям - кодировка BiSecur позволяет
            без помех, на протяжении всего дня - следить за состоянием ворот и
            быть уверенным в безопасности своего дома. Дистанционные пульты
            отличаются высокой функциональностью, удобством и оригинальным
            дизайном. <br />
            <br />
            Ворота немецкой компании также оснащены дополнительным шлюзом
            Gateway BiSecur. Он позволяет легко контролировать и управлять
            дверной системой, находясь в любой точке города, с помощью
            мобильного телефона или планшета - взглянув на монитор в любое
            время суток, можно узнать открыты или закрыты ворота. Продуманные
            технологии Hörmann упрощают жизнь до мелочей, даря ощущение
            комфорта, безопасности и безграничной свободы.
            <br />
            <br />
            Более внимательно рассмотреть автоматику для гаражных ворот
            Hörmann в Одессе - можно в открытом ШОУ-РУМе, компании “Hördis”.
          </p>
          <div class="box_btns mt-3">
            <a id="go" href="#" class="box_btns_item">Узнать цену</a>
            <a href="./catalog/PrivodyGarajnye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
          </div>
        </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/garage/supramatic.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">Supra Matic</p>
            <p class="goods_k_container_box_desc">
              Мощный привод от компании Hörmann для тех, кто ценит комфорт,
              скорость и пунктуальность. Секционные ворота с помощью SupraMatic
              открываются почти вдвое быстрее. Современная комплектация и
              программирование привода также позволяют регулировать
              дополнительную высоту ворот, оставляя гараж слегка открытым для
              безопасного проветривания. Встроенная подсветка очень удобна,
              автономна и легко управляется с помощью пульта ДУ. Кроме
              качественного профессионального функционала, привод SupraMatic,
              имеет сдержанный, элегантный дизайн, что делает его еще более
              привлекательным и востребованным.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">ProMatic</p>
            <p class="goods_k_container_box_desc">
              ProMatic имеет полный арсенал преимуществ электроприводов Hörmann:
              дистанционное управление, регулируемая подсветка гаража,
              противовзломная защита, функция ускоренного открывания и прочее.
              Главное превосходство ProMatic Akku – возможность подключения
              дистанционного блока питания. Отсутствие электросети в гараже
              легко решается с помощью переносного аккумулятора, который
              заряжается за пару часов, даже от автономной солнечной батареи.
              Привод имеет безупречный дизайн, и универсальную
              высококачественную фурнитуру подходящую практически для всех
              гаражных секционных систем.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/garage/promatic.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Радиосистема BiSecur с сертифицированной безопасностью
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_BiSecur_f7235aad97.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Радиосистема BiSecur – это новаторская технология. Она создана
                для высоко-комфортного удобства контроля автоматических
                механизмов гаражных и въездных ворот, освещения, дверей и
                другого функционала общей системы.
                <br />
                Эта разработка компании Hörman прошла тестирования и в итоге -
                сертифицирована, что дает чувство уверенности и безопасности с
                данной системой. Радиосигнал не поддается чтению и скопирован
                другими устройствами - быть не может.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                И снова об уникальной радиосистеме
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/guter_grund_perfekt_abgestimmt_400x280.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Все устройства радиосистемы BiSecur абсолютно совместимы. С
                помощью пульта ДУ легко и просто управлять приводами дверей,
                гаражными и въездными воротами, которые оборудованы приводами
                Hörmann.
                <br />
                Большим плюсом радиосистемы BiSecur есть возможность контроля
                входной двери, либо гаражными, а также въездными воротами, через
                смартфон или планшет.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Функция запроса о положении ворот, без отвлечений
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_Abfrage_08c43643f9.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Современный комфорт предполагает наименьшее отвлечение от
                основных занятий.
                <br />
                Достаточно использовать пару клавиш на пульте HS 5 BS, и по
                cветодиоду, а именно по его цвету - можно узнать в каком
                положении ворота. Более простого управления воротами – просто
                нет!
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Стильный дизайн</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_Design_b6a558976c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Оригинальные пульты ДУ BiSecur имеют не только красивый
                благородный черный и белый цвет, но и имеют элегантную форму. Он
                выглядит не просто банально, а говорит об изысканном вкусе
                владельца.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Безопасность</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_USP_Aufschiebesicherung_Einbrecher_7c22b56060.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Компания Hörmann на высочайшем уровне заботится о безопасности
                своих клиентов. Противовзломное устройство от подваживания
                автоматически срабатывает сразу же после закрытия дверной
                системы гаража. Кроме того, запереть ворота можно и механическим
                способом, что дает возможность - не зависеть от перебоев в
                работе электросети, и обезопасить себя от внезапных
                неприятностей. Надежность продукции Hörmann предусмотрена до
                мельчайших деталей и гарантирует максимальную профессиональную
                защиту.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Надежность</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_USP_Abschaltautomatik_444458b0a8.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Продуманная технология автоматических систем компании Hörmann
                позволяет максимально регулировать открытие или закрытие дверей.
                Электропривод дает возможность устанавливать наиболее комфортную
                индивидуальную скорость спуска и подъема ворот, а также
                отрегулировать необходимые фазы остановки механизма. При
                появлении внезапных препятствий, ворота автоматически
                останавливаются. Привод SupraMatic P, для большей безопасности,
                серийно оснащен дополнительным световым барьером. Данная функция
                возможна, как дополнительная для любой модели привода гаражных
                ворот Hörmann.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Высокая скорость открывания
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_USP_Turboschnell_3dc9356eb5.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Благодаря инновациям Hörmann, управлять гаражными воротами стало
                еще проще и быстрее. Автоматическая система привода SupraMatic
                дает возможность открыть дверную систему гаража почти вдвое
                быстрее, и избавится от любого рода неудобств, связанных с
                очередями. SupraMatic-однозначно лидер по скорости работы в
                сравнении с другими приводами. Управлять дверными системами еще
                никогда не было так просто и комфортно.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Сертифицированы</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/garage/advantages/csm_Zertifiziert_d571060c5c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Все элементы автоматических дверных систем и приводов Hörmann
                разрабатываются на собственных высокоспециализированных
                предприятиях и полностью согласованы. Механизмы прошли
                сертификацию известных независимых компаний специализирующихся в
                сфере безопасности.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

    <?php include "footer.php"?>

       
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?><div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
