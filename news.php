<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Главная</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
    <!--navbar section-->
    <section>
      <nav class="grey_bg navbar navbar-expand-xl my-navbar fixed-top bg-white">
        <a class="navbar-brand" href="index.php"
          ><img class="navbar-brand-img" src="img/logo_svg.svg"
        /></a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="collapse navbar-collapse header-menu flex-md-wrap justify-content-md-center "
          id="navbarSupportedContent"
        >
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown-position">
              <div class="menu-link-wrap">
                <a href="category.php" class="nav-link my-nav-link menu-link"
                  >Частные строения</a
                >
                <nav class="my-nav-link-btn ">
                  <span></span>
                  <span></span>
                  <span></span>
                </nav>
              </div>

              <div class="drop-menu-wrap container">
                <nav class="drop-menu left_drop">
                  <a
                    id="1"
                    href="pod-category.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/0_1 Гаражные вороты.svg"
                      alt=""
                    />
                    Гаражные ворота
                  </a>
                  <a
                    id="2"
                    href="pod-category-1.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/0_2 Двери.svg"
                      alt=""
                    />
                    Двери
                  </a>
                  <a
                    id="3"
                    href="pod-category-2.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/0_3 Приводы.svg"
                      alt=""
                    />
                    Приводы
                  </a>
                </nav>
                <div class="sub-menu-wrap">
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="1"
                      href="tovar.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/1_1%20Гаражные%20секционные%20ворота.svg"
                        alt=""
                      />
                      Гаражные секционные ворота
                    </a>
                    <a
                      data-id="1"
                      href="tovar-1.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/1_2%20Гаражные%20рулонные%20ворота%20RollMatic.svg"
                        alt=""
                      />
                      Гаражные рулонные ворота RollMatic
                    </a>
                    <a
                      data-id="1"
                      href="tovar-2.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/1_3%20Гаражные%20боковые%20двери.svg"
                        alt=""
                      />
                      Гаражные боковые двери
                    </a>
                  </nav>
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="2"
                      href="tovar-3.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/2_1%20Входные%20двери%20Thermo65%20и%20ThermoPro.svg"
                        alt=""
                      />
                      Входные двери Thermo65 и ThermoPro
                    </a>
                    <a
                      data-id="2"
                      href="tovar-4.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/2_2 Входные двери ThermoSafe и ThermoCarbon.svg"
                        alt=""
                      />
                      Входные двери ThermoSafe и ThermoCarbon
                    </a>
                    <a
                      data-id="2"
                      href="tovar-5.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/2_3 Межкомнатные двери.svg"
                        alt=""
                      />
                      Межкомнатные двери
                    </a>
                    <a
                      data-id="2"
                      href="tovar-6.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/FireResistance.svg"
                        alt=""
                      />
                      Огнестойкие/Дымозащитные двери
                    </a>
                    <!--<a data-id="2" href="tovar-7.php" class="drop-menu-link door">-->
                    <!--<img class="svg" src="img/menu-icons/2_5 Звукоизоляционные двери.svg" alt="">-->
                    <!--Звукоизоляционные двери-->
                    <!--</a>-->
                    <a
                      data-id="2"
                      href="tovar-8.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/2_6 Внутренние двери ZK.svg"
                        alt=""
                      />
                      Внутренние двери ZK
                    </a>
                  </nav>
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="3"
                      href="tovar-9.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/3_1 Гаражных ворот.svg"
                        alt=""
                      />
                      Для гаражных ворот
                    </a>
                    <a
                      data-id="3"
                      href="tovar-10.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/3_2 Въездные ворота.svg"
                        alt=""
                      />
                      Для въездных ворот
                    </a>
                    <a
                      data-id="3"
                      href="tovar-11.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/InnerDoors.svg"
                        alt=""
                      />
                      Для внутренних дверей
                    </a>
                  </nav>
                </div>
              </div>
            </li>
            <li class="nav-item dropdown-position">
              <div class="menu-link-wrap">
                <a
                  href="category-prom.php"
                  class="nav-link my-nav-link menu-link"
                  >Промышленность
                </a>
                <nav class="my-nav-link-btn">
                  <span></span>
                  <span></span>
                  <span></span>
                </nav>
              </div>

              <div class="drop-menu-wrap container">
                <nav class="drop-menu">
                  <a
                    id="4"
                    href="pod-category-3.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/4_1%20Промышленные%20секционные%20ворота.svg"
                      alt=""
                    />
                    Промышленные ворота
                  </a>
                  <a
                    id="5"
                    href="pod-category-4.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/0_2%20Двери.svg"
                      alt=""
                    />
                    Двери
                  </a>
                  <a
                    id="6"
                    href="pod-category-5.php"
                    class="drop-menu-item drop-menu-link door menu-line"
                  >
                    <img
                      class="svg"
                      src="img/menu-icons/0_3%20Приводы.svg"
                      alt=""
                    />
                    Приводы
                  </a>
                </nav>
                <div class="sub-menu-wrap">
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="4"
                      href="tovar-12.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/4_1%20Промышленные%20секционные%20ворота.svg"
                        alt=""
                      />
                      Промышленные секционные ворота
                    </a>
                    <a
                      data-id="4"
                      href="tovar-13.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/4_2%20Скоростные%20ворота.svg"
                        alt=""
                      />
                      Скоростные ворота
                    </a>
                    <a
                      data-id="4"
                      href="tovar-14.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/4_3%20Рулонные%20ворота%20и%20рулонные%20решетки.svg"
                        alt=""
                      />
                      Рулонные ворота и рулонные решетки
                    </a>
                    <a
                      data-id="4"
                      href="tovar-15.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/4_4%20Перегрузочное%20оборудование.svg"
                        alt=""
                      />
                      Перегрузочное оборудование
                    </a>
                  </nav>
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="5"
                      href="tovar-16.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/5_1_FireResistance.svg"
                        alt=""
                      />
                      Огнестойкие/Дымозащитные двери
                    </a>
                    <!--<a data-id="5" href="tovar-17.php" class="drop-menu-link door">-->
                    <!--<img class="svg" src="img/menu-icons/5_2%20Звукоизоляционные%20двери.svg" alt="">-->
                    <!--Звукоизоляционные двери-->
                    <!--</a>-->
                    <a
                      data-id="5"
                      href="tovar-18.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/5_3%20Внутренние%20двери%20ZK.svg"
                        alt=""
                      />
                      Внутренние двери ZK
                    </a>
                  </nav>
                  <nav class="sub-menu drop-menu">
                    <a
                      data-id="6"
                      href="tovar-19.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/6_1%20Гаражных%20ворот.svg"
                        alt=""
                      />
                      Для гаражных ворот
                    </a>
                    <a
                      data-id="6"
                      href="tovar-20.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/6_2%20Въездные%20ворота.svg"
                        alt=""
                      />
                      Для въездных ворот
                    </a>
                    <a
                      data-id="6"
                      href="tovar-21.php"
                      class="drop-menu-link door"
                    >
                      <img
                        class="svg"
                        src="img/menu-icons/6_3_InnerDoors.svg"
                        alt=""
                      />
                      Для внутренних дверей
                    </a>
                  </nav>
                </div>
              </div>
            </li>
            <li class="nav-item ">
              <a class="nav-link  menu-link" href="stock.php">Акции</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link  menu-link" href="all-news.php">Новости</a>
            </li>
            <li class="nav-item ">
              <a class="nav-link  menu-link" href="about-us.php">О нас</a>
            </li>
            <li class="nav-item ">
              <a class="nav-link  menu-link" href="gallery.php">Галерея</a>
            </li>
            <li class="nav-item ">
              <a class="nav-link menu-link" href="contact.php">Контакты</a>
            </li>
          </ul>
          <div class="header-link-wrap">
           <a href="tel:+38 (096) 320 12 99" class="call-link menu-link"> +38 (096) 320 12 99</a>
            <!--<a href="#" class="leng-link menu-link">RU</a>-->
          </div>
        </div>
      </nav>
    </section>
    <!--navbar mobile section-->
    <section>
      <nav class="navbar navbar-expand-xl navbar-light bg-white fixed-top my-navbar-mob">
        <div class="my-navbar-mob-link-wrap">
            <button class="navbar-toggler main-menu-btn-wrap" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="Toggle navigation">
                        <span class="mob-sub-menu-button main-menu-btn ">
                    <span></span>
                    <span></span>
                </span>
            </button>
            <a class="navbar-brand align-self-sm-center navbar-brand-mob" href="index.php">
                <img class="navbar-brand-img" src="img/logo_svg.svg">
            </a>
            <a  href="tel:+38 (096) 320 12 99">
                <img src="img/phone-icone.svg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item mob-menu-item">
                    <div class="supper-button-wrap">
                        <a href="category.php" class="nav-link mob-menu-link">Частные строения</a>
                        <nav class="mob-sub-menu-button supper-btn">
                            <span></span>
                            <span></span>
                        </nav>
                    </div>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category.php" class="nav-link mob-menu-link mar-30">Гаражные ворота</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                            секционные ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-1.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                            рулонные ворота RollMatic</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-2.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Гаражные
                            боковые двери</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-1.php" class="nav-link mob-menu-link mar-30">Двери</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-3.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Входные
                            двери Thermo65 и ThermoPro</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-4.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Входные
                            двери ThermoSafe и ThermoCarbon</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-5.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Межкомнатные
                            двери</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-6.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Огнестойкие/Дымозащитные
                            двери</a></li>
                        <!--<li class="mob-drop-menu-item">-->
                            <!--<a href="tovar-7.php" class="nav-link mob-menu-link mob-drop-menu-link">-->
                                <!--Звукоизоляционные двери-->
                            <!--</a>-->
                        <!--</li>-->
                        <li class="mob-drop-menu-item"><a href="tovar-8.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Внутренние
                            двери ZK</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-2.php" class="nav-link mob-menu-link mar-30">Приводы</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-9.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для гаражных
                            ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-10.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для въездных
                            ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-11.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для внутренних
                            дверей</a></li>
                    </ul>
                </li>
                <li class="nav-item mob-menu-item">
                    <div class="supper-button-wrap">
                        <a href="category-prom.php" class="nav-link mob-menu-link">Промышленные</a>
                        <nav class="mob-sub-menu-button supper-btn">
                            <span></span>
                            <span></span>
                        </nav>
                    </div>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-3.php" class="nav-link mob-menu-link mar-30">Ворота</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-12.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Промышленные
                            секционные ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-13.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Скоростные
                            ворота</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-14.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Рулонные
                            ворота и рулонные решетки</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-15.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Перегрузочное
                            оборудование</a>
                        </li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-4.php" class="nav-link mob-menu-link mar-30">Двери</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-16.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Огнестойкие/Дымозащитные
                            двери</a></li>
                        <!--<li class="mob-drop-menu-item"><a href="tovar-17.php"-->
                                                          <!--class="nav-link mob-menu-link mob-drop-menu-link">Звукоизоляционные-->
                            <!--двери</a></li>-->
                        <li class="mob-drop-menu-item"><a href="tovar-18.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Внутренние
                            двери ZK</a></li>
                    </ul>
                    <ul class="mob-drop-menu">
                        <li class="sub-button-wrap">
                            <a href="pod-category-5.php" class="nav-link mob-menu-link mar-30">Приводы</a>
                            <nav class="mob-sub-menu-button">
                                <span></span>
                                <span></span>
                            </nav>
                        </li>
                        <li class="mob-drop-menu-item"><a href="tovar-19.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для гаражных
                            ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-20.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для въездных
                            ворот</a></li>
                        <li class="mob-drop-menu-item"><a href="tovar-21.php"
                                                          class="nav-link mob-menu-link mob-drop-menu-link">Для внутренних
                            дверей</a></li>
                    </ul>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="stock.php">Акции</a></li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="all-news.php">Новости</a>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="about-us.php">О нас</a></li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="gallery.php">Галерея</a>
                </li>
                <li class="nav-item mob-menu-item"><a class="nav-link mob-menu-link" href="contact.php">Контакты и
                    сервис</a></li>
            </ul>
            <!--<a href="#" class="leng-link-mob">English</a>-->
        </div>
    </nav>
    </section>


    <section class="news_k">
      <div class="wrapper_k">
        <div class="news_k_header">
          <img src="img/24var_.jpg" alt="" class="news_k_header_image" />
          <div class="news_k_header_box">
            <p class="news_k_header_box_title">
                Новинка! 24 поверхности Duragrain.
            </p>
            <p class="news_k_header_box_desc">10 февраля, 2018</p>
          </div>
        </div>
      </div>
    </section>
    <section class="new_box_k">
      <div class="wrapper_k">
        <div class="new_box_k_container">
          <p class="new_box_k_container_title">Преимущества панели Duragrain:</p>
          <p class="new_box_k_container_desc">
            - печать производится на современном цифровом оборудовании, это преимущество дает возможность длительного использования поверхности в первозданном виде; <br>
            - поверхности имеют естественный архитектурный стиль, с точной передачей самых мелких деталей; <br>
            - обработка панели специальным лаком, продлевает срок службы в первозданном виде, и сохраняя презентабельный вид на долгие годы; <br>
            - цвет перемычки и фальш-панели всегда соответствует тону декоративной отделки.
          </p>
          <img
            src="img/24__.jpg"
            alt=""
            class="new_box_k_container_image"
          />
        </div>
        <div class="new_box_k_container">
          <p class="new_box_k_container_title">Структура полотна Duragrain:</p>
          <p class="new_box_k_container_desc">
            - высококачественные полотна Duragrain заполнены полиуретаном, имеют структурное покрытие, и оборудованы двустенными стальными секциями; <br>
            - нанесение грунтовочного покрытия серо-белого цвета RAL-9002 с внутренней и наружной стороны; <br>
            - за счет грунтовочного покрытия, с внешней стороны секций, декоративная печать приобретает естественный «настоящий» вид; <br>
            - несомненным преимуществом есть высокая устойчивость декоративной отделки к каким либо погодным условиям и катаклизмам; <br>
            - безупречная устойчивость покрытия Duragrain к царапинам и иным повреждениям. Благодаря этому сохраняется изначальная эстетика ворот.
          </p>
          <img
            src="img/24__2.jpg"
            alt=""
            class="new_box_k_container_image"
          />
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <div id="overlay"></div>
    <div class="alert alert-success" role="alert">
        <p class="text-center">Ваша заявка отправлена</p>
    </div>
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
