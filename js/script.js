$('img.svg').each(function(){
	var $img = $(this);
	var imgID = $img.attr('id');
	var imgClass = $img.attr('class');
	var imgURL = $img.attr('src');

	$.get(imgURL, function(data) {
		// Get the SVG tag, ignore the rest
		var $svg = $(data).find('svg');

		// Add replaced image's ID to the new SVG
		if (typeof imgID !== 'undefined') {
			$svg = $svg.attr('id', imgID);
		}
		// Add replaced image's classes to the new SVG
		if (typeof imgClass !== 'undefined') {
			$svg = $svg.attr('class', imgClass + ' replaced-svg');
		}

		// Remove any invalid XML tags as per http://validator.w3.org
		$svg = $svg.removeAttr('xmlns:a');

		// Replace image with new SVG
		$img.replaceWith($svg);

	}, 'xml');
});

/*======== search for a page link =======*/

const currentLocation = window.location.href.split('Hordis_new/')[1];

$('.nav-item').each((key, el) => {
	let $link = $(el).find('.nav-link.menu-link');
	let $linkHref = $link.attr('href');
	if ($linkHref === currentLocation) {
		$(el).addClass('active');
	}
});



$('.sub-menu-wrap .sub-menu .drop-menu-link').each((key, el) => {
	let $menuLink = $(el).attr('href');
	if ($menuLink === currentLocation) {
		$(el).addClass('menu-line');
	}
});

$('.drop-menu-item').each((key, el) => {
	let $menuLink = $(el).attr('href');
	let $rightLink = $('.sub-menu .drop-menu-link.menu-line');
	if ($menuLink === currentLocation) {
		$(el).addClass('isMenuLine');
	}
	if ($(el).attr('id') === $rightLink.attr('data-id')) {
		$(el).addClass('isMenuLine');
	}
	if ($(el).hasClass('isMenuLine') && $(el).attr('id') <= 3) {
		$(el).parents('.nav-item').addClass('active');
	} else if ($(el).hasClass('isMenuLine') && $(el).attr('id') > 3) {
		$(el).parents('.nav-item').addClass('active');
	}
});

/*======== search for a page link =======*/

$(document).ready(function () {

	$('footer').after(`
		<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
					      <div class="modal-header">
						 		<h5 class="modal-title" id="exampleModalLongTitle">Ваша заявка успешно отправлена!</h5>
					      </div>
					      <div class="modal-footer">
					        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Окей</button>
					      </div>
				    </div>
			  </div>
		</div>
	`);

	 $('input[name=phone]').mask('+38 (999) 999-99-99');

	$('#controlAuto, #controlManual').click(function () {
		if ($(this).prop('checked') === true) {
			$('#controlAuto, #controlManual').val($(this).attr('id'));
		}
	});

	const HEADERS = {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	};

	// $('.estimation_k_container').on('submit', function (e) { // НАЗВАНИЕ КЛАССА ФОРМЫ
	// 	e.preventDefault();
	// 	let switcher;
	// 	let url = '/';
	// 	const $inputArr = $('.estimation_k_container input:not([name=mail]), .estimation_k_container select'); // МАССИВ ИНПУТОВ ИЛИ СЕЛЕКТОВ (КОТОРЫЕ ДОЛЖНЫ ВАЛИДИРОВАТЬСЯ)
	//
	// 	let dataObject = {};
	//
	// 	$inputArr.each((key, el) => {
	// 		let inputName = $(el).attr('name');
	// 		let inputVal = $(el).val();
	// 		if ($(el).val() === '' || $(el).val() === null) {
	// 			$(el).addClass('error'); // ПОВЕСИТЬ СТИЛИ НА ЭТОТ КЛАСС
	// 		} else {
	// 			$(el).removeClass('error');
	// 			dataObject[inputName] = inputVal; // ОБЯЗАТЕЛЬНО ВСЕМ ИНПУТАМИ ПРИСВОИТЬ АТТРИБУТ "name"
	// 		}
	// 	});
	//
	//
	//
	// 	switcher = !$inputArr.hasClass('error');
	//
	// 	if (switcher === true) {
	// 		$.ajax({
	// 			url: url,
	// 			type: 'POST',
	// 			headers: HEADERS,
	// 			data: dataObject
	// 		})
	// 			.done((res) => { // ДЕЙСТВИЯ ПРИ ОТПРАВКЕ ФОРМЫ (ОЧИСТКА ИНПУТОВ, ПОКАЗ МОДАЛКИ И Т.Д.)
	// 				$('#successModal').modal('show');
	// 				$('.estimation_k_container').find('input, select').val('');
	// 			})
	// 			.fail((res) => {
	// 				alert('Ошибка при отправке данных'); // ДЕЙСТВИЕ ПРИ ОШИБКЕ ОТПРАВКИ
	// 			});
	// 	}
	// });

	/*=================================     main-page      ================================= */
	/*====  slick   ====*/
	$('.main-slider').slick({
		arrows: true,
		nextArrow: $('.main-arrow-next-desctop'),
		responsive: [
				{
					breakpoint: 1199,
					settings: {
						nextArrow: $('.main-arrow-next')
					}
				}
				]
	});

	/*===== navbar-shadow========*/
	$('.navbar-toggler').click(function () {
		if (!$('.navbar-collapse').hasClass('show')) {
			setTimeout(function () {
				$('.my-navbar-mob').css({
					overflow: 'auto',
					height: '100%'
				});
			}, 300);
		} else {
			$('.my-navbar-mob').css({
				overflow: 'hidden',
				height: 'auto'
			});
		}
	});


	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 25) {
			$('.navbar').addClass('my-navbar-active');
			$('#grey').removeClass('grey_bg');
		} else {
			$('.navbar').removeClass('my-navbar-active');
			$('#grey').addClass('grey_bg');
		}
	});


	var menuLink = $('.drop-menu-item');
	menuLink.removeClass('menu-line');
	menuLink.on('mouseover', function () {
		menuLink.removeClass('menu-line');
		$(this).addClass('menu-line');
	});

	$('.menu-link').hover(function (e) {
		var closest = $(this).closest('.dropdown-position');
		$(this).next().addClass('active-sm-button').closest('.dropdown-position').siblings('.dropdown-position').find('.my-nav-link-btn').removeClass('active-sm-button');
		closest.find('.drop-menu-wrap').addClass('drop-menu-wrap-active');
		closest.addClass('dropdown-position-active');
		closest.siblings('.dropdown-position').removeClass('dropdown-position-active');
		closest.siblings('.dropdown-position').find('.drop-menu-wrap').removeClass('drop-menu-wrap-active');
	});

	$('.my-nav-link-btn').on('click', function (e) {
		e.preventDefault();
		var closest = $(this).closest('.dropdown-position');
		closest.removeClass('dropdown-position-active');
		closest.find('.drop-menu-wrap').removeClass('drop-menu-wrap-active');
		$(this).removeClass('active-sm-button');
	});

	$('.drop-menu-wrap').on('mouseleave', function () {
		$(this).removeClass('drop-menu-wrap-active');
		$(this).parent().removeClass('dropdown-position-active');
		$(this).parent().find('.my-nav-link-btn').removeClass('active-sm-button');
	});

	/*===== drop-menu-button=====*/
	$('.drop-menu-button').on('click', function () {
		$('.dropdown-menu').slideToggle(300);
		$(this).toggleClass('active-button');
	});

	/*=====     tabs    =======*/
	$('.sub-menu').hide();
	$('.sub-menu:first-child').show();
	$('.drop-menu-item').mouseover(function () {
		let thisEl = $(this);
		let tabIndex = thisEl.index();
		thisEl.closest('.drop-menu')
			.siblings('.sub-menu-wrap').find('.sub-menu')
			.eq(tabIndex).fadeIn(200).siblings().fadeOut(200);
	})
});


/*======    mobile drop-menu    =======*/
$('.mob-drop-menu-item').hide();
$('.mob-sub-menu-button').on('click', function () {
	$(this).toggleClass('active-button');
	let thisCont = $(this).closest('.mob-drop-menu');
	thisCont.find('.mob-drop-menu-item').slideToggle();
});

$(".mob-drop-menu").hide();
$('.supper-btn').on('click', function () {
	let thisCont = $(this).closest('.mob-menu-item');
	thisCont.find('.mob-drop-menu').slideToggle();
});


/*======================================    stock-page    =============================== */



/*=====     stock-tabs    =======*/

$('.stock-tabs-content').hide();
$('.stock-tabs-content:first').show();
$('.stock-tabs-menu-link').on('click', function () {
	let tabIndex = $(this).closest('.stock-tabs-menu-link-wrap').index();

	$(this).addClass('stock-tabs-menu-link-active')
		.closest('.stock-tabs-menu-link-wrap')
		.siblings().find('.stock-tabs-menu-link')
		.removeClass('stock-tabs-menu-link-active');
	$(this).closest('.stock-tabs-box-wrap')
		.find('.stock-tabs-content')
		.eq(tabIndex)
		.show().siblings().hide();
 });
// Переключение ворот и цвета
$('.second-tab-item').hide();
$('.second-tab-item:first').show();
$('.color-btns').hide();
$('.color-btns:first').show();
$('.type-btn').click(function () {
	$('.type-btn').removeClass('btn-active');
	$(this).addClass('btn-active');
	$('.second-tab-item').hide();
	$('.color-btns').hide();
	const tabIndex = $(this).index();
	$(this).closest('.stock-tabs-content')
		.find('.second-tab-item')
		.eq(tabIndex)
		.show();
	$(this).closest('.sub-trigger').find('.color-btns').eq(tabIndex).show();
});
const woodgrain = [
	{
		color: 'Белый цвет',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-white.png',
	},
	{
		color: 'Белый алюминий',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-grey.png',
	},
	{
		color: 'Серый алюминий',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-darkGrey.png',
	},
	{
		color: 'Антрацит металлик',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-antracit.png',
	},
	{
		color: 'Серый антрацит',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-greyAntracit.png',
	},
	{
		color: 'Коричневый',
		surface: 'woodgrain',
		img:'./img/woodgrain/woodgrain-brown.png',
	},
	{
		color: 'Золотой дуб',
		surface: 'Decocolor',
		img:'./img/woodgrain/woodgrain-gold.png',
	},
	{
		color: 'Темний дуб',
		surface: 'Decocolor',
		img:'./img/woodgrain/woodgrain-darkDoob.png',
	},
	{
		color: 'Ночной дуб',
		surface: 'Decocolor',
		img:'./img/woodgrain/woodgrain-nightDoob.png',
	},
];

const planar = [
	{
		color: 'Белый цвет',
		surface: 'planar',
		img:'./img/decolor_planar/planar-white.png',
	},
	{
		color: 'Белый алюминий',
		surface: 'planar',
		img:'./img/decolor_planar/planar-grey.png',
	},
	{
		color: 'Серый алюминий',
		surface: 'planar',
		img:'./img/decolor_planar/planar-darkgrey.png',
	},
	{
		color: 'Антрацит металлик',
		surface: 'planar',
		img:'./img/decolor_planar/planar-antracitMetal.png',
	},
	{
		color: 'Серый антрацит',
		surface: 'planar',
		img:'./img/decolor_planar/planar-antracitGrey.png',
	},
	{
		color: 'Коричневый',
		surface: 'planar',
		img:'./img/decolor_planar/planar-brown.png',
	},
	{
		color: 'Золотой дуб',
		surface: 'Decocolor',
		img:'./img/decolor_planar/decolor-goldDoob.png',
	},
	{
		color: 'Темний дуб',
		surface: 'Decocolor',
		img:'./img/decolor_planar/decolor-darkDoob.png',
	},
	{
		color: 'Ночной дуб',
		surface: 'Decocolor',
		img:'./img/decolor_planar/decolor-nightDoob.png',
	},
];


function setColor(){
	$('.color-btn').click(function () {
		const thisIndex = $(this).index();
		const sectionActiveIndx = $('.btn-active').index();
		const thisContainer = $('.second-tab-item').eq(sectionActiveIndx);
		if($(this).attr('data-name')=='planar') {

			thisContainer.find('.main-tab-img').attr('src', planar[thisIndex].img);
			thisContainer.find('.surface').text(planar[thisIndex].surface);
			thisContainer.find('.door-color').text(planar[thisIndex].color);
			$('.planar img').attr('src', planar[thisIndex].img );
			$('.woodgrain img').attr('src', woodgrain[thisIndex].img );


		}
		else if($(this).attr('data-name')=='woodgrain'){
			thisContainer.find('.main-tab-img').attr('src', woodgrain[thisIndex].img);
			thisContainer.find('.surface').text(woodgrain[thisIndex].surface);
			thisContainer.find('.door-color').text(woodgrain[thisIndex].color);
			$('.planar img').attr('src', planar[thisIndex].img );
			$('.woodgrain img').attr('src', woodgrain[thisIndex].img );
		}

	});
}
setColor();

/*=============     about-us slick      ==============*/
let aboutSlider = $('.about-us-main-slider');

aboutSlider.on('init', function (event, slick) {
	let slideCount = slick.slideCount;
	$('.total').text(slideCount);
	$('.current').text(slick.currentSlide + 1);
});

aboutSlider.slick({
	arrows: true,
	nextArrow: $('.about-us-arrow-next'),
	prevArrow: $('.about-us-arrow-prev')
});

aboutSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
	$('.current').text(nextSlide + 1);

});


// Tovar
$('.gallery_slider').slick({
	speed: 700,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
});

function slideInit(el) {
	$(`.goods_k_container_box_slider_big.${el}`).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		draggable: false,
		arrows: false
	});
	$(`.goods_k_container_box_slider_small.${el}`).slick({
		slidesToShow: 3,
		draggable: false,
		infinite: false,
		asNavFor: `.goods_k_container_box_slider_big.${el}`,
		focusOnSelect: true,
		arrows: false
	});
}

$(document).ready(function () {
	let slideNum = $('body .goods_k');
	slideNum.each((key, elem) => {
		let $slideBig = $(elem).find('.goods_k_container_box_slider_big');
		let $slideSmall = $(elem).find('.goods_k_container_box_slider_small');
		$slideBig.addClass(`garage_${key}`);
		$slideSmall.addClass(`garage_${key}`);
		slideInit(`garage_${key}`);
	});
	let typeCont = $('.types_k');
	typeCont.each((key, el) => {
		let typeBox = $(el).find('.types_k_container_box');
		for (let i = 0; i < typeBox.length; i++) {
			$(el).find('.types_k_slider').append(`
	                <div class="slide">
	                    <div class="types_k_container_box">
	                        <div class="types_k_container_box_image"
	                             style="background:url(${$(typeBox[i]).find('.types_k_container_box_image').attr('style').split(')')[0].split('(')[1]}) center/contain no-repeat">
	                            <div class="overlay">
	                                <p class="overlay_desc">${$(typeBox[i]).find('.overlay_desc').text()}</p>
	                            </div>
	                        </div>
	                        <p class="types_k_container_box_desc">${$(typeBox[i]).find('.types_k_container_box_desc').text()}</p>
	                    </div>
	                </div>
	            `);
		}
	});
	$('.types_k_slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		arrows: false,
		centerMode: true,
	});

	$('.view_k_container_box_slider_big.door .slide').each((key, el) => {
		let imgUrl = $(el).find('img').attr('src');
		let slideSmallArr = $('.view_k_container_box_slider_small.door');
		slideSmallArr.append(`
			<div class="slide">
				<div class="slide-box">
					<img src=${imgUrl} alt="" class="slide-box_image">
				</div>
			</div>
		`)
	});
	$('.view_k_container_box_slider_big.wood .slide').each((key, el) => {
		let imgUrl = $(el).find('img').attr('src');
		let slideSmallArr = $('.view_k_container_box_slider_small.wood');
		slideSmallArr.append(`
			<div class="slide">
				<div class="slide-box">
					<img src=${imgUrl} alt="" class="slide-box_image">
				</div>
			</div>
		`)
	});
	$('.view_k_container_box_slider_big.door').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.view_k_container_box_slider_small'
	});
	$('.view_k_container_box_slider_small.door').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		vertical: true,
		focusOnSelect: true,
		centerMode: true,
		centerPadding: '0px',
		asNavFor: '.view_k_container_box_slider_big',
		prevArrow: '<div class="arrow up"><img src="img/arrow_up.png" alt=""></div>',
		nextArrow: '<div class="arrow down"><img src="img/arrow_up.png" alt=""></div>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 2,
					vertical: false,
				}
			}
		]
	});

	$('.view_k_container_box_slider_big.wood').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.view_k_container_box_slider_small'
	});
	$('.view_k_container_box_slider_small.wood').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		vertical: true,
		focusOnSelect: true,
		centerMode: true,
		centerPadding: '0px',
		asNavFor: '.view_k_container_box_slider_big',
		prevArrow: '<div class="arrow up"><img src="img/arrow_up.png" alt=""></div>',
		nextArrow: '<div class="arrow down"><img src="img/arrow_up.png" alt=""></div>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 2,
					vertical: false,
				}
			}
		]
	});

	$('.advantage_k .advantage_k_container_item').each((key, el) => {
		$(el).find('.advantage_box').attr('data-accord', key);
		$(el).find('.advantage_k_container_item_tabul').attr('id', key);
	});
});

$('[data-tab-tovar]').click(function () {
	var tovar = $(this).attr('data-tab-tovar');

	$('.view_k_container_tabs_text').removeClass('active');
	$(this).addClass('active');

	$('.view_k_container_box').removeClass('active');
	$('.view_k_container_box' + tovar).addClass('active');
});

$(document).ready(function () {
	let $prodArr = $('.types_k_container');
	let $wrapProd = $('.wrapper_k');
	$prodArr.each((key, el) => {
		let $prodBoxArr = $(el).find('.types_k_container_box');
		for (let i in $prodBoxArr) {
			if (i > 7) {
				$($prodBoxArr[i]).addClass('hide');
			}
		}
	})
	;
	$wrapProd.each((key, el) => {
		if ($(el).find('.types_k_container_box').hasClass('hide')
		) {
			$(el).append(`
                <div class="estimation_k_container_box show_more d-none d-xl-block">
                    <button class="estimation_k_container_btn">Показать еще</button>
                </div>
            `)
		}
	})
	;
	$('.show_more').click(function () {
		$(this).parent().find('.types_k_container .types_k_container_box').removeClass('hide');
		$(this).addClass('hide');
	});
});


$('.advantage_k_container_item_tabul').slideUp();
$('.advantage_k_container_item_tabul:first').slideDown();
$('.advantage_box').on('click', function () {
	let that = $(this);
	that.find('.advantage_k_container_item_arrow').addClass('active-arrow-tov');
	that.next().slideDown();
	that.closest('.advantage_k_container_item').siblings().find('.advantage_k_container_item_tabul').slideUp();
	that.closest('.advantage_k_container_item').siblings().find('.advantage_k_container_item_arrow').removeClass('active-arrow-tov');
});

// Category

$('.property_k_slider .slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: '<div class="arrow left"><img src="img/arrow_left_wh.png"></div>',
	nextArrow: '<div class="arrow right"><img src="img/arrow_right_wh.png"></div>',
	swipeToSlide: false
});

$('[data-podcategory]').mouseover(function () {
	var podcategory = $(this).attr('data-podcategory');

	$('.property_k_container_box').removeClass('active');
	$(this).addClass('active');

	$('.property_k_container_bg').removeClass('active');
	$('.property_k_container_bg' + podcategory).addClass('active');

});


var $work = $('.single-item');
var slideCount = null;

$work.on('init', function (event, slick) {

	slideCount = slick.slideCount;
	setSlideCount();
	setCurrentSlideNumber(slick.currentSlide);

});

$work.slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: '<div class="arrow left"><img src="img/arrow_category.svg"></div>',
	nextArrow: '<div class="arrow right"><img src="img/arrow_category.svg"></div>'

});

$work.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
	setCurrentSlideNumber(nextSlide);
});

function setSlideCount() {
	var $el = $('.slide-count-wrap').find('.total');
	$el.text(slideCount);
}

function setCurrentSlideNumber(currentSlide) {
	var $el = $('.slide-count-wrap').find('.current');

	$el.text(currentSlide + 1);
}


// Pod-Category

var $mySlider = $('.podcategory_k_slider .slider');
var timer = null;
var clickCounter = 0;
let counter = false;
// const bgImg = $($('.podcategory_k_container_image_item').get(0));
// const bgImageData = {
// 	...bgImg.offset(),
// 	width: bgImg.width(),
// 	height: bgImg.height(),
// };
// const rightImage = $(window).width() - (bgImageData.left + bgImageData.width);

function slideShow() {
	$('.podcategory_k').addClass('active');
	$('.my-navbar').addClass('pod_category');
	$('.podcategory_k_slider').removeClass('none');
	$mySlider.slick('resize');
}

// let slideRemove = function () {
// 	let sliderTimeOut = null;
// 	clearTimeout(sliderTimeOut);
// 	$('.podcategory_k_background').animate({
// 		'top': '50px',
// 		'right': rightImage + 'px',
// 		'width': bgImageData.width + 'px',
// 		'height': bgImageData.height + 'px',
// 	}, 8);
// 	$('.podcategory_k_slider').addClass('animation');
// 	$('.podcategory_k_background').addClass('animation');
//
// 	$('.podcategory_k_container_box').addClass('disp_block');
// 	$('.podcategory_k_bac').addClass('disp_block');
//
// 	setTimeout(function () {
// 		$('.podcategory_k_slider').removeClass('animation');
// 		$('.podcategory_k_background').removeClass('animation');
// 		$('.podcategory_k_slider').addClass('none');
// 		$('.my-navbar').removeClass('pod_category');
// 		$('.podcategory_k').removeClass('active');
//
// 		$('.podcategory_k_container_box').removeClass('disp_block');
// 		$('.podcategory_k_bac').removeClass('disp_block');
// 		$('.podcategory_k_background').css({
// 			'top': '',
// 			'right': '',
// 			'width': '',
// 			'height': '',
// 		});
// 	}, 800);
// };

$mySlider.on('init', function (event, slick) {
	var srcImage = $('[data-slick-index]').find('.podcategory_k_slider_box_image').attr('src');
	$('.podcategory_k_background[data-back=0]').addClass('active');
	$('.podcategory_k_background[data-back]').css('background-image', `url(${srcImage})`);
	// $('.arrow.top.toExit').on('click', slideRemove);
});

$mySlider.slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	dots: true,
	fade: true,
	infinite: false,
	dontAnimate: true,
	prevArrow: '<div class="arrow top"><img src="img/arrow_left_wh.png"></div>',
	nextArrow: '<div class="arrow down"><img src="img/arrow_right_wh.png"></div>',
});

slideShow();

// $mySlider.on('afterChange', (event, slick, currentSlide) => {
// 	if (currentSlide === 0) {
// 		setTimeout(() => {
// 			$('.arrow.top').addClass('toExit');
// 			$('.arrow.top.toExit').on('click', slideRemove);
// 		}, 500)
// 	}
// });

$mySlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

	// $('.arrow.top').removeClass('toExit');

	var srcImage = $('[data-slick-index=' + nextSlide + ']').find('.podcategory_k_slider_box_image').attr('src');
	$('.podcategory_k_background').removeClass('active');
	$('.podcategory_k_background').removeClass('bottom');
	$('.podcategory_k_background[data-back=' + nextSlide + ']').addClass('active');
	$('.podcategory_k_background[data-back=' + nextSlide + ']').css('background-image', `url(${srcImage})`);
	$('.podcategory_k_background[data-back=' + currentSlide + ']').addClass('bottom');

	if (currentSlide > nextSlide) {
		$('.podcategory_k_slider_box_image').addClass('active');
		$('[data-slick-index=' + currentSlide + ']').find('.podcategory_k_slider_box_image').removeClass('active');
		$('.slick-dots').addClass('active');
		$('.podcategory_k_slider_box_title').addClass('active');
	} else {
		$('.podcategory_k_slider_box_image').removeClass('active');
		$('[data-slick-index=' + currentSlide + ']').find('.podcategory_k_slider_box_image').addClass('active');
		$('.slick-dots').removeClass('active');
		$('.podcategory_k_slider_box_title').removeClass('active');
	}

});

$('.podcategory_k').on('wheel', function (e) {
	if (e.originalEvent.deltaY > 0) {
		$mySlider.slick('slickNext');
	} else {
		$mySlider.slick('slickPrev');
	}
});

// let slideMove = (e) => {
// 	console.log('srabatolo govno');
// 	if ($('.podcategory_k').hasClass('active')) {
// 		var windowCurrent = $mySlider.find('.slick-current').data('slick-index');
// 		if (e.originalEvent.deltaY < 0) {
// 			windowCurrent = windowCurrent - 1;
// 			$mySlider.slick('slickGoTo', windowCurrent);
// 		} else {
// 			windowCurrent = windowCurrent + 1;
// 			$mySlider.slick('slickGoTo', windowCurrent);
// 		}
// 	}
// };

// const podcateg = document.querySelector('.podcategory_k');
//
// console.log(podcateg);
//
// function touchMove(e) {
// 	console.log(e.touches[0].pageY);
// 	if (e.touches[0].pageY < 350) {
// 		counter = true;
// 		if (counter === true) {
// 			slideShow(e);
// 		}
// 	}
// }
//
// podcateg.addEventListener("touchmove", touchMove, false);

// $('.podcategory_k').on('wheel', function (e) {
// 	if (e.originalEvent.deltaY > 0) {
// 		counter = true;
// 		if (counter === true) {
// 			slideShow(e);
// 		}
// 	}
// });

// $('section.podcategory_k .wrapper_k .podcategory_k_scroll_more').click(function () {
// 	$('.podcategory_k').addClass('active');
// 	$('.my-navbar').addClass('pod_category');
// 	$('.podcategory_k_slider').removeClass('none');
// 	slideShow();
// });

// $mySlider.on('afterChange', function(event, slick, currentSlide){

//         $('.arrow.top').on('click', function(){
//             if(currentSlide == 0){
//                 $('.podcategory_k').removeClass('active');
//                 $mySlider.slick('unslick');
//             }
//         });
//     });

// var sliderTimeOut = null;

// $(document).on('keydown', function (e) {
// 	if (e.keyCode == 27) {
// 		clearTimeout(sliderTimeOut);
// 		$('.podcategory_k_background').animate({
// 			// 'top': bgImageData.top + 'px',
// 			'top': '50px',
// 			'right': rightImage + 'px',
// 			'width': bgImageData.width + 'px',
// 			'height': bgImageData.height + 'px',
// 		}, 8);
// 		$('.podcategory_k_slider').addClass('animation');
// 		$('.podcategory_k_background').addClass('animation');
//
// 		$('.podcategory_k_container_box').addClass('disp_block');
// 		$('.podcategory_k_bac').addClass('disp_block');
//
// 		sliderTimeOut = setTimeout(function () {
// 			$('.podcategory_k_slider').removeClass('animation');
// 			$('.podcategory_k_background').removeClass('animation');
// 			$('.podcategory_k_slider').addClass('none');
// 			$('.my-navbar').removeClass('pod_category');
// 			// $mySlider.slick('unslick');
// 			$('.podcategory_k').removeClass('active');
//
// 			$('.podcategory_k_container_box').removeClass('disp_block');
// 			$('.podcategory_k_bac').removeClass('disp_block');
// 			$('.podcategory_k_background').css({
// 				'top': '',
// 				'right': '',
// 				'width': '',
// 				'height': '',
// 			});
// 		}, 800);
//
// 	}
// });


// Gallery
$(document).ready(function () {
	var arr = $('.gallery_k_grid .gallery_k_grid_item');
	arr.each((key, element) => {
		$(element).attr('data-gallery', key);

	});
});
var $gallery = $('.slider_modal_container');
var galleryCount = null;


$gallery.on('init', function (event, slick) {

	galleryCount = slick.slideCount;
	setGalleryCount();
	setCurrentGalleryNumber(slick.currentSlide);

});

$gallery.slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: '<div class="arrow left"><img src="img/arrow_up.png"></div>',
	nextArrow: '<div class="arrow right"><img src="img/arrow_up.png"></div>',
	fade: true,
})

$gallery.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

	setCurrentGalleryNumber(nextSlide);
});

function setGalleryCount() {

	var $el1 = $('.slider_modal_counter').find('.total');
	$el1.text(galleryCount);
}

function setCurrentGalleryNumber(currentSlide) {
	var $el1 = $('.slider_modal_counter').find('.current');
	$el1.text(currentSlide + 1);
}

$('.gallery_k_grid_item').click(function () {
	var gallery = $(this).attr('data-gallery');
	$('.slider_modal').addClass('active');
	$('.body_scroll').addClass('active');


	$('.slider_modal_container').slick('slickGoTo', gallery, true);


});


// $('section.gallery_k .wrapper_k .gallery_k_grid_item').click(function(){
//     $('.slider_modal').addClass('active');
// });
$('.slider_modal .overlay').click(function () {
	$('.slider_modal').removeClass('active');
	$('.body_scroll').removeClass('active');
});


// main
$(document).ready(function () {
	var newWidth;
	var windWidth = $(window).width();
	var slideInactWidth = 1;

	$('.nav_cont.next').mousemove(function (e) {
		var pos = $(this).offset();
		var elem_left = pos.left;
		var Xinner = e.pageX - elem_left;
		var $slideActive = $('.slide.active');

		var newSlideInactWidth = slideInactWidth + (Xinner * 2.1);
		$slideActive.width(newSlideInactWidth);
		$('.arrow_bg_slider').addClass('active');


		var pos = $(this).offset();
		var elem_left = pos.left;
		var Xinner = e.pageX - elem_left;
		var $slideActive = $('.slide.active');

		if (Xinner < 80) {
			$slideActive.width(1);
		}

		return newWidth = newSlideInactWidth;
	});

	$('.nav_cont.next').click(function () {
		var $slideActive = $('.slide.active');
		var disc = windWidth - newWidth;
		let $that = $(this);
		$slideActive.width(newWidth + disc);
		newWidth = 0;

		$('.section_main_box_foot').addClass('animate');
		$('.section_main_box_foot').removeClass('transform');

		$that.css('z-index', '-1');

		setTimeout(function () {
			$('.slide').toggleClass('active');
			var $slideActive = $('.slide.active');
			$slideActive.width(1);
			$that.css('z-index', '10');
		}, 280);
		setTimeout(function () {
			$('.section_main_box_foot').removeClass('animate');
			$('.section_main_box_foot').addClass('transform');
		}, 750);
	});

	$('.nav_cont.next').mouseenter(function () {
		setTimeout(function () {
			$('.slide:not(.active) img').addClass('hide');
			$('.slide.active img').addClass('show');
		}, 300);
	});

	$('.nav_cont.next').mouseleave(function () {
		$('.arrow_bg_slider').removeClass('active');
		$('.slide:not(.active) img').removeClass('hide');
		$('.slide.active img').removeClass('show');
	});
});


/* ===================== modal window   =====================*/

$(document).ready(function () { // вся мaгия пoсле зaгрузки стрaницы
	$('#go,.go').click(function (event) { // лoвим клик пo ссылки с id="go"
		event.preventDefault(); // выключaем стaндaртную рoль элементa
		$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
			function () { // пoсле выпoлнения предъидущей aнимaции
				$('#modal_form')
					.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
					.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
			});
	});
	/* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
	$('#modal_close, #overlay').click(function () { // лoвим клик пo крестику или пoдлoжке
		$('#modal_form')
			.animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
				function () { // пoсле aнимaции
					$(this).css('display', 'none'); // делaем ему display: none;
					$('#overlay').fadeOut(400); // скрывaем пoдлoжку
				}
			);
	});
});
/* ===================== modal window 2  =====================*/

$(document).ready(function () { // вся мaгия пoсле зaгрузки стрaницы
	$('.footer_link').click(function (event) {
		event.preventDefault(); // выключaем стaндaртную рoль элементa
		$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
			function () { // пoсле выпoлнения предъидущей aнимaции
				$('#modal_form_small')
					.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
					.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
			});
	});
	/* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
	$('#modal_close_small, #overlay').click(function () { // лoвим клик пo крестику или пoдлoжке
		$('#modal_form_small')
			.animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
				function () { // пoсле aнимaции
					$(this).css('display', 'none'); // делaем ему display: none;
					$('#overlay').fadeOut(400); // скрывaем пoдлoжку
				}
			);
	});
});
// $('#input_2_k_sm').mask("+38(999) 999-99-99");
// $('#input_2_k').mask("+38(999) 999-99-99");
// Акция Табулятор 2 на дверях
$('.dor-tab-item').hide();
$('.dor-tab-item:first').show();
$('.doors-btn').click(function () {
	const thisIndex = $(this).index();
	$('.dor-tab-item').eq(thisIndex).show().siblings().hide();
});
const white = [
	{
		url:"./img/THP-010/THP010white.jpg",
		price:"47 000 грн*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP015/THP015belii.jpg",
		price:"47 000 грн*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP515/THP515belii.jpg",
		price:"47 000 грн*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP700S/THP700Sbelii.jpg",
		price:"59 500 грн.*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP810/THP810Sbelii.jpg",
		price:"59 500 грн.*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP900/THP900Sbelii.jpg",
		price:"59 500 грн.*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP700/THP700Abelii.jpg",
		price:"47 000 грн*",
		color:"RAL 9016 белый цвет"

	},
	{
		url:"./img/THP750/THP750Fbelii.jpg",
		price:"47 000 грн*",
		color:"RAL 9016 белый цвет"

	},
];
const antracGrey= [
	{
		url:"./img/THP-010/THP010greyAntracit.jpg",
		price:"47 000 грн*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP015/THP015seriiantrazit.jpg",
		price:"47 000 грн*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP515/THP515seriiantrazit.jpg",
		price:"47 000 грн*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP700S/THP700Santracitgrey.jpg",
		price:"59 500 грн.*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP810/THP810Sseriiantrazit.jpg",
		price:"59 500 грн.*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP900/THP900Sseriiantrazit.jpg",
		price:"59 500 грн.*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP700/THP700Aseriiantrazit.jpg",
		price:"50 750 грн.*",
		color:"RAL 7016 серый антрацит"

	},
	{
		url:"./img/THP750/THP750Fseriiantrazit.jpg",
		price:"50 750 грн.*",
		color:"RAL 7016 серый антрацит"

	},
];
const brown= [
	{
		url:"./img/THP-010/THP010brown.jpg",
		price:"47 000 грн*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP015/THP015koricnevii.jpg",
		price:"47 000 грн*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP515/THP515korichnevii.jpg",
		price:"47 000 грн*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP700S/THP700Sbrown.jpg",
		price:"59 500 грн.*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP810/THP810Skoricnevii.jpg",
		price:"59 500 грн.*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP900/THP900Skoricnevii.jpg",
		price:"59 500 грн.*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP700/THP700Akorichnevii.jpg",
		price:"50 750 грн.*",
		color:"RAL 8028 коричневый"

	},
	{
		url:"./img/THP750/THP750Fkorichnevii.jpg",
		price:"50 750 грн.*",
		color:"RAL 8028 коричневый"

	},
];
const antracMetal= [
	{
		url:"./img/THP-010/THP010antracitMetalic.jpg",
		price:"47 000 грн*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP015/THP015atracitmetall.jpg",
		price:"47 000 грн*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP515/THP515antrazitmetall.jpg",
		price:"47 000 грн*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP700S/THP700SantracitMetalic.jpg",
		price:"59 500 грн.*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP810/THP810Santrazitmetall.jpg",
		price:"59 500 грн.*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP900/THP900Santracitmetall.jpg",
		price:"59 500 грн.*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP700/THP700Aanrtazitmetall.jpg",
		price:"54 250 грн.*",
		color:"CH 703 антрацит металлик"

	},
	{
		url:"./img/THP750/THP750Fantrazitmetall.jpg",
		price:"54 250 грн.*",
		color:"CH 703 антрацит металлик"

	},
];
const gold= [
	{
		url:"./img/THP-010/THP010goldDoob.jpg",
		price:"52 500 грн.*",
		color:"Decograin Золотой дуб"
	},
	{
		url:"./img/THP015/THP015goolddyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP515/THP515goolddyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP700S/THP700Sgold.jpg",
		price:"64 750 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP810/THP810Sgoolddyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP900/THP900Sgoolddyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP700/THP700Agoolddyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Золотой дуб"

	},
	{
		url:"./img/THP750/THP750Fgoolddyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Золотой дуб"

	},
];
const dark= [
	{
		url:"./img/THP-010/THP010darkDoob.jpg",
		price:"52 500 грн.*",
		color:"Decograin Темный дуб"
	},
	{
		url:"./img/THP015/THP015temniidyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP515/THP515temniidyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP700S/THP700SDarkDoob.jpg",
		price:"64 750 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP810/THP810Stemniidyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP900/THP900Stemniidyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP700/THP700Atemniidyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Темный дуб"

	},
	{
		url:"./img/THP750/THP750Ftemniidyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Темный дуб"

	},

];
const night= [
	{
		url:"./img/THP-010/THP010nightDoob.jpg",
		price:"52 500 грн.*",
		color:"Decograin Ночной дуб"
	},
	{
		url:"./img/THP015/TPS_015nochnoidyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP515/THP515nocnoidyb.jpg",
		price:"52 500 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP700S/THP700SnightDoob.jpg",
		price:"64 750 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP810/THP810Snocnoidyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP900/THP900Snocnoiidyb.jpg",
		price:"64 750 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP700/THP700Anocnoidyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Ночной дуб"

	},
	{
		url:"./img/THP750/THP750Fnochnoidyb.jpg",
		price:"54 250 грн.*",
		color:"Decograin Ночной дуб"
	},

];


$('.colors-box-item img').click(function () {

	const thisData = $(this).attr('data-color');
	const doorsBtnArr = $('.doors-btn img');
	const doorsImgArr = $('.tab-img-wrap img');
	const colorField = $('.color-doors');
	const priceArr = $('.price-block__price:not(.TPS-price)');


	function setData(array) {
		$(doorsBtnArr).each(function(i, item){
			$(item).attr('src', array[i].url);
		});
		$(doorsImgArr).each(function(i, item){
			$(item).attr('src', array[i].url);
		});
		$(colorField).text(array[0].color);
		$(priceArr).each(function (i,elem) {
			elem.innerText = array[i].price;

		});

	}


	switch (thisData) {
		case "white":{
			setData(white);
			break;
		};

		case "antracGrey":{
			setData(antracGrey);
			break;
		};

		case "brown":{
			setData(brown);
			break;
		};

		case "antracMetal":{
			setData(antracMetal);
			break;
		};

		case "gold":{
			setData(gold);
			break;
		};

		case "dark":{
			setData(dark);
			break;
		};

		case "night":{
			setData(night);
			break;
		};

	}
});

const TPS=[
	{
		url:"./img/TPS010/TPS10white.jpg",
		color:"RAL 9016 білий колір",
		price:"28 000 грн.*",
	},
	{
		url:"./img/TPS010/TPS10greyAntracit.jpg",
		color:"RAL 7016 cірий антрацит",
		price:"28 000 грн.*",
	},
	{
		url:"./img/TPS010/TPS10brown.jpg",
		color:"RAL 8028 коричневый",
		price:"28 000 грн.*",
	},
	{
		url:"./img/TPS010/TPS10metallicAntrac.jpg",
		color:"CH 703 антрацит металлик",
		price:"33 600 грн.*",
	},
	{
		url:"./img/TPS010/TPS10gold.jpg",
		color:"Decograin Золотой дуб",
		price:"33 600 грн.*",
	},
	{
		url:"./img/TPS010/TPS10dark.jpg",
		color:"Decograin Темний дуб",
		price:"33 600 грн.*",
	},
];
$('.colors-btn').click(function () {
	const doorImg = $('.door-container-img');
	const doorColor = $('.TPS-color');
	const doorPrice = $('.TPS-price');

	const thisIndex = $(this).index();

	$(doorImg).attr('src',TPS[thisIndex].url);
	$(doorColor).text(TPS[thisIndex].color);
	$(doorPrice).text(TPS[thisIndex].price);
});

$(".send-form").on('submit',function(e){
	e.preventDefault();
	const that = $(this);
	const field = that.find('input, textarea');
	const requiredInput = that.find('.required');
	const inputEmail =that.find('input[name="email"]').val();
	const inputName = that.find('input[name="name"]').val();
	const EMAIL_REGEXP = new RegExp("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
	const name_regexp =  new RegExp('^[0-9]+$');
	console.log(inputName);
	$(requiredInput).each((i,item)=>{

		if($(item).val()==="" ){
			$(item).addClass('err');
		}

		else{
			$(item).removeClass('err');
		}
	});
	if (name_regexp.test(inputName)||inputName =="") {

		that.find('input[name="name"]').addClass('err')
	}
	else {that.find('input[name="name"]').removeClass('err')}

	 if(inputEmail!== "" && !EMAIL_REGEXP.test(inputEmail)) {
		 that.find('input[name="email"]').addClass('err');

	 }
	 else{
		 that.find('input[name="email"]').removeClass('err');
	 }
	let switcher;
	const inputVal = that.serializeArray();
	switcher = !field.hasClass('err');

	if (switcher) {
		$.ajax({
			url: "./sent.php",
			type: 'POST',
			data: inputVal,
			success: function (res) {
				if(res.status){
				$('#myModal').modal('show');
				$('#modal_form_small').hide();
				$('#modal_form').hide();
				$('#overlay').hide();
				$(field).each(function(i,item){
					$(item).val('');
				});
				}
			},
			error: function () {
				alert('ошибка при вводе данных');
			}
		});
	}

});
