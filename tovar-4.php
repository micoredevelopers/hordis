<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>


    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Входные двери - Лицо частного владения
            </p>
            <p class="goods_k_container_box_desc">
              Главный аспект визитной карты владельца дома, отражается во
              входной двери в его усадьбу. Высочайшее качество Hörmann - это
              продуманный внешний вид, подчеркивающий элегантность архитектуры
              дома, и воплощающий в жизнь самые смелые дизайнерские задумки.
              Четыре образа входной двери помогут найти дорогу к индивидуальной
              архитектуре каждого.
              <br />
              <br />
              Двери и ворота Hörmann в Одессе - это компания “Hördis”, ШОУ-РУМ
              которой, есть в городе. Организация проведет компетентный монтаж,
              и предоставит сервисное обслуживание не только в городе, но и за
              его пределами.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog/DveriThermo.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-3.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/main-3.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/carbon-1.jpg') center/contain no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/carbon-2.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/carbon-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/carbon-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Двери категории Премиум-класса ThermoCarbon
            </p>
            <p class="goods_k_container_box_desc">
              Превосходство модели гарантирует лучшую теплоизоляцию и защитную
              систему от взлома. Дверь отвечает высоким стандартам
              энергоэффективности, благодаря теплоизоляции с внушительным
              коэффициентом UD (около 0,47 Вт/(м²·K)). Максимальную надежность
              обеспечивает устройство RC 3 класса, позволяющее запереть дверь с
              помощью 9 –кратной блокировки. Комплектация створки реализована
              профилем из высокотехнологичного материала, на основе стекла и
              карбона, что определяет сверхпрочность и надежность конструкции,
              среди алюминиевых представителей дверей.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-1.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет рубиново-красный RAL 3003, матовый
                  <br />
                  <br />
                  Ручка из нержавеющей стали HOE 700, сертифицированная дверь
                  для энергосберегающих домов, коэффициент теплоизоляции UD до
                  0,50 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 860</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-2.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый, ручка-планка и
                  углубление в дверном полотне предпочтительного матового цвета
                  серого антрацита RAL 7016 <br />
                  <br />
                  Ручка-планка, установленная заподлицо, серийно цвета белого
                  алюминия RAL 9006 с шелковистым блеском, сертифицированная
                  дверь для энергосберегающих домов, коэффициент теплоизоляции
                  UD до 0,51 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 308</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-3.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 700, аппликации из нержавеющей
                  стали, орнаментное стекло Pave белого цвета, 4-слойное
                  теплоизоляционное остекление, сертифицированная дверь для
                  энергосберегающих домов, коэффициент теплоизоляции UD до 0,57
                  Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 680</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-4.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет Hörmann CH 703 «антрацит», структурная
                  поверхность, аппликация из алюминия предпочтительного
                  рубиново-красного цвета RAL 3003, матовая <br />
                  <br />
                  Ручка из нержавеющей стали, установленная заподлицо,
                  углубление в дверном полотне серийно в цвет двери, аппликация
                  из алюминия заподлицо, сертифицированная дверь для
                  энергосберегающих домов, коэффициент теплоизоляции UD до 0,50
                  Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 305</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-5.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный светло-серый цвет RAL 7040, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 600, аппликация из нержавеющей
                  стали, стекло с мотивом: матовое Float с 7 прозрачными
                  полосами, четырехслойное теплоизоляционное остекление,
                  сертифицированная дверь для энергосберегающих домов,
                  коэффициент теплоизоляции UD до 0,55 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 686</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-6.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный светло-серый цвет RAL 7040, матовый <br />
                  <br />
                  Ручка из нержавеющей стали, установленная заподлицо,
                  углубленный вырез под остекление, орнаментное стекло Float
                  матовое, тройное теплоизоляционное остекление, коэффициент
                  теплоизоляции UD до 0,63 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 310</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-7.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый <br />
                  <br />
                  Ручка-планка серийно цвета белого алюминия RAL 9006, с
                  шелковистым блеском, углубление в дверном полотне серийно в
                  цвет двери, сертифицированная дверь для энергосберегающих
                  домов, коэффициент теплоизоляции UD до 0,50 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 300</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-8.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный светло-серый цвет RAL 7040, матовый <br />
                  <br />
                  Установленная заподлицо ручка-планка серийно цвета белого
                  алюминия RAL 9006, с шелковистым блеском, углубление в дверном
                  полотне серийно в цвет двери, аппликации из нержавеющей стали,
                  стекло с мотивом – матовое Float с одной прозрачное полосой,
                  4-слойное теплоизоляционное остекление, сертифицированная
                  дверь для энергосберегающих домов, коэффициент теплоизоляции
                  UD до 0,57 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 302</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-9.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет Hörmann CH 607 «каштановый», структурная
                  поверхность, углубление в дверном полотне предпочтительного
                  цвета белого алюминия RAL 9006, с шелковистым блеском <br />
                  <br />
                  Ручка из нержавеющей стали, установленная заподлицо,
                  углубление в дверном полотне серийно в цвет двери, аппликации
                  из нержавеющей стали, орнаментное стекло Float, матовое,
                  4-слойное теплоизоляционное остекление, сертифицированная
                  дверь для энергосберегающих домов, коэффициент теплоизоляции
                  UD до 0,57 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 306</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/carbon/door-10.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 700, орнаментное стекло Pave
                  белого цвета, тройное теплоизоляционное остекление,
                  сертифицированная дверь для энергосберегающих домов,
                  коэффициент теплоизоляции UD до 0,57 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 650</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Двери категории Высший-класс ThermoSafe
            </p>
            <p class="goods_k_container_box_desc">
              ThermoSafe<br />
              <br />
              Элегантный дизайн, инновационная теплоизоляция и надежная защита –
              лучшая основа для современной входной зоны. Безопасность дома
              гарантирует 5-кратное закрывающее устройство RC 3 класса. Высокие
              теплоизоляционные свойства обеспечены алюминиевой рамой 80 мм с
              тепловым разделением. Богатство мотивов, что составляет более 70
              видов - ThermoSafe удовлетворит любые требования.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/safe-1.jpg') center/contain no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-carbon-safe/safe-2.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/safe-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-carbon-safe/safe-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/THS_0504.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет мха RAL 6005, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 500, матовое стекло Float с
                  мотивом с 5 прозрачными поперечными полосами, тройное
                  теплоизоляционное остекление, коэффициент теплоизоляции UD до
                  0,9 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент MG 504: стекло с мотивом – Float, матовое, с 7
                  прозрачными поперечными полосами, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 504</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-1.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет Hörmann CH 607, каштановый, структурная
                  поверхность <br />
                  <br />
                  Ручка из нержавеющей стали HOE 500, орнаментное стекло
                  Satinato, тройное теплоизоляционное остекление, коэффициент
                  теплоизоляции UD до 0,9 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 502</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-2.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный белый цвет RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 700, коэффициент теплоизоляции
                  UD до 0,8 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 860</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-3.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет серого антрацита RAL 7016, структурная
                  поверхность <br />
                  <br />
                  Ручка из нержавеющей стали HOE 500, матовое стекло Float с
                  мотивом с 7 прозрачными поперечными полосами, тройное
                  теплоизоляционное остекление, коэффициент теплоизоляции UD до
                  0,9 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент MG 503: стекло с мотивом – Float, матовое, с 7
                  прозрачными поперечными полосами, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 503</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-4.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 500, стекло с мотивом: матовое
                  Float с 4 прозрачными полосами, тройное теплоизоляционное
                  остекление, коэффициент теплоизоляции UD до 0,9 Вт/(м²·K)*
                  <br />
                  <br />
                  Боковой элемент MG 505: стекло с мотивом – Float, матовое, с 4
                  прозрачными полосами, тройное теплоизоляционное остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 505</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-5.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 620, стекло с мотивом: матовое
                  Float с 7 прозрачными полосами, тройное теплоизоляционное
                  остекление, коэффициент теплоизоляции UD до 0,9 Вт/(м²·K)*
                  <br />
                  <br />
                  Боковой элемент MG 867: стекло с мотивом – Float, матовое, с 7
                  прозрачными полосами, тройное теплоизоляционное остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 867</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-6.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет серого камня RAL 7030, структурная
                  поверхность <br />
                  <br />
                  Ручка из нержавеющей стали HOE 500, орнаментное стекло
                  Satinato, тройное теплоизоляционное остекление, коэффициент
                  теплоизоляции UD до 0,9 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент: орнаментное стекло, тройное теплоизоляционное
                  остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 501</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-7.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный рубиново-красный цвет RAL 3003, матовый
                  <br />
                  <br />
                  Ручка из нержавеющей стали HOE 600, аппликация из нержавеющей
                  стали, стекло с мотивом: матовое Float с 7 прозрачными
                  полосами, тройное теплоизоляционное остекление, коэффициент
                  теплоизоляции UD до 0,9 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент MG 686: стекло с мотивом – Float, матовое, с 7
                  прозрачными полосами, тройное теплоизоляционное остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 686</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-8.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный белый цвет RAL 9016, матовый <br />
                  <br />
                  Ручка из нержавеющей стали HOE 620, коэффициент теплоизоляции
                  UD до 0,8 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент MG 872: стекло с мотивом Float, матовое, с 6
                  прозрачными полосами, тройное теплоизоляционное остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 872</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-6">
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-carbon-safe/safe/door-9.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет серого антрацита RAL 7016, структурная
                  поверхность <br />
                  <br />
                  Ручка из нержавеющей стали HOE 620, коэффициент теплоизоляции
                  UD до 0,8 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент MG 871: стекло с мотивом Float, матовое, с 3
                  прозрачными полосами, тройное теплоизоляционное остекление
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 871</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Эстетика дверного полотна с незримым профилем створки
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_USP_ohneFluegelprofil_feddd3cea7.jpg"
                alt=""
              />
              <p class="advantage_k_container_item_tabul_text">
                Утонченность и грацию дверей ThermoSafe, ThermoCarbon, Thermo65
                подчеркнет цельное, сплошное полотно с обеих сторон. Внутренне
                расположение профиля створки создает уникальную эстетичность
                формы, позволяющую гармонично сочетаться с любым дизайном.
                <br />
                Изысканность оформления обеспечивается скрытым расположением
                петель в серии дверей ThermoCarbon или ThermoSafe, где эта
                функция идет как дополнительная. Особенность позволяет
                усовершенствовать внешний вид изнутри, подчеркнуть благородство
                стиля, и не нарушить гармонию общей идеи оформления дома.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Эволюция теплоизоляции
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_USP_ueberragendeWaermedaemmung_3b4f174d7f.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Высочайшему качеству теплоизоляционных свойств, двери обязаны
                значительному показателю UD. ThermoCarbon - лучший образец
                идеальной теплоизоляции из представленных моделей. Дверное
                полотно Thermo65 характеризуется коэфициентом теплозащиты около
                0,87 Вт/‌(м²·К), впечатляют и показатели ThermoSafe (0,8
                Вт/‌(м²·К)). Уникальная технология сбережения тепла Hörmann
                соответствует европейским стандартам закона EnEV новой версии, и
                гарантирует качество на оптимальном уровне.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Противовзломная защита
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_UPS_einbruchhemmendeVerriegelung_b2f962552c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Защитная система гарантирует максимальную надежность в
                соответствии с индивидуальными потребностями. Двери
                ThermoCarbon, ThermoSafe задают принципиально новый темп в
                развитии противовзломной системы, выпускаясь с защитой класса RC
                3. Полотно ThermoCarbon характеризуется мощью с надежной защитой
                RC 4 класса. Модели TopComfort и Thermo65 кроме серийной
                комплектации, также могут быть оснащены RC 2 классом защиты.<br />
                Надежные блокировки и замки профессионально скрыты и никоим
                образом не влияют на внешний вид полотна входных дверей.
                Безопасность конструкции в сочетании с утонченностью и
                изысканностью дизайна является отличительной чертой продукции
                Hörmann.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Стекло в безопасном исполнении
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_UPS_sichereVerglasung_240938a235.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Одни из лучших в Европе показатели безопасности принадлежат
                входным дверям Hörmann. Наряду с профессионально закрывающими
                механизмами, двери оснащены многослойным стеклом VSG(8мм).
                Высокое качество защитного слоя позволяет обезопасить стекло от
                любых нежелательных вторжений и воздействий.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Электромеханика привода дверей
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_USP_komfortableTuerantriebe_72c9ed0f5b.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Hörmann гарантирует высочайшее качество до мельчайших деталей.
                Благодаря инновационной технологии электромеханической системы
                ECturn открытие и закрытие дверей легко управляемо с помощью
                пульта ДУ. Привод входит в комплектацию дверей ThermoCarbon и
                ThermoSafe. Ненавязчиво и надежно датчики показывают, открыты
                или заперты двери, а приложение BiSecur позволяет контролировать
                ситуацию из любой точки двора. Привод ECturn отличное решение
                для входных безбарьерных дверных систем.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Входные двери с роскошной высотой
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-carbon-safe/advantages/csm_USP_raumhoheHaustueren_4e2e95fd85.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Значительный вклад в атмосферу уюта и превосходства преподносят
                двери XXL. Высота ThermoSafe достигает 2,5 м, а вариации
                ThermoCarbon возможны в размере 3 м высотой. Исключительность в
                оформлении входа - стала доступной, благодаря изысканному
                сочетанию массивности и утонченности дизайнов дверей Hörmann.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/mW7a24MEoUE/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>

            <p class="videos_k_container_card_text">
              Приводы гаражных ворот – спокойствие и безопасность
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/k8lXfJWSgmg/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Привод гаражных ворот SupraMatic – Дуэль
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/s72U23NHptA/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Надежно, удобно, эксклюзивно: радиосистема BiSecur компании
              Хёрманн
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/YUnE6rc4YaQ/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>

            <p class="videos_k_container_card_text">
              Современные гаражные ворота и входные двери. Особенности
              конструкции.
            </p>
          </div>
        </div>
      </div>
    </section>
    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <div id="overlay"></div>
    
    <!--modal window-->

     
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
