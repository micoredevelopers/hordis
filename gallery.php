<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css">-->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body class="body_scroll">
  <?php include "header.php"?>
<main class="main-gallery">
    <h1 class="contacts-form-title text-center">Наши работы</h1>
    <div class="container">
        <div class="row">
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20цвет%208019.jpg"
                   data-caption="Входная дверь Hormann Thermo65 цвет 8019">
                    <img class="w-100" src="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20цвет%208019.jpg">Входная дверь Hormann Thermo65 цвет 8019</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20%20Темный%20дуб%20Decograin.jpg"
                   data-caption="Входная дверь Hormann Thermo65 Темный дуб Decograin">
                    <img class="w-100" src="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20%20Темный%20дуб%20Decograin.jpg">Входная дверь Hormann Thermo65 Темный дуб Decograin</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20%20Темный%20дуб%20Decograin%20мотив%20700А%20Крыжановка.jpg"
                   data-caption="Входная дверь Hormann Thermo65 Темный дуб Decograin мотив 700А Крыжановка">
                    <img class="w-100" src="./img/gallery/Входные%20двери/входная%20дверь%20Hormann%20Thermo65%20%20Темный%20дуб%20Decograin%20мотив%20700А%20Крыжановка.jpg">Входная дверь Hormann Thermo65 Темный дуб Decograin мотив 700А Крыжановка</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Входные%20двери/Дверь%20Термо%2065%20цвет%207016%20мотив%20700А.jpg"
                   data-caption="Дверь Термо 65 цвет 7016 мотив 700А">
                    <img class="w-100" src="./img/gallery/Входные%20двери/Дверь%20Термо%2065%20цвет%207016%20мотив%20700А.jpg">Дверь Термо 65 цвет 7016 мотив 700А</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20D-гофр%207016%20c%20нержавеющими%20вставками.jpg"
                   data-caption="Алюминиевая калитка Хьорман D-гофр 7016 c нержавеющими вставками">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20D-гофр%207016%20c%20нержавеющими%20вставками.jpg">Алюминиевая калитка Хьорман D-гофр 7016 c нержавеющими вставками</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20М-гофр%20Decograin%20Dark%20Oak%20(2).jpg"
                   data-caption="Алюминиевая калитка Хьорман М-гофр Decograin Dark Oak (2)">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20М-гофр%20Decograin%20Dark%20Oak%20(2).jpg">Алюминиевая калитка Хьорман М-гофр Decograin Dark Oak (2)</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20М-гофр%20Decograin%20Dark%20Oak%201.jpg"
                   data-caption="Алюминиевая калитка Хьорман М-гофр Decograin Dark Oak 1">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/Алюминиевая%20калитка%20Хьорман%20М-гофр%20Decograin%20Dark%20Oak%201.jpg">Алюминиевая калитка Хьорман М-гофр Decograin Dark Oak 1</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/Ворота%20консольного%20типа%20%20с%20заполнением%20М-гофр,%20цвет%20Темный%20дуб.jpg"
                   data-caption="Въездные ворота и калитки/Ворота консольного типа с заполнением М-гофр, цвет Темный дуб">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/Ворота%20консольного%20типа%20%20с%20заполнением%20М-гофр,%20цвет%20Темный%20дуб.jpg">Въездные ворота и калитки/Ворота консольного типа с заполнением М-гофр, цвет Темный дуб</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/ворота%20на%20опорном%20ролике%20с%20заполнением%20Хьорман%20и%20калитка%20Хьорман%20%20М-гофр%207016%20вид%20изнутри.jpg"
                   data-caption="Ворота на опорном ролике с заполнением Хьорман и калитка Хьорман М-гофр 7016 вид изнутри">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/ворота%20на%20опорном%20ролике%20с%20заполнением%20Хьорман%20и%20калитка%20Хьорман%20%20М-гофр%207016%20вид%20изнутри.jpg">Ворота на опорном ролике с заполнением Хьорман и калитка Хьорман М-гофр 7016 вид изнутри</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Въездные%20ворота%20и%20калитки/ворота%20на%20опорном%20ролике%20с%20заполнением%20Хьорман%20М-гофр%20цвет%207016.jpg"
                   data-caption="Ворота на опорном ролике с заполнением Хьорман М-гофр цвет 7016">
                    <img class="w-100" src="./img/gallery/Въездные%20ворота%20и%20калитки/ворота%20на%20опорном%20ролике%20с%20заполнением%20Хьорман%20М-гофр%20цвет%207016.jpg">Ворота на опорном ролике с заполнением Хьорман М-гофр цвет 7016</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decgrainr%20Dark%20Oak%203000x2500.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decgrainr Dark Oak 3000x2500">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decgrainr%20Dark%20Oak%203000x2500.jpg">Ворота секционные Хьорман М-гофр Decgrainr Dark Oak 3000x2500</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decgrainr%20Dark%20Oak%203000x2500%20вид%20изнутри.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decgrainr Dark Oak 3000x2500 вид изнутри">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decgrainr%20Dark%20Oak%203000x2500%20вид%20изнутри.jpg">Ворота секционные Хьорман М-гофр Decgrainr Dark Oak 3000x2500 вид изнутри</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decocolor%20Dark%20Oak%20c%20ручкой-замком.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decocolor Dark Oak c ручкой-замком">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decocolor%20Dark%20Oak%20c%20ручкой-замком.jpg">Ворота секционные Хьорман М-гофр Decocolor Dark Oak c ручкой-замком</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograine%20Dark%20Oak%202500x2500%20c%20Promatic.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decograine Dark Oak 2500x2500 c Promatic">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograine%20Dark%20Oak%202500x2500%20c%20Promatic.jpg">Ворота секционные Хьорман М-гофр Decograine Dark Oak 2500x2500 c Promatic</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20цвет%208028.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр цвет 8028">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20цвет%208028.jpg">Ворота секционные Хьорман М-гофр цвет 8028</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograin%20Golden%20Oak%201.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decograin Golden Oak">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograin%20Golden%20Oak%201.jpg">Ворота секционные Хьорман М-гофр Decograin Golden Oak </a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograin%20Golden%20Oak.jpg"
                   data-caption="Ворота секционные Хьорман М-гофр Decograin Golden Oak">
                    <img class="w-100" src="./img/gallery/Гаражные%20ворота/ворота%20секционные%20Хьорман%20М-гофр%20Decograin%20Golden%20Oak.jpg">Ворота секционные Хьорман М-гофр Decograin Golden Oak</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Межкомнатные%20двери/межкомнатная%20дверь%20с%20остеклением,%20покрытие%20Duradecor%209016.jpg"
                   data-caption="Межкомнатная дверь с остеклением, покрытие Duradecor 9016">
                    <img class="w-100" src="./img/gallery/Межкомнатные%20двери/межкомнатная%20дверь%20с%20остеклением,%20покрытие%20Duradecor%209016.jpg">Межкомнатная дверь с остеклением, покрытие Duradecor 9016</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Межкомнатные%20двери/межкомнатная%20дверь%20с%20покрытием%20Duradecor%209006%20и%20нержавеющими%20вставками.jpg"
                   data-caption="Межкомнатная дверь с покрытием Duradecor 9006 и нержавеющими вставками">
                    <img class="w-100" src="./img/gallery/Межкомнатные%20двери/межкомнатная%20дверь%20с%20покрытием%20Duradecor%209006%20и%20нержавеющими%20вставками.jpg">Межкомнатная дверь с покрытием Duradecor 9006 и нержавеющими вставками</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Промышленные%20ворота/промышленные%20ворота%20со%20встроенной%20калиткой.jpg"
                   data-caption="Промышленные ворота со встроенной калиткой">
                    <img class="w-100" src="./img/gallery/Промышленные%20ворота/промышленные%20ворота%20со%20встроенной%20калиткой.jpg">Промышленные ворота со встроенной калиткой</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Промышленные%20ворота/Промышленные%20панорамные%20алюминиевые%20ворота%202.jpg"
                   data-caption="Промышленные панорамные алюминиевые ворота">
                    <img class="w-100" src="./img/gallery/Промышленные%20ворота/Промышленные%20панорамные%20алюминиевые%20ворота%202.jpg">Промышленные панорамные алюминиевые ворота</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Промышленные%20ворота/Промышленные%20панорамные%20алюминиевые%20ворота.jpg"
                   data-caption="Промышленные панорамные алюминиевые ворота">
                    <img class="w-100" src="./img/gallery/Промышленные%20ворота/Промышленные%20панорамные%20алюминиевые%20ворота.jpg">Промышленные панорамные алюминиевые ворота</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery"
                   href="./img/gallery/Автоматика/наружные%20фотоэлементы%20Хьорман.jpg"
                   data-caption="Наружные фотоэлементы Хьорман"
                >
                    <img class="w-100" src="./img/gallery/Автоматика/наружные%20фотоэлементы%20Хьорман.jpg">Наружные фотоэлементы Хьорман</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery"
                   href="./img/gallery/Автоматика/Опережающие%20световые%20барьеры%20VL2%20для%20промышленных%20ворот.jpg"
                   data-caption="Опережающие световые барьеры VL2 для промышленных ворот"
                >
                    <img class="w-100" src="./img/gallery/Автоматика/Опережающие%20световые%20барьеры%20VL2%20для%20промышленных%20ворот.jpg">Опережающие световые барьеры VL2 для промышленных ворот</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery"
                   href="./img/gallery/Автоматика/Привод%20Hormann%20LineaMatic%20с%20фотоэлементами%20и%20сигнальной%20лампой.jpg"
                   data-caption="Привод Hormann LineaMatic с фотоэлементами и сигнальной лампой"
                >
                    <img class="w-100" src="./img/gallery/Автоматика/Привод%20Hormann%20LineaMatic%20с%20фотоэлементами%20и%20сигнальной%20лампой.jpg">Привод Hormann LineaMatic с фотоэлементами и сигнальной лампой</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery"
                   href="./img/gallery/Автоматика/Привод%20Hormann%20ProMatic%20для%20гаражных%20%20ворот.jpg"
                   data-caption="Привод Hormann ProMatic для гаражных ворот">
                    <img class="w-100 " src="./img/gallery/Автоматика/Привод%20Hormann%20ProMatic%20для%20гаражных%20%20ворот.jpg">Привод Hormann ProMatic для гаражных ворот</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery"
                   href="./img/gallery/Автоматика/Привод%20Hormann%20ProPort%20S%20с%20фотоэлементами%20и%20сигнальной%20лампой.jpg"
                   data-caption="Привод Hormann ProPort S с фотоэлементами и сигнальной лампой">
                    <img class="w-100" src="./img/gallery/Автоматика/Привод%20Hormann%20ProPort%20S%20с%20фотоэлементами%20и%20сигнальной%20лампой.jpg">Привод Hormann ProPort S с фотоэлементами и сигнальной лампой</a>
            </div>
            <div class="mb-5 mt-5 col-12 col-md-6 col-lg-4">
                <a data-fancybox="gallery" href="./img/gallery/Автоматика/сигнальная%20лампа%20Хьорман.jpg"
                   data-caption="Сигнальная лампа Хьорман">
                    <img class="w-100" src="./img/gallery/Автоматика/сигнальная%20лампа%20Хьорман.jpg">Сигнальная лампа Хьорман</a>
            </div>
        </div>
    </div>

    <?php include "formTemplates/formFooter.php" ?>

</main>

     <?php include "footer.php"?>
    <div id="overlay"></div>
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
