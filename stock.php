<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Акции</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>


    <!--tabs-->
    <section>
      <div class="pt container">
        <h1 class="stock-main-title">
          Акционные предложения, которые не оставят Вас безразличным
        </h1>
        <div class="stock-tabs-box-wrap">
          <div class="stock-tabs-menu row">
            <div class="col-6 col-sm-6 col-lg-2 stock-tabs-menu-link-wrap">
              <a class=" stock-tabs-menu-link stock-tabs-menu-link-active"
                >Гаражные секционные ворота<br><strong>RenoMatic 2019</strong></a
              >
            </div>
            <div class="col-6 col-sm-6 col-lg-2 stock-tabs-menu-link-wrap">
              <a class=" stock-tabs-menu-link "
                >Входные двери<br> <strong>Thermo65</strong></a
              >
            </div>
            <div class="col-6 col-sm-6 col-lg-2 stock-tabs-menu-link-wrap">
              <a class=" stock-tabs-menu-link ">Входные двери<br> <strong>Thermo46</strong></a>
            </div>
            <div class="col-6 col-sm-6 col-lg-2 stock-tabs-menu-link-wrap">
              <a class=" stock-tabs-menu-link ">Приводы въездных ворот<br> <strong>ProPort &amp; LineaMatic &amp; RotaMatic </strong></a>
            </div>
            <div class="col-6 col-sm-6 col-lg-2 stock-tabs-menu-link-wrap">
              <a class=" stock-tabs-menu-link ">Умный дом<br> <strong>SmartKey &amp; HKSI</strong></a>
            </div>
          </div>
          <div class="stock-tabs-content-wrap">
            <div class="stock-tabs-content row">
              <div class="col-12 second-tab-item row">
                <div class="col-12 row mb-5">
                  <div class="col-12 mb-3 mb-md-0 col-md-6">
                    <h2 class="stock-tabs-content-title mt-0 mb-3">
                      Гаражные секционные ворота RenoMatic 2019
                    </h2>
                    <p class="guaranty-list-item">вкл. гарнитуры ручек, привод ProLift, ProMatic или SupraMatic</p>
                  </div>
                  <div class="col-12 mb-3 mb-md-0 col-md-6">
                    <div class="row">
                      <div class="col-6">
                        <h2 class="stock-tabs-content-title mt-0 mb-3">Выбранная поверхность</h2>
                        <p class="guaranty-list-item surface ">Woodgrain</p>
                      </div>
                      <div class="col-6">
                        <h2 class="stock-tabs-content-title mt-0 mb-3 text-right">Выбранный цвет</h2>
                        <p class="guaranty-list-item text-right door-color">Белый алюминий</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=" col-12 col-lg-6  ">
                  <img class="w-100  main-tab-img" src="./img/woodgrain/woodgrain-white.png" alt="">
                </div>
                <div class=" col-12 col-lg-6 ">
                  <table class="stock-tabs-table">
                    <tr>
                      <th class="p-1">Акционний размер</th>
                      <th class="p-1">вкл. гарнитуры ручек</th>
                      <th class="p-1">вкл. привод ProLift</th>
                    </tr>
                    <tr>
                      <td>2500 × 2125 мм, 2500 × 2250</td>
                      <td>20 475 грн.*</td>
                      <td>27 125 грн.*</td>
                    </tr>
                    <tr>
                      <td>2500 × 2500 мм</td>
                      <td>21 350 грн.*</td>
                      <td>29 925 грн.*</td>
                    </tr>
                    <tr>
                      <td>2750 × 2125 мм, 2750 × 2250 мм </td>
                      <td>23 625 грн.*</td>
                      <td>30 625 грн.*</td>
                    </tr>
                    <tr>
                      <td>2750 × 2500 мм</td>
                      <td>25 200 грн.*</td>
                      <td>33 950 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 2125 мм,3000 × 2250 мм</td>
                      <td>25 550 грн.*</td>
                      <td>32 375 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 2500 мм</td>
                      <td>27 125 грн.*</td>
                      <td>35 875 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 3000 мм</td>
                      <td>32 900 грн.*</td>
                      <td>42 000 грн.*</td>
                    </tr>
                    <tr>
                      <td>3500 × 2125 мм, 3500 × 2250 мм</td>
                      <td>28 350 грн.*</td>
                      <td>34 650 грн.*</td>
                    </tr>
                    <tr>
                      <td>3500 × 2500 мм,4000 × 2125 мм, 4000 × 2500 мм,</td>
                      <td>30 100 грн.*</td>
                      <td>37 100 грн.*</td>
                    </tr>
                    <tr>
                      <td>4000 × 2500мм, 5000 × 2125 мм, 5000 × 2250 мм</td>
                      <td>35 000 грн.*</td>
                      <td>42 000 грн.*</td>
                    </tr>
                    <tr>
                      <td>5000 × 2500 мм</td>
                      <td>40 250 грн.**</td>
                      <td>51 800 грн.**</td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="col-12 second-tab-item row">
                <div class="col-12 row mb-5">
                  <div class="col-12 mb-3 mb-md-0 col-md-6">
                    <h2 class="stock-tabs-content-title mt-0 mb-3">
                      Гаражные секционные ворота RenoMatic 2019
                    </h2>
                    <p class="guaranty-list-item">вкл. гарнитуры ручек, привод ProLift, ProMatic или SupraMatic</p>
                  </div>
                  <div class="col-12 mb-3 mb-md-0 col-md-6">
                    <div class="row">
                      <div class="col-6">
                        <h2 class="stock-tabs-content-title mt-0 mb-3">Выбранная поверхность</h2>
                        <p class="guaranty-list-item surface">Planar</p>
                      </div>
                      <div class="col-6">
                        <h2 class="stock-tabs-content-title mt-0 mb-3 text-right">Выбранный цвет</h2>
                        <p class="guaranty-list-item text-right door-color">Белый цвет.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-6 ">
                  <img class="w-100 main-tab-img" src="./img/decolor_planar/planar-white.png" alt="">
                </div>
                <div class="col-12 col-lg-6">
                  <table class="stock-tabs-table">
                    <tr>
                      <th class="p-1">Акционный размер</th>
                      <th class="p-1">вкл. привод ProMatic</th>
                    </tr>
                     <tr>
                         <td>2500 × 2125 мм, 2500 × 2250 мм</td>
                         <td>32 375 грн.*</td>
                     </tr>
                    <tr>
                      <td>2500 × 2500 мм</td>
                      <td>36 050 грн.*</td>
                    </tr>
                    <tr>
                      <td>2750 × 2125 мм, 2750 × 2250 мм</td>
                      <td>35 000 грн.*</td>
                    </tr>
                    <tr>
                      <td>2750 × 2500 мм	</td>
                      <td>38 500 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 2125 мм, 3000 × 2250 мм</td>
                      <td>36 750 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 2500 мм</td>
                      <td>40 600 грн.*</td>
                    </tr>
                    <tr>
                      <td>3000 × 3000 мм</td>
                      <td>47 600 грн.*</td>
                    </tr>
                    <tr>
                      <td>3500 × 2125 мм, 3500 × 2250 мм</td>
                      <td>39 900 грн.*</td>
                    </tr>
                    <tr>
                      <td>3500 × 2500 мм, 4000 × 2125 мм, 4000 × 2250 мм</td>
                      <td>43 050 грн.*</td>
                    </tr>
                    <tr>
                      <td>4000 × 2500 мм, 5000 × 2125 мм, 5000 × 2250 мм</td>
                      <td>50 750 грн.*</td>
                    </tr>
                    <tr>
                      <td>5000 × 2500 мм</td>
                      <td>	62 300 грн.**</td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="col-12 mb-5">
                <div class="sub-trigger row mt-5">
                  <div class="col-12 col-lg-6 type-btn-wrap">
                    <div class="type-btn woodgrain btn-active">
                      <img class="w-100" src="./img/woodgrain/woodgrain-white.png" alt="">
                    </div>
                    <div class="type-btn planar">
                      <img class="w-100" src="./img/decolor_planar/planar-white.png" alt="">
                    </div>
                  </div>
                  <div class="color-btns offset-lg-1 col-12 col-lg-4">
                    <div class="row">
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-white.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-grey.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-darkGrey.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-antracit.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-greyAntracit.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-brown.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn  mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-gold.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-darkDoob.png" alt="">
                      </div>
                      <div data-name="woodgrain" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/woodgrain/woodgrain-nightDoob.png" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="color-btns offset-lg-1 col-12 col-lg-4 ">
                    <div class="row">
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-white.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-grey.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-darkgrey.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-antracitMetal.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-antracitGrey.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/planar-brown.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn  mb-3">
                        <img class="w-100" src="./img/decolor_planar/decolor-goldDoob.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/decolor-darkDoob.png" alt="">
                      </div>
                      <div data-name="planar" class="col-4 color-btn mb-3">
                        <img class="w-100" src="./img/decolor_planar/decolor-nightDoob.png" alt="">
                      </div>
                    </div>
                  </div>
                </div>
                <p class="mt-3">* Рекомендованая цена с учетом ПДВ на акционные размеры продукции без учета монтажа.Цена может быть изменена в случае превышения официального курса НБУ 35 грн. / 1 євро.
                </p>
                <p class="mt-3">
                    ** Цена за ворота с приводом SupraMatic 3.
                </p>
              </div>
              <div class="guaranty-wrap container p-4 mb-5">
                  <div class="row">
                      <div class="col-12 col-lg-6 mb-5">
                          <ul class="guaranty-list">
                              <li class="guaranty-list-item mb-3">- Двустенные утепленные секции толщиной 42 мм для высокоэффективной теплоизоляции, стабильности и приятного бесшумного движения</li>
                              <li class="guaranty-list-item mb-3">- 19 размеров ворот по акции</li>
                              <li class="guaranty-list-item mb-3">- 18 вариантов дизайна поверхности на выбор</li>
                              <li class="guaranty-list-item mb-3">- Только в Hörmann: надежная защита от подваживания ворот благодаря механическому блокированию в тяге</li>
                              <li><img src="./img/new-tab/tabs-sect-new-1.png" alt=""></li>
                          </ul>
                      </div>
                      <div class="col-12 col-lg-6">
                          <div class="row align-items-center mb-5">
                              <div class="col-4"><img class="w-100" src="./img/new-tab/new-tab-woodgrain-white.png" alt=""></div>
                              <div class="col-8">
                                  <p class="guaranty-list-item">
                                     Поверхность Woodgrain на воротах с М-гофрами отличается точным отражением среза древесины и высокой устойчивостью (рис. Слева в белом цвете RAL 9016)
                                  </p>
                              </div>
                          </div>
                          <div class="row align-items-center mb-5">
                              <div class="col-4"><img class="w-100" src="./img/new-tab/new-tab-planar-antr-met.png" alt=""></div>
                              <div class="col-8">
                                  <p class="guaranty-list-item">
                                      НОВИНКА: гладкая поверхность Planar для ворот с L-гофрами, в 6 эксклюзивных цветах Hörmann Matt deluxe имеет чрезвычайный элегантный вид (рис. слева в цвете CH 703 Matt deluxe, антрицит металлик)
                                  </p>
                              </div>
                          </div>
                          <div class="row align-items-center mb-5">
                              <div class="col-4"><img class="w-100" src="./img/new-tab/new-tab-aktion-decocolor.png" alt=""></div>
                              <div class="col-8">
                                  <p class="guaranty-list-item">
                                      Крашеная поверхность Decocolor привлекает подобию натуральной древесины в трех исполнениях (рис. cлева крашеная поверхность под "золотой дуб")
                                  </p>
                              </div>
                          </div>
                          <div class="row align-items-center mb-5">
                              <div class="col-4"><img class="w-100" src="./img/new-tab/new-tab-aktion-preview.png" alt=""></div>
                              <div class="col-8">
                                  <p class="guaranty-list-item">
                                      Внутренняя поверхность всех ворот выполнена из оцинкованной стали серебристого цвета и покрыта защитным лаком
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container ">
                  <div class="row">
                      <div class="col-12 col-lg-6">
                          <img class="w-100 mb-3" src="./img/new-tab/new-tab-promatic.jpg" alt="">
                          <p class="guaranty-list-item">
                              C современной технологией кодирования радиосигнала BiSecur и дополнительной функцией проветривания гаража, в комплекте 4-клавишный пульт дистанционного управления HSE 4 BS черного цвета со структурной матовой поверхностью
                          </p>
                      </div>
                      <div class="col-12 col-lg-6">
                          <img class="w-100 mb-3" src="./img/new-tab/new-tab-prolift-700.jpg" alt="">
                          <p class="guaranty-list-item">
                              В комплекте 2 пульта дистанционного
                              управления изысканной формы с кольцом
                              для ключей
                          </p>
                      </div>
                  </div>
              </div>
            </div>
            <div class="stock-tabs-content ">
                <div class="door-box row">
                  <div class="col-12 mb-5">
                    <div class="row ">
                      <div class="col-6">
                        <h2 class="stock-tabs-content-title mt-0 mb-3">
                          Входные двери Thermo65
                        </h2>
                      </div>
                      <div class="col-6 text-right">
                        <h2 class="stock-tabs-content-title mt-0 mb-3">Выбранный цвет</h2>
                        <p class="guaranty-list-item color-doors">RAL 9016 белый цвет</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6">
                      <div class="doors-tab-item">
                        <div class="row">
                          <div class="col-12">
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP-010/THP010white.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">47 000 грн*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 010</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP015/THP015belii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">47 000 грн*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 015</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP515/THP515belii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">47 000 грн*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 515</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP700S/THP700Sbelii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">59 500 грн.*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 700S</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP810/THP810Sbelii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">59 500 грн.*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 810S</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP900/THP900Sbelii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">59 500 грн.*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 900S</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP700/THP700Abelii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">47 000 грн*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 700A</h3>
                                </div>
                              </div>
                            </div>
                            <div class="dor-tab-item">
                              <div class="tab-img-wrap text-center">
                                <img class="w-50" src="./img/THP750/THP750Fbelii.jpg" alt="">
                              </div>
                              <div class="row text-center">
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Выбранный мотив</h3>
                                  <p class="price-block__price">47 000 грн*</p>
                                </div>
                                <div class="col-6">
                                  <h3 class="stock-tabs-content-title">Мотив THP 750F</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div class="row">
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP-010/THP010white.jpg" alt="">
                      </div>
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP015/THP015belii.jpg" alt="">
                      </div>
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP515/THP515belii.jpg" alt="">
                      </div>
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP700/THP700Abelii.jpg" alt="">
                      </div>
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP810/THP810Sbelii.jpg" alt="">
                      </div>
                      <div class="col-4 doors-btn text-center mb-2">
                        <img class="w-75 text-center" src="./img/THP900/THP900Sbelii.jpg" alt="">
                      </div>
                      <div class="col-6 doors-btn text-center mb-2">
                        <img class="w-50 " src="./img/THP700S/THP700Sbelii.jpg" alt="">
                      </div>
                      <div class="col-6 doors-btn text-center mb-2">
                        <img class="w-50 " src="./img/THP750/THP750Fbelii.jpg" alt="">
                      </div>
                    </div>

                  </div>
                  <div class="col-12">
                    <div class="colors-box">
                      <div class="colors-box-item"><img data-color="white" class="w-100" src="./img/colors/white.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="antracGrey" class="w-100" src="./img/colors/antracitgrey.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="brown" class="w-100" src="./img/colors/brown.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="antracMetal" class="w-100" src="./img/colors/antracitMetal.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="gold" class="w-100" src="./img/colors/gold.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="dark" class="w-100" src="./img/colors/darkDoob.jpg" alt=""></div>
                      <div class="colors-box-item"><img data-color="night" class="w-100" src="./img/colors/nightDoob.jpg" alt=""></div>
                    </div>
                    <p class="mt-3">* Рекомендованая цена с учетом ПДВ на акционные размеры продукции без учета монтажа.Цена может быть изменена в случае превышения официального курса НБУ 35 грн. / 1 євро.
                      </p>
                  </div>
                </div>
                <div class="guaranty-wrap container mt-5 p-4">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <ul class="guaranty-list pl-3 pr-3">
                                <li class="guaranty-list-item mb-3">-  Сплошное дверное полотно без видимого профиля створки</li>
                                <li class="guaranty-list-item mb-3">- Стальное дверное полотно толщиной 65 мм
                                    с термическим разделением и заполнением с
                                    твердого полиуретана</li>
                                <li class="guaranty-list-item mb-3">- Алюминиевая коробка толщиной 80 мм с термическим разделением
                                    в 4-х акционных цветах и ​​4-х декорах Decograin с обеих сторон</li>
                                <li class="guaranty-list-item mb-3">- Замок с 5-точечным закрыванием с повышенным
                                    уровнем защиты от взлома RC 2 серийно
                                    (Для THP 700A и THP 750 F опция)
                                    показатель теплоизоляции
                                    UD = 0,87 Вт / (м² ∙ K) **</li>
                                <li class="guaranty-list-item mb-3">- Акционные размеры дверей до 1250 х 2250 мм
                                    к двери Thermo65 можно заказать боковые
                                    элементы и окна верхнего света</li>
                            </ul>
                            <p class="guaranty-list-item mb-3">
                                Новинка: акционные двери Thermo65 можно заказать в шести эксклюзивных цветах Hormann Matt deluxe, подобных цвета ворот RenoMatic
                            </p>
                        </div>
                        <div class="col-12 col-lg-6">
                            <h4 class="stock-tabs-content-title mt-0 mb-3">Красивая ручка подчеркивает внешний вид: выберите желаемую ручку</h4>
                            <div class="row mb-5 align-items-center">
                                <div class="col-4">
                                    <img class="w-100" src="./img/new-tab/new-tab-ru4ka-1.png" alt="">
                                </div>
                                <div class="col-8">
                                    <p class="guaranty-list-item">
                                        Гарнитур ручек ES 0 / ES 1 с внешней круглой ручкой-кнопкой
                                    </p>
                                </div>
                            </div>
                            <div class="row mb-5 align-items-center">
                                <div class="col-4">
                                    <img class="w-100" src="./img/new-tab/new-tab-ru4ka-2.png" alt="">
                                </div>
                                <div class="col-8">
                                    <p class="guaranty-list-item">
                                        Гарнитур нажимных ручек ES 0 / ES 1
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stock-tabs-content">
                <div class="row mb-5">
                  <div class="col-12 col-md-6">
                    <h2 class="stock-tabs-content-title mt-0 mb-3">
                        Входные двери Thermo46
                    </h2>
                  </div>
                  <div class="col-12 col-md-6 d-flex justify-content-between">
                    <div class="type-door">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">
                          Выбранный мотив
                      </h2>
                      <p class="guaranty-list-item">
                        Мотив TPS 010
                      </p>
                    </div>
                    <div class="type-of-color">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">
                          Выбранный цвет
                      </h2>
                      <p class="guaranty-list-item TPS-color">
                        RAL 9016
                        белый цвет
                      </p>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                      <div class="door-container">
                        <img class="door-container-img" src="./img/TPS010/TPS10white.jpg" alt="" class="w-50">
                      </div>
                  </div>
                  <div class="col-12 col-md-6 d-flex flex-column justify-content-center">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">
                          Входные двери Thermo46
                      </h2>
                      <p class="price-block__price TPS-price">28 000 грн.*</p>
                      <div class="row mt-5">
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/white.jpg" alt="">
                          </div>
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/antracitgrey.jpg" alt="">
                          </div>
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/brown.jpg" alt="">
                          </div>
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/antracitMetal.jpg" alt="">
                          </div>
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/gold.jpg" alt="">
                          </div>
                          <div class="mb-3 col-4 colors-btn">
                              <img class="w-100" src="./img/colors/darkDoob.jpg" alt="">
                          </div>
                      </div>
                  </div>
                  <div class="col-12">
                      <p class="mt-3">* Рекомендованая цена с учетом ПДВ на акционные размеры продукции без учета монтажа.Цена может быть изменена в случае превышения официального курса НБУ 35 грн. / 1 євро.
                      </p>
                  </div>
                </div>
                <div class="guaranty-wrap container mt-5 p-4">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <ul class="guaranty-list pl-3 pr-3">
                                <li class="guaranty-list-item mb-3">- Стальное дверное полотно толщиной 46 мм с термическим
                                    разделением и заполнением из твердого полиуретана</li>
                                <li class="guaranty-list-item mb-3">Алюминиевая прямоугольная коробка толщиной 60 мм
                                    с термическим разделением</li>
                                <li class="guaranty-list-item mb-3">-Противовзломная система с несколькими механизмами
                                    блокировки</li>
                                <li class="guaranty-list-item mb-3">- Показатель теплоизоляции UD = 1,2 Вт / (м² ∙ K) **/li>
                                <li class="guaranty-list-item mb-3">- Лаковое покрытие поверхности в 3-х цветах и 3-х декорах Decograin внутри и снаружи</li>
                                <li class="guaranty-list-item mb-3">- Может поставляться с глазком (опция)</li>
                                <li class="guaranty-list-item mb-3">- Акционные размеры дверей до 1250 х 2250 мм</li>
                            </ul>

                        </div>
                        <div class="col-12 col-lg-6">
                            <h4 class="stock-tabs-content-title mt-0 mb-3">Красивая ручка подчеркивает внешний вид: выберите желаемую ручку</h4>
                            <div class="row mb-5 align-items-center">
                                <div class="col-4">
                                    <img class="w-100" src="./img/new-tab/new-tab-ru4ka-1.png" alt="">
                                </div>
                                <div class="col-8">
                                    <p class="guaranty-list-item">
                                        Гарнитур ручек ES 0 / ES 1 с внешней круглой ручкой-кнопкой
                                    </p>
                                </div>
                            </div>
                            <div class="row mb-5 align-items-center">
                                <div class="col-4">
                                    <img class="w-100" src="./img/new-tab/new-tab-ru4ka-2.png" alt="">
                                </div>
                                <div class="col-8">
                                    <p class="guaranty-list-item">
                                        Гарнитур нажимных ручек ES 0 / ES 1
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stock-tabs-content">
                <div class="row mb-5">
                    <div class="col-12 col-md-6 mb-3">
                        <img src="./img/privod1.png" alt="" class="w-100">
                    </div>
                    <div class="col-12 col-md-6">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">Привод распашных ворот ProPort D</h2>
                      <hr>
                      <ul class="guaranty-list">
                        <li class="guaranty-list-item"> - Макс. тяговое усилие 800 Н</li>
                        <li class="guaranty-list-item"> - Надежная радиочастота 433 МГц RC</li>
                        <li class="guaranty-list-item"> - Размеры ворот до макс. 5 м (2 х 2,5 м)</li>
                        <li class="guaranty-list-item"> - Синхронное открывание створок (при их одинаковой ширине)</li>
                        <li class="guaranty-list-item"> - Открытие одной створки для прохода людей</li>
                      </ul>
                      <hr>
                      <p class="price-block__price TPS-price">15 000 грн.*</p>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-12 col-md-6 mb-3">
                        <img src="./img/privod2.png" alt="" class="w-100">
                    </div>
                    <div class="col-12 col-md-6">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">Привод откатных ворот ProPort S</h2>
                      <hr>
                      <ul class="guaranty-list">
                        <li class="guaranty-list-item"> - Макс. тяговое усилие 600 Н</li>
                        <li class="guaranty-list-item"> - Надежная радиочастота 433 МГц RC</li>
                        <li class="guaranty-list-item"> - Размер ворот макс. 4 м</li>
                        <li class="guaranty-list-item"> - Частичное открывание ворот для прохода людей</li>
                      </ul>
                      <hr>
                      <p class="price-block__price TPS-price">11 200 грн.*</p>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-12 col-md-6 mb-3">
                        <img src="./img/privod3.jpg" alt="" class="w-100">
                    </div>
                    <div class="col-12 col-md-6">
                      <h2 class="stock-tabs-content-title mt-0 mb-3"> Привод откатных ворот LineaMatic P</h2>
                      <hr>
                      <ul class="guaranty-list">
                        <li class="guaranty-list-item"> - Привод откатных ворот с современной технологией кодирования радиосигнала BiSecur</li>
                        <li class="guaranty-list-item"> - Для ворот высотой до 2000 мм и шириной до 8000 мм створки до 500 кг</li>
                        <li class="guaranty-list-item"> - Вес створки до 500 кг</li>
                        <li class="guaranty-list-item"> - Функция плавного пуска и плавной остановки</li>
                        <li class="guaranty-list-item"> - Регулируемое ограничение усилия</li>
                        <li class="guaranty-list-item"> - Возможность программирования функции для прохода людей</li>
                        <li class="guaranty-list-item"> - Профили зубчатых реек нужно заказывать отдельно в зависимости от выполнения / ширины ворот</li>
                      </ul>
                      <hr>
                      <p class="price-block__price TPS-price">15 000 грн.*</p>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-12 col-md-6 mb-3">
                        <img src="./img/privod1.png" alt="" class="w-100">
                    </div>
                    <div class="col-12 col-md-6">
                      <h2 class="stock-tabs-content-title mt-0 mb-3">Привод распашных ворот RotaMatic 2</h2>
                      <hr>
                      <ul class="guaranty-list">
                        <li class="guaranty-list-item"> - Надежный электромеханический привод распашных ворот с современной технологии кодирования радиосигнала BiSecur</li>
                        <li class="guaranty-list-item"> - для 2-створчатых распашных ворот высотой до 2000 мм и шириной створки до 2500 мм</li>
                        <li class="guaranty-list-item"> - Вес створки до 220 кг</li>
                        <li class="guaranty-list-item"> - Функция плавного пуска и плавной остановки</li>
                        <li class="guaranty-list-item"> - Регулируемое ограничение усилия</li>
                        <li class="guaranty-list-item"> - Возможность программирования функции проходной створки</li>
                      </ul>
                      <hr>
                      <p class="price-block__price TPS-price">22 000 грн.*</p>
                    </div>
                </div>
                <p class="mt-3">* Рекомендованая цена с учетом ПДВ на акционные размеры продукции без учета монтажа.Цена может быть изменена в случае превышения официального курса НБУ 35 грн. / 1 євро.
                </p>
            </div>
            <div class="stock-tabs-content">
              <div class="row mb-5">
                <div class="col-12 col-md-6 mb-3 text-center">
                  <img src="./img/SmartKey-small-1.jpg" alt="" style="width: 140px">
                </div>
                <div class="col-12 col-md-6">
                  <h2 class="stock-tabs-content-title mt-0 mb-3">Привод дверного замка с дистанционным управлением SmartKey</h2>
                  <hr>
                  <ul class="guaranty-list">
                    <li class="guaranty-list-item"> - Идеально для дооснащения входных дверей квартир и коттеджей</li>
                    <li class="guaranty-list-item"> - Простое открывание дверного замка: напр. когда руки заняты пакетами с покупками или необходимо открыть дверь на расстоянии гостям или детям</li>
                    <li class="guaranty-list-item"> - Комфортное управление с помощью пульта дистанционного управления, внутреннего выключателя или непосредственно на приводе SmartKey</li>
                  </ul>
                  <hr>
                  <p class="price-block__price TPS-price">від 7 000 грн.*</p>
                </div>
              </div>
              <div class="row mb-5">
                <div class="col-12 col-md-6 mb-3 text-center">
                  <img src="./img/HKSI-small-2.jpg" alt="" style="width: 200px">
                </div>
                <div class="col-12 col-md-6">
                  <h2 class="stock-tabs-content-title mt-0 mb-3">Климатический датчик HKSI</h2>
                  <hr>
                  <ul class="guaranty-list">
                    <li class="guaranty-list-item"> - Он действительно предупреждает появление плесени в гараже</li>
                    <li class="guaranty-list-item"> - Контролирует влажность воздуха в гараже и автоматически регулирует проветривания</li>
                    <li class="guaranty-list-item"> - Только в комплекте с приводом SupraMatic 3 (прошивка с версии С)</li>
                  </ul>
                  <hr>
                  <p class="price-block__price TPS-price">3 325 грн.*</p>
                </div>
              </div>
              <p class="mt-3">
                  * Рекомендованая цена с учетом ПДВ на акционные размеры продукции без учета монтажа.Цена может быть изменена в случае превышения официального курса НБУ 35 грн. / 1 євро.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
<!--





-->
    <!--garanty section-->
<!--    <section class="guaranty-wrap">-->
<!--      <div class="my-container guaranty row">-->
<!--        <div class="col-lg-6">-->
<!--          <div class="guaranty-item">-->
<!--            <h3 class="guaranty-title">10 лет гарантии на ворота</h3>-->
<!--            <ul class="guaranty-list">-->
<!--              <li class="guaranty-list-item">-->
<!--                — двустенные утепленные секции толщина 42 мм для высокоэффективной теплоизоляции, стабильности и приятного бесшумно движения-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Внешняя поверхность Woodgrain с нежной структурой в 3 цветах или УФ-стойкая поверхность Decocolor в 3 вариантах оформления-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Оптимальная долговечная защита коробки с помощью антикорозийнои пластмассовой опоры-->
<!--              </li>-->
<!--            </ul>-->
<!--          </div>-->
<!--        </div>-->
<!--        <div class="col-lg-6">-->
<!--          <div class="guaranty-item">-->
<!--            <h3 class="guaranty-title">5 лет гарантии на привода</h3>-->
<!--            <ul class="guaranty-list">-->
<!--              <li class="guaranty-list-item">-->
<!--                <span>Привод для гаражных ворот ProLift 700</span>-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Очень тихая технология зубчатого ремня-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Надежная система автоматического выключения-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">— Надежная радиочастота 433 МГц</li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Проверено и сертифицировано в сочетании с гаражными воротами Hörmann по стандарту ЕС 13241-1-->
<!--              </li>-->
<!--              <li class="guaranty-list-item">-->
<!--                — Два 2-канальные пульты дистанционного управления красивой формы с хромированным кольцом для ключей-->
<!--              </li>-->
<!--            </ul>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </section>-->

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>

    <div id="overlay"></div>
      
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
    <script src="./js/script.js"></script>
  </body>
</html>
