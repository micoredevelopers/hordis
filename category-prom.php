<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css"> -->
    <!-- <link rel="stylesheet" href="slick/slick-theme.css"> -->
    <!-- <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>
    <section
      class="main_k"
      style="background:url('img/category/industrial.jpg') center/cover no-repeat;"
    >
      <div class="wrapper_k">
        <div class="main_k_container">
          <p class="main_k_container_title">Для промышленности и бизнеса</p>
        </div>
      </div>
    </section>

    <section class="cost_k">
      <div class="wrapper_k">
        <div class="cost_k_container row">
          <div class="col-12 col-md-6">
            <img src="./img/second-fotо.jpg" alt="" class="w-100" />
          </div>
          <div class="col-12 col-md-6">
               <div class="cost_k_container_box">
              <p class="box_title">
                Продукция Hörmann для промышленных и коммерческих помещений в
                Одессе
              </p>
              <p class="box_desc mb-1">
                Немецкий производитель представлен в ШОУ-РУМе Одессы, где можно
                увидеть и получить компетентные консультации по всем вопросам
                качества к:
              </p>
              <ul class="pl-4">
                <li class="box_desc mb-1">
                  Рулонным воротам, дверям и решеткам;
                </li>
                <li class="box_desc mb-1">Секционным и скоростным воротам;</li>
                <li class="box_desc mb-1">
                  Перегрузочному оборудованию, а также к системам автоматики и
                  другим элементам, что производит компания Hörmann.
                </li>
                <li class="box_desc mb-1">
                  Установкой по городу Одессе и близлежащим районам, и дальнейшим
                  сервисным обслуживанием - обеспечивает компания Hördis. В
                  соответствующем разделе сайта, есть все контакты организации.
                </li>
              </ul>
              <p class="box_desc">
                Установкой по городу Одессе и близлежащим районам, и дальнейшим
                сервисным обслуживанием - обеспечивает компания Hördis. В
                соответствующем разделе сайта, есть все контакты организации.
              </p>
              <a href="#" id="go" class="box_btn">Узнать цену</a>
            </div>
          </div>

        </div>
      </div>
    </section>

    <section class="property_k">
      <div class="wrapper_k">
        <p class="property_k_title">Hörmann для промышленных владений</p>
        <div class="property_k_container">
          <a
            href="pod-category-3.php"
            data-podcategory="#pod_1"
            class="property_k_container_box active"
          >
            <p class="property_k_container_box_text">Промышленные ворота</p>
            <div class="underline_k"></div>
          </a>
          <a
            href="pod-category-4.php"
            data-podcategory="#pod_2"
            class="property_k_container_box"
          >
            <p class="property_k_container_box_text">Двери</p>
            <div class="underline_k"></div>
          </a>
          <a
            href="pod-category-5.php"
            data-podcategory="#pod_3"
            class="property_k_container_box"
          >
            <p class="property_k_container_box_text">Приводы</p>
            <div class="underline_k"></div>
          </a>

          <div
            class="property_k_container_bg active"
            id="pod_1"
            style="background:url('img/category/industrial-gates.jpg') center/cover no-repeat;"
          ></div>
          <div
            class="property_k_container_bg"
            id="pod_2"
            style="background:url('img/podcategory_k.jpg') center/cover no-repeat;"
          ></div>
          <div
            class="property_k_container_bg"
            id="pod_3"
            style="background:url('img/privodi.jpg') center/cover no-repeat;"
          ></div>
        </div>
        <div class="property_k_slider">
          <div class="slider">
            <div class="slide">
              <a
                href="pod-category-3.php"
                class="box"
                style="background:url('img/category/industrial-gates.jpg') center/cover no-repeat;"
              >
                <div class="overlay"></div>
                <p class="box_text">Промышленные ворота</p>
              </a>
            </div>
            <div class="slide">
              <a
                href="pod-category-4.php"
                class="box"
                style="background:url('img/podcategory_k.jpg') center/cover no-repeat;"
              >
                <div class="overlay"></div>
                <p class="box_text">Двери</p>
              </a>
            </div>
            <div class="slide">
              <a
                href="pod-category-5.php"
                class="box"
                style="background:url('img/privodi.jpg') center/cover no-repeat;"
              >
                <div class="overlay"></div>
                <p class="box_text">Приводы</p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="about-us-slider-section-wrap">
      <div class="about-us-slider-section">
        <div class="about-us-slider-content-wrap my-container row">
          <div class="col-12 col-lg-6">
            <h3 class="about-us-slider-content-title">Наш ШОУ-РУМ</h3>
            <p class="about-us-slider-content-text pb-3">
              <span
                >Официальный партнер Hörmann в Одессе компания “Hördis”</span
              >
              - это надежная и компетентная установка с гарантией и сервисным
              обслуживанием по Одессе, и за ее пределами. <br />
              В ШОУ-РУМе представлены образцы гаражных секционных ворот как для
              частного владения, так и промышленного сектора; входные и
              межкомнатные двери; автоматика для гаражных и въездных ворот и все
              необходимые к ним аксессуары.
            </p>
          </div>
          <div class="col-12 col-lg-6 text-center pad-0">
              <div class="about-us-main-slider">
                  <div class="about-us-main-slider-item">
                      <img src="./img/gallery2/1.jpg" style="object-position: 40% 50%" alt="" />
                  </div>
                  <div class="about-us-main-slider-item">
                      <img src="./img/gallery2/2.jpg" style="object-position: 38% 50%" alt="" />
                  </div>
                  <div class="about-us-main-slider-item">
                      <img src="./img/gallery2/3.jpg" alt="" />
                  </div>
                  <div class="about-us-main-slider-item">
                      <img src="./img/gallery2/4.jpg" alt="" />
                  </div>
                  <div class="about-us-main-slider-item">
                      <img src="./img/gallery2/5.jpg" alt="" />
                  </div>

              </div>
            <div class="about-us-slider-arrows best_class_forever">
              <a href="./gallery.php" class="category_k_box_btn">Больше работ</a>
              <div class="arrow_k_block_about">
                <div class="about-us-arrows about-us-arrow-prev"></div>
                <div class="about-us-slider_modal_counter">
                  <span class="current"></span>
                  /
                  <span class="total"></span>
                </div>
                <div class="about-us-arrows about-us-arrow-next"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <!--modal window-->

   
    <div id="overlay"></div>
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
