<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
                <p class="goods_k_container_box_title">
                    Секционные гаражные ворота - Немецкое новаторство современных технологий от Hörmann
                </p>
                <p class="goods_k_container_box_desc">
                    Секционные гаражные ворота - Немецкое новаторство современных технологий от Hörmann <br>
                    Секционные гаражные ворота Hörmann - созданы в лучшем воплощении нашего времени. Это особенные конструкции, которые открываясь, находятся под потолком, тем самым освобождая внешнее и внутреннее пространство. Такой функционал позволяет максимально экономить место в гараже, и конечно же рядом с ним. Секционные ворота для гаража подойдут под любые параметры проема, даже обеспечивая зазор в 10 см, что несомненно создает большее пространство по ширине проема для въезда, чем ворота с подъемно-поворотным функционированием.<br>
                    Отличная герметизация достигается при помощи сверхпрочных устойчивых уплотнений на гаражных секционных воротах, что установлены по всему периметру.<br>
                </p>
            </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/gar-sec-vor.jpg') bottom/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <video autoplay loop>
                  <source
                    src="img/roll-matic/Animation_Garagentore_fuer_Website_Sectionaltor_01.mp4"
                    type="video/mp4"
                  />
                </video>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/slide1.png') center/cover no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/slide2.png') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <video autoplay loop>
                  <source
                    src="img/roll-matic/Animation_Garagentore_fuer_Website_Sectionaltor_01.mp4"
                    type="video/mp4"
                  />
                </video>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/slide1.png') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-12">
                <p class="box_title">
                    Сертификация по ЕС стандартам - максимальный комфорт
                    <!-- <p class="goods_k_container_box_title">Секционные гаражные ворота - Немецкое новаторство современных технологий от Hörmann -->
                </p>
                <p class="goods_k_container_box_desc">
                    <!-- Секционные гаражные ворота Hörmann - созданы в лучшем воплощении нашего времени. Это особенные конструкции, которые открываясь, находятся под потолком, тем самым освобождая внешнее и внутреннее пространство. Такой функционал позволяет максимально экономить место в гараже, и конечно же рядом с ним. <br>
                          Секционные ворота для гаража подойдут под любые параметры проема, даже обеспечивая зазор в 10 см, что несомненно создает большее пространство по ширине проема для въезда, чем ворота с подъемно-поворотным функционированием.
                          Отличная герметизация достигается при помощи сверхпрочных устойчивых уплотнений на гаражных секционных воротах, что установлены по всему периметру. -->
                    Среди доступных на украинском рынке - секционные гаражные ворота
                    от Hörmann - отвечают всем возможным евростандартам в режиме
                    13241-1. После разных тестирований были подтверждены требования к
                    безопасности, а также положительные показатели комфорта и многие
                    другие аспекты.
                    <br />На досуге, можно удостовериться при помощи проверочных
                    контрольных вопросов о практичности и актуальности сертификатов
                    секционных гаражных ворот Hörmann в Одесском ШОУ-РУМе компании
                    “Hördis”.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a target="_blank" href="./catalog/VorotaSektcionnye&Bokoviye.pdf" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
        </div>
      </div>
    </section>
    <section class="goods_desc_k">
      <div class="wrapper_k">
        <div class="goods_desc_k_container">
          <div class="goods_desc_k_container_box p-3 p-lg-5">
            <p class="box_title">
              Чувство стиля или выбор секционных гаражных ворот от Hörmann в
              Одессе
            </p>
            <p class="box_desc">
              Предпочтение современному дизайну и прогрессивным технологиям, что
              воплощены в немецкой продукции Hörmann - отдает каждый одессит,
              что владеет усадьбой. На нашем сайте можно ознакомиться с
              одностенными воротами LTE, что больше подойдут для отдельно
              стоящих гаражей, или двустенными воротами LPU, которые уже с
              теплоизоляцией, плавным ходом и более устойчивы, за счет своей
              плотности. Также представлены деревянные по типу LTH ворота
              Hörmann с филенкой, что отличаются шикарной массивной древесиной.
              Секционные гаражные ворота от Hörmann - для тех, кто привык
              получать лучшее.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Выберите подходящий тип ворот</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/section-gates/csm_67_neu_500x630_db9a7833d1.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Энергосберегающие ворота выгодно отличаются секциями толщиной
                  67 мм с термическим разделением и прекрасной теплоизоляцией.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">LPU 67 Thermo</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/section-gates/csm_LPU_500_630_f2ba72b45b.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Двустенные ворота LPU обладают самой эффективной изоляцией.
                  Наряду с этим, секции толщиной 42 мм обеспечивают наилучшую
                  устойчивость и плавное, бесшумное движение ворот.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">LPU 42</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/section-gates/csm_LTE_500_630_fd20170638.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Одностенные ворота LTE идеально подходят для отдельно стоящих
                  гаражей, не требующих теплоизоляции.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">LTE 42</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/section-gates/csm_LTH_500_630_cdb4569c14.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Ворота из панелей массивной древесины идеально подходят для
                  деревянных домов или зданий с многочисленными деревянными
                  элементами, например, фахверк или элементы фасада.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">LTH 42</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="view_k">
      <div class="wrapper_k">
        <p class="view_k_title">Обзор мотивов</p>
        <div class="view_k_container">
          <div class="view_k_container_tabs">
            <p
              class="view_k_container_tabs_text active"
              data-tab-tovar="#tovar1"
            >
              Стальные секционные ворота
            </p>
            <div class="line"></div>
            <p class="view_k_container_tabs_text" data-tab-tovar="#tovar2">
              Секционные ворота из массивной древесины
            </p>
          </div>
          <div class="tabul_container">
            <div class="view_k_container_box active" id="tovar1">
              <div class="view_k_container_box_slider_big door slider">
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      D-гофр (LPU), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/D-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      T-гофр (LPU), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/T-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      Кассета (LPU, LTE), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/Kassette_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 450, белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_450_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 456, белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_456_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 461 с остеклением, белого цвета RAL
                      9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_461_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 457, белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_457_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 462 с остеклением, белого цвета RAL
                      9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_462_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 458, белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_458_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 454, белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_454_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 469 с остеклением, белого цвета RAL
                      9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_469_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      L-гофр (LPU), мотив 481 с остеклением, белого цвета RAL
                      9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/L-Sicke_481_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      T-гофр (LPU), мотив 501 со сплошными вставками под
                      нержавеющую сталь, под дерево
                    </p>
                    <img
                      src="img/section-gates/steel-doors/T-Sicke_Holz_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      T-гофр (LPU), мотив 500 с прерывистыми вставками под
                      нержавеющую сталь или под дерево (см рис.)
                    </p>
                    <img
                      src="img/section-gates/steel-doors/T-Sicke_Teilholz_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      S-гофр (LPU, LTE), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/S-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      M-гофр (LPU, LTE), белого цвета RAL 9016
                    </p>
                    <img
                      src="img/section-gates/steel-doors/M-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
              </div>
              <div class="view_k_container_box_slider_small door slider"></div>
            </div>
            <div class="view_k_container_box" id="tovar2">
              <div class="view_k_container_box_slider_big wood slider">
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Мотив 403, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/403_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      Мотив 403, с природным камнем, северная ель
                    </p>
                    <img
                      src="img/section-gates/wood-doors/403_Stein_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Мотив 404, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/404_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      Мотив 404, с природным камнем, северная ель
                    </p>
                    <img
                      src="img/section-gates/wood-doors/404_Stein_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Мотив 405, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/405_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">
                      Мотив 405, с фурнитурой распашных ворот «Exklusiv»
                      (опция), северная ель
                    </p>
                    <img
                      src="img/section-gates/wood-doors/405_exklusiv_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">S-гофр, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/Holz_S-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">M-гофр, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/Holz_M-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">L-гофр, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/Holz_L-Sicke_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Кассета, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/Holz_Kassette_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Мотив 401, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/401_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
                <div class="slide">
                  <div class="slide-box">
                    <p class="slide-box_text">Мотив 402, северная ель</p>
                    <img
                      src="img/section-gates/wood-doors/402_500x286.png"
                      alt=""
                      class="slide-box_image"
                    />
                  </div>
                </div>
              </div>
              <div class="view_k_container_box_slider_small wood slider"></div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="sales_k">
      <div class="sales_k_container">
        <img src="img/sale1.jpg" alt="" class="sales_k_container_image" />
        <div class="sales_k_container_box">
          <p class="sales_k_container_box_title">
            Акционное предложение от Hördis
          </p>
          <p class="sales_k_container_box_desc">
            Дом - это место покоя, безопасности и созданного его владельцем -
            комфорта. Истинное европейское качество ворот и дверей уже в Одессе
            с достойной скидкой до 30%.
          </p>
          <a href="stock.php" class="sales_k_container_box_btn">Посмотреть</a>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Гофры в синхронном порядке
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_gleiche_sickenabstaende_1000x700_70addc6a3a.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Отличительной чертой гаражных ворот Hörmann является строгое
                равномерное распределение гофров. Когда ворота находятся в
                закрытом положении, то переходов между ними практически не
                видно. В отличии от ворот, других производителей, где не учтена
                гармония - представлено на картинке, где это четко
                визуализируется. Также есть возможность - установки практичных
                боковых дверей от Hörmann, которые будут идти в унисон с
                гаражными гофрами. Это единство - дает безупречный внешний вид,
                который по достоинству оценен уже многими владельцами немецкого
                качества.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Равномерное размещение кассет
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_gleichmaessige_kassettenaufteilung_1000x700_7d1bf67cee.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Для более эстетичного вида, компания Hörmann представила
                секционные ворота, где высота и расстояние между кассетами
                абсолютно одинаковы. За счёт этого, создаётся более гармоничный
                вид ворот, что смело можно отнести к категории лучших
                дизайнерских решений.
                <br />
                <br />
                Также есть возможность установки практичных боковых дверей,
                которые будут иметь такой же визуал как и гаражные секционные
                ворота.
                <br />
                <br />
                Гаражные секционные ворота других производителей, не имеют
                такого эстетического вида как у Hörmann, потому как у них нет
                равномерного расстояния между кассетами, и они имеют разную
                высоту, а асимметрия довольно часто отмечается гостями или
                прохожими.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Абсолютное визуальное совпадение направляющей с полотном ворот
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_USP_Ansichtsgleichheit_1000x700_b5d8bd621d.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Большое внимание Hörmann уделяет аксессуарам. Достичь более
                изящного вида удается благодаря возмещаемой фальш-панели, что
                находится в зоне перемычки которую поставляют к белым воротам.
                Все поверхности сочетаемы с секциями ворот. У Hörmann всегда
                идеальное сочетание всех компонентов.<br />
                Боковые системы под пометкой 2 - экспортируются массово, но к
                белоснежной отделке полотна Woodgrain. Для секционных ворот,
                которые оснащены плоскостями Sandgrain, Silkgrain или Decograin
                предоставляются как дополнение - облицовочные направляющие с
                поверхностью по типу секции ворот. В то же время, ворота с
                плоскостью Micrograin дополняются облицовочной направляющей, что
                доставляется со скользящей поверхностью Silkgrain.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Безопасность прежде всего
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_USP_aufschiebesicherung_1000x700_27fbe54496.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                У непрошенных гостей теперь нет никаких шансов попасть в
                домовладение, которое под защитой Hörmann. Во время закрытия
                гаражных ворот защитная система от подваживавния (проникновения)
                на автомате упирается в направляющую шину, а далее происходит
                фиксация и обеспечивается защита от подваживания (взлома).
                Такого функционала больше нет нигде, так как все системы защиты
                у Hörmann запатентованы. В сравнении с приводной защитой у
                конкурентов, ворота этой фирмы имеют механическое управление,
                что не влияет на работоспособность даже в отсутствие
                электроснабжения.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Уравновешивающая фальш-панель перемычки, со стыковкой заподлицо.
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_Sturzblende_3f5083decd.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Когда закрываются ворота, в это же время выполняется состыковка
                заподлицо с фальш-панелью.<br />
                Это актуально для ворот модели LPU, где восполняющая
                фальш-панель расположена в районе перемычки с незримой
                органической трансформацией между фальш-панелью и полотном.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Долговечная защита</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_Langzeitschutz_8f5553cbbf.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Пластмассовое основание направляющих, что достигает высоты 4 см
                - выполнено из антикоррозийного материала. Это позволяет
                обеспечить отличную защиту ворот при любых погодных условиях. Ни
                одна из конкурирующих компаний еще не представляла на рынке
                ворот и дверей такого функционала.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Эффективная термо- и теплоизоляция
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/Waermedaemmung_15Proz_RU.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Когда гараж – это часть дома, то стоит позаботиться о хорошей
                теплоизоляции гаражных ворот. С помощью уплотнения направляющей
                TermoFrame от производителя Hörmann возможно улучшить, и так
                отличные показатели теплоизоляции ворот.
                <br />
                <br />
                С помощью пластмассового профиля черного цвета, обеспечивается
                термическое разделение между рамой и кирпичной стеной, и за счет
                этого - улучшается термоизоляция на 15%.
                <br />
                <br />
                А также у Вас есть возможность заказать уплотнительную прокладку
                TermoFrame для абсолютно всех ворот от Hörmann.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Современные технические решения для дома
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_Praxisnahe_Loesungen_8219942a6e.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Для простого прохода в гараж, без поднятия ворот, мы можем Вам
                порекомендовать калитку без порога. С ее помощью, у Вас будет
                возможность быстро и беспрепятственно попасть в гараж, и взять
                необходимые инструменты. Высота порога из нержавеющей стали
                всего 10 мм посредине и 5мм по краям. Благодаря этой детали,
                практически исключается спотыкание.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Сертифицированная радиосистема BiSecur для безопасности.
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_BiSecur_f7235aad97.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Радиосистема BiSecur – это новаторская технология. Она создана
                для высоко-комфортного удобства контроля автоматических
                механизмов гаражных и въездных ворот, освещения, дверей и
                другого функционала общей системы. Эта разработка компании
                Hörman прошла тестирования и в итоге - сертифицирована, что дает
                чувство уверенности и безопасности с данной системой.
                Радиосигнал не поддается чтению и скопирован другими
                устройствами - быть не может.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                И снова об уникальной радиосистеме
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/guter_grund_perfekt_abgestimmt_400x280.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Все устройства радиосистемы BiSecur абсолютно совместимы. С
                помощью пульта ДУ легко и просто управлять приводами дверей,
                гаражными и въездными воротами, которые оборудованы приводами
                Hörmann.<br />
                Большим плюсом радиосистемы BiSecur есть возможность контроля
                входной двери, либо гаражными, а также въездными воротами, через
                смартфон или планшет.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Функция запроса о положении ворот, без отвлечений
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_Abfrage_08c43643f9.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Современный комфорт предполагает наименьшее отвлечение от
                основных занятий. Достаточно использовать пару клавиш на пульте
                HS 5 BS, и по cветодиоду, а именно по его цвету - можно узнать в
                каком положении ворота. Более простого управления воротами –
                просто нет!
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">Стильный дизайн</p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/section-gates/advantages/csm_Design_b6a558976c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Оригинальные пульты ДУ BiSecur имеют не только красивый
                благородный черный и белый цвет, но и имеют элегантную форму. Он
                выглядит не просто банально, а говорит об изысканном вкусе
                владельца.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/s72U23NHptA/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Надежно, удобно, эксклюзивно: радиосистема BiSecur компании
              Хёрманн
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/k8lXfJWSgmg/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Привод гаражных ворот SupraMatic – Дуэль
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/mW7a24MEoUE/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Приводы гаражных ворот – спокойствие и безопасность
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/YUnE6rc4YaQ/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Современные гаражные ворота и входные двери. Особенности
              конструкции.
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/cErcJl30jIA/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Гаражные ворота Hörmann. Монтаж
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="gallery_section videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Галерея</p>
        <div class="gallery_slider slider">
          <div class="slide">
            <div class="image_container">
              <img
                src="img/section-gates/gallery/csm_S_Sicke_2sp_4413433ec6.jpg"
                alt=""
              />
            </div>
          </div>
          <div class="slide">
            <div class="image_container">
              <img
                src="img/section-gates/gallery/csm_Tormotiv405_2sp_01_3833cddec7.jpg"
                alt=""
              />
            </div>
          </div>
          <div class="slide">
            <div class="image_container">
              <img
                src="img/section-gates/gallery/Garagen_Sectionaltor_2sp.jpg"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <div id="overlay"></div>
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/jquery.maskedinput.js"></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
