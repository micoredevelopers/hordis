<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
</head>
<body>
<?php include "header.php"?>
<section class="goods_k ">
    <div class="wrapper_k">
        <div class="goods_k_container row m-0 align-items-start">
            <div class="goods_k_container_box  col-12 col-lg-6 ">
                <div class="goods_k_container_box_slider_big slider">
                    <div class="slide">
                        <div class="big_slider_image"
                             style="background:url('img/roll-matic/csm_CSF8575_X_1000x700_a7b8b51721.jpg') center/cover no-repeat"></div>
                    </div>
                    <div class="slide">
                        <video autoplay loop>
                            <source src="img/roll-matic/Animation_Garagentore_fuer_Website_Rolltor_01.mp4"
                                    type="video/mp4">
                        </video>
                    </div>
                    <div class="slide">
                        <div class="big_slider_image"
                             style="background:url('img/roll-matic/csm_rollmatic_1_1000x700_c767c16ac2.jpg') center/cover no-repeat"></div>
                    </div>
                </div>
                <div class="goods_k_container_box_slider_small slider">
                    <div class="slide">
                        <div class="small_slider_image"
                             style="background:url('img/roll-matic/csm_CSF8575_X_1000x700_a7b8b51721.jpg') center/cover no-repeat"></div>
                    </div>
                    <div class="slide">
                        <video autoplay loop>
                            <source src="img/roll-matic/Animation_Garagentore_fuer_Website_Rolltor_01.mp4"
                                    type="video/mp4">
                        </video>
                    </div>
                    <div class="slide">
                        <div class="small_slider_image"
                             style="background:url('img/roll-matic/csm_rollmatic_1_1000x700_c767c16ac2.jpg') center/cover no-repeat"></div>
                    </div>
                </div>
            </div>
            <div class="goods_k_container_box col-12 col-lg-6 ">
                <div class="goods_k_container_box ">
                                    <p class="goods_k_container_box_title">Исключительная надежность немецкого качества - Рулонные гаражные ворота.</p>
                                    <p class="goods_k_container_box_desc m-0">
                                        Рулонные ворота RollMatic наматываются на вал, тем самым экономя пространство не только внутри, но и с внешней стороны гаража. Место под потолком тоже остается свободным, что дает возможность оборудовать свободную зону дополнительными полезными аксессуарами.
                                        Монтирование гаражных рулонных ворот допускается не только в стандартный квадратный проем, но и можно выполнить установку в скошенную, сегментную или полукруглую арку.
                                        <br>
                                        <br>
                                        Когда нет возможности установки секционных гаражных ворот, наиболее лучшим и выгодным решением будет установка рулонных (роллетных) ворот.
                                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 goods_k_container_box mt-3">
                <p class="goods_k_container_box_desc">
                    Такой вариант подойдет лучше всего, если подъезд к гаражу занимает ограниченную территорию, или въезд осуществляется с тротуара.
                    <br>
                    <br>
                    Цветовая палитра насчитывает более 10 цветов, что не составит труда подобрать дизайн ворот под фасад дома.<br>
                    Отличное дополнение создаст установка боковых дверей, что безупречно сочетаются с воротами по экстерьеру, и визуально абсолютно идентичны.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a href="./catalog/VorotaRollMatic.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
    </div>
</section>
<section class="goods_k not_first">
    <div class="wrapper_k">
        <div class="goods_k_container row m-0">
            <div class="goods_k_container_box col-lg-6 col-12">
                <p class="goods_k_container_box_title">Ворота потолочные RollMatic OD в Одессе - Редкая высокофункциональная продукция.</p>
                <p class="goods_k_container_box_desc">
                    Компактные потолочные ворота RollMatic OD – это отличный выбор для современного человека.  Такая конструкция позволяет альтернативную установку в ограниченном пространстве, когда небольшой гараж, и площади совсем минимум. За счет некрупной конструкции, ворота требуют перемычки размером всего в 60 мм.
                    <br>
                    <br>
                    Благодаря комплектации по стандартам, привод RollMatic может быть установлен как с правой, так и с левой стороны горизонтальной направляющей шине, при этом не занимая лишнего места в перемычке.
                </p>
            </div>
            <div class="goods_k_container_box col-lg-6 col-12">
                <div class="goods_k_container_box_slider_big slider">
                    <div class="slide">
                        <div class="big_slider_image"
                             style="background:url('img/roll-matic/csm_automatik-rolltor_rollmatic_1000x700_e300d8aaf4.jpg') center/cover no-repeat"></div>
                    </div>
                    <div class="slide">
                        <video autoplay loop>
                            <source src="img/roll-matic/Animation_Garagentore_fuer_Website_Deckenlauftor_240x344.mp4"
                                    type="video/mp4">
                        </video>
                    </div>
                    <div class="slide">
                        <div class="big_slider_image"
                             style="background:url('img/roll-matic/csm_Deckenlauftor_RollMatic_OD_A.jpg') center/cover no-repeat"></div>
                    </div>
                </div>
                <div class="goods_k_container_box_slider_small slider">
                    <div class="slide">
                        <div class="small_slider_image"
                             style="background:url('img/roll-matic/csm_automatik-rolltor_rollmatic_1000x700_e300d8aaf4.jpg') center/cover no-repeat"></div>
                    </div>
                    <div class="slide">
                        <video autoplay loop>
                            <source src="img/roll-matic/Animation_Garagentore_fuer_Website_Deckenlauftor_240x344.mp4"
                                    type="video/mp4">
                        </video>
                    </div>
                    <div class="slide">
                        <div class="small_slider_image"
                             style="background:url('img/roll-matic/csm_Deckenlauftor_RollMatic_OD_A.jpg') center/cover no-repeat"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 goods_k_container_box mt-3">

                <p class="goods_k_container_box_desc">
                    С включенной в комплектацию опцией для привода SupraMatic, потолочные ворота OD могут открываться с ускорением, что значительно сэкономит время. В дополнительных функциях можно настроить высоту открывания или освещения. Пульт ДУ содержит много полезного функционала, который можно использовать для собственного индивидуального комфорта.
                    <br>
                    <br>
                    Купить гаражные потолочные ворота RollMatic OD в Одессе и компетентно установить их, поможет компания “Hordis”. Монтаж сопровождается последующей гарантией и сервисным обслуживанием, о чем подробнее можно узнать у менеджеров офиса, связавшись по контактам на сайте.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a href="./catalog/VorotaRollMatic.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="advantage_k">
    <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Надежный режим баланса ворот</p>
                    <img src="img/arrow_adv.png" alt="" class="active-arrow-tov advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_USP_Zugfeder_Technik_3ddbf50447.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Система с функцией уравновешивания надежно поддерживает привод, когда открываются или закрываются ворота, чем сохраняют износостойкость механизмов, что содержаться в приводе. Эта структура встроена в направляющие шины.
                        Защита полотна потолочных ворот, происходит благодаря блоку многократных пружин и двойным проволочным тросам. В момент отключения электроэнергии или других аварийных причин, ворота очень просто открыть ручным способом. Для таких манипуляций, дополнительная рукоятка на рулонные ворота не устанавливается.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Бесконтактное автоотключение</p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/sichere_abschaltautomatik.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Ворота RollMatic функционируют очень тихо, но это возможно только используя щадящий режим. Надежная система на автоматике, что контролируют параметры уравновешивания и отключения, позволяют моментально задержать ворота при любых препятствиях, которые попали под момент их закрытия. Этот функционал освобождает от ограничений - дети, а также домашние любимцы, всегда будут в безопасности. Для эффективнейшей защиты и безупречной надежности, рекомендуется установить бесконтактный световой барьер.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Рулонные ворота в стиле минимализма</p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_USP_Wartungsfreundlich_c7598c7bc3.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Монтаж рулонных ворот наиболее выгоден для сохранения пространства под потолком. Когда есть необходимость в наведении дополнительного антуража внутри гаража - такой компромисс отлично экономит место для воплощения задумок владельца. Установить рулонники можно как снаружи, так и внутри помещения, что несомненно удобно для выбора оптимального решения. Ворота идеально сматываются в зоне перемычки, чем и удивляют глобально продуманной концепцией.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">
                        Концепция “минимум” в потолочных воротах
                    </p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_Deckenlauftor_Cam_Tor_oben_Innen_rechts_c99afc3271.jpg"
                         alt="" class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Потолочники RollMatic OD идеально вписываются для гаражей с небольшой высотой перемычки. Они производятся в современных малых размерах, и довольно престижно выглядят на небольших гаражах. Ворота, состоящие из полотна поднимаются к потолку по нешироким, и даже малым направляющим. Конструкция настолько компактна, что сопровождается небольшой перемычкой по размеру, которая насчитывает всего - 60 мм.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Сертифицированная радиосистема BiSecur для безопасности.
                    </p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_BiSecur_f7235aad97.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Радиосистема BiSecur – это новаторская технология. Она создана для высоко-комфортного удобства контроля автоматических механизмов гаражных и въездных ворот, освещения, дверей и другого функционала общей системы.
                        Эта разработка компании Hörman прошла тестирования и в итоге - сертифицирована, что дает чувство уверенности  и безопасности с данной системой.  Радиосигнал не поддается чтению и скопирован другими устройствами - быть не может.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">И снова об уникальной радиосистеме
                    </p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/guter_grund_perfekt_abgestimmt_400x280.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Все устройства радиосистемы BiSecur абсолютно совместимы. С помощью пульта ДУ легко и просто управлять приводами дверей, гаражными и въездными воротами, которые оборудованы приводами Hörmann.
                        Большим плюсом радиосистемы BiSecur есть возможность контроля входной двери, либо гаражными, а также въездными воротами, через смартфон или планшет.
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Функция запроса о положении ворот, без отвлечений</p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_Abfrage_08c43643f9.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Современный комфорт предполагает наименьшее отвлечение от основных занятий.
                        Достаточно использовать пару клавиш на пульте HS 5 BS, и по cветодиоду, а именно по его цвету - можно узнать в каком положении ворота. Более простого управления воротами – просто нет!
                    </p>
                </div>
            </div>
            <div class="advantage_k_container_item">
                <div class="advantage_box">
                    <img src="img/shield.png" alt="" class="advantage_k_container_item_img">
                    <p class="advantage_k_container_item_text">Стильный дизайн</p>
                    <img src="img/arrow_adv.png" alt="" class="advantage_k_container_item_arrow">
                </div>
                <div class="advantage_k_container_item_tabul">
                    <img src="img/roll-matic/advantages/csm_Design_b6a558976c.jpg" alt=""
                         class="advantage_k_container_item_tabul_image">
                    <p class="advantage_k_container_item_tabul_text">
                        Оригинальные  пульты ДУ BiSecur имеют не только красивый благородный черный и белый цвет, но и имеют элегантную форму. Он выглядит не просто банально, а говорит об изысканном вкусе владельца.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery_section videos_k">
    <div class="wrapper_k">
        <p class="videos_k_title">Галерея</p>
        <div class="gallery_slider slider">
            <div class="slide">
                <div class="image_container">
                    <img src="img/roll-matic/gallery-image.jpg" alt="">
                </div>
            </div>
            <div class="slide">
                <div class="image_container">
                    <img src="img/roll-matic/gallery-image.jpg" alt="">
                </div>
            </div>
            <div class="slide">
                <div class="image_container">
                    <img src="img/roll-matic/gallery-image.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="videos_k">
    <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
            <div class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/s72U23NHptA/?rel=0&showinfo=0&enablejsapi=1" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <p class="videos_k_container_card_text">Надежно, удобно, эксклюзивно: радиосистема BiSecur компании Хёрманн</p>
            </div>
        </div>
    </div>
</section>

<?php include "formTemplates/formFooter.php" ?>


 <?php include "footer.php"?>
<!--modal window-->


<div id="overlay"></div>
    
<?php include "formTemplates/formContact.php" ?>
  
<?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
<script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>