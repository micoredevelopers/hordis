<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Промышленные секционные ворота
            </p>
            <p class="goods_k_container_box_desc">
              Дверные системы Hörmann - это современные стандарты и высокое
              качество. Компания предлагает уникальный ассортимент секционных
              ворот для промышленных предприятий, которые изготавливаются с
              учетом индивидуальных требований клиента. Открытие ворот
              происходит вертикально вверх, что максимально освобождает
              около-воротное пространство, внутри и снаружи здания. Надежный
              воротовой механизм плавно поднимает секции вверх, и складывает их
              параллельно под потолком в специально оборудованную плоскость.
              Заход относительно неглубокий, что делает эти воротные системы
              особо привлекательными для зданий с прозрачными фасадами или
              стеклянной крышей. Благодаря разнообразию фурнитуры, ворота
              подходят к любым промышленным конструкциям.
            </p>

            <p class="goods_k_container_box_desc d-lg-none">
              Модели серии 50 выпускаются из стали, алюминия и комбинированного
              сплава (алюминий - сталь). Ворота имеют универсальный изысканный
              дизайн, высокопрофессиональную защиту от взлома, большой выбор
              диапазона ширины и надежный сертификат качества.
            </p>
            <p class="goods_k_container_box_desc d-lg-none">
              Секционные ворота формируют значительную часть наружной
              конструкции промышленных зданий, что очень экономично и выгодно в
              плане энергосбережения. Больше об этой информации можно узнать,
              изучив раздел «Путеводитель по экономии энергии».
            </p>
            <p class="goods_k_container_box_desc d-lg-none">
              Неограниченный доступ к ознакомлению с промышленными секционными
              воротами Hörmann в Одессе, в открытом ШОУ-РУМе компанией “Hördis”.
              Подготовленные менеджеры отвечают на вопросы посетителей, и дают
              более детальные консультации заинтересованной аудитории.
            </p>
            <div class="box_btns d-lg-none">
              <a id="" href="#" class="box_btns_item go">Узнать цену</a>
              <a href="./catalog_prom/VorotaSectcionniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <img
                  class="big_slider_image"
                  src="./img/Industrial/industrial_gates/csm_Industrie-Sectionaltore_1000x700_615a3ed787.jpg"
                >
              </div>
            </div>
          </div>
          <div class=" goods_k_container_box d-none d-lg-block col-12">
            <p class="goods_k_container_box_desc">
              Модели серии 50 выпускаются из стали, алюминия и комбинированного
              сплава (алюминий - сталь). Ворота имеют универсальный изысканный
              дизайн, высокопрофессиональную защиту от взлома, большой выбор
              диапазона ширины и надежный сертификат качества.
            </p>
            <p class="goods_k_container_box_desc">
              Секционные ворота формируют значительную часть наружной
              конструкции промышленных зданий, что очень экономично и выгодно в
              плане энергосбережения. Больше об этой информации можно узнать,
              изучив раздел «Путеводитель по экономии энергии».
            </p>
            <p class="goods_k_container_box_desc">
              Неограниченный доступ к ознакомлению с промышленными секционными
              воротами Hörmann в Одессе, в открытом ШОУ-РУМе компанией “Hördis”.
              Подготовленные менеджеры отвечают на вопросы посетителей, и дают
              более детальные консультации заинтересованной аудитории.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog_prom/VorotaSectcionniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Абсолютная прозрачность на долгое время
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_dauerhaft_Durchsicht_1000x700_1b053f9a3a.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Hörmann всегда заботится о прочности и долговечности своих
                товаров. Промышленные ворота подвержены более высоким нагрузкам,
                поэтому модели с прозрачным фасадом изготовлены из
                высококачественной пластмассы марки DURATEC. Специальное
                надежное покрытие защищает поверхность от царапин и прочих
                механических повреждений. Даже после тщательных многочисленных
                чисток, стекла остаются идеально прозрачными. <br />
                <br />
                Все секционные ворота с остеклением серийно выпускаются с
                фирменной пластмассой DURATEC.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                ThermoFrame - Результативная теплоизоляция
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_effizienteWaermedaemmung_1000x700_01_511f2cf44a.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Для отапливаемых помещений нужны ворота, которые будут
                удерживать тепло. Уникальная технология ThermoFrame от Hörmann
                позволяет максимально улучшить теплоизоляцию секционных ворот
                почти на 20%. Суть изоляции состоит в том, что между стеной и
                направляющей конструкцией, с помощью которой прикреплены ворота,
                размещается уплотнительный барьер ThermoFrame. Кроме того,
                усиленное уплотнение в верхней зоне перемычки и боковые манжеты
                секционных ворот надежно защищают помещение от потерь не только
                тепла, но и прохлады.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Простые решения для промышленных условий
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_Praxisnahe_Loesungen_Industrie_89b736f239.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Особым комфортом отличается калитка Hörmann с практически
                плоским порогом. Ее можно использовать как для дополнительного
                входа в ворота, так и в качестве отдельных эвакуационных дверей.
                Секционные ворота шириной менее 5,5 метров имеют порог высотой
                10мм, а по бокам – 5мм. Такое дизайнерское решение не только
                избавит от неосторожного спотыкания, но и очень удобно для
                безопасного выезда тележек. Отсутствие порога также незаменимо
                при безбарьерном строительстве.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Управление движением ворот на расстоянии
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_beruerungsloseTorueberw_1000x700_933600338e.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Секционных ворота укомплектованные приводами ITO 400, а также WA
                400 - серийно выпускаются с защитным закрывающим устройством с
                оптическими датчиками. Также, без дополнительной платы, Hörmann
                дает возможность использовать световой барьер VL 1, который
                позволит бесконтактно контролировать край закрытия, что очень
                удобно для безопасной и комфортной работы секционных ворот.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/-X62eO2as_U/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Остекление DURATEC – высочайшая стойкость к царапинам
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/O6LJAwVYJWE/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Инновационное решение – калитка без порога
            </p>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/formContact.php" ?>
    <?php include "formTemplates/successForm.php" ?>
    <?php include "footer.php"?>

    <div id="overlay"></div>
    


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
