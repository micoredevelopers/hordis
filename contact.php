<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Главная</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
    <div class="height_k_100">
        <?php include "header.php"?>

      <section class="contacts my-container">
        <div class="contacts-content-wrap flex-column-reverse flex-lg-row row">
          <div class="col-lg-5">
            <div class="contacts-form-wrap">
              <h1 class="contacts-form-title">Получить просчет</h1>
              <form class="contacts-form send-form" action="">
                <div class="box_kkk">
                  <input type="hidden" name="form_name" value="Получить просчет">
                  <input
                    class="contacts-form-item required"
                    id="input_1_k"
                    type="text"
                    name="name"

                  />
                  <label class="label_for" for="input_1_k">Имя *</label>
                </div>
                <div class="box_kkk">
                  <input
                    class="contacts-form-item required"
                    id="input_2_k"
                    type="text"
                    name="phone"

                  />
                  <label class="label_for" for="input_2_k"
                    >Номер телефона *</label
                  >
                </div>
                <div class="box_kkk">
                  <input
                    class="contacts-form-item"
                    id="input_3_k"
                    type="text"
                    name="email"
                  />
                  <label class="label_for" for="input_3_k">Почта</label>
                </div>
                <div class="box_kkk">
                  <textarea
                    class="contacts-form-item "
                    id="input_4_k"
                    type="text"
                    name="desc"
                  ></textarea>
                  <label class="label_for abs-top" for="input_4_k">Описание</label>
                </div>
                <div class="estimation_k_container_box">
                  <button type="submit" class="estimation_k_container_btn">
                    Отправить
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="offset-lg-1 col-lg-6 contacts-content-box">
            <div class="row contacts-address-box text-center text-lg-left">
              <div class="col-lg-12">
                <h3 class="contacts-title">Одесса</h3>
                <ul class="contacts-list">
                  <li class="contacts-list-item">
                    Ул. Академика Вильямса 1В, <br />
                    ТЦ «Décor House»<br>
                    График работы: без выходных, с 10:00 до 19:00
                  </li>

                  <li class="contacts-list-item"><a href="tel:+380963201299"> тел.: +38 096 320 12 99</a></li>
                  <li class="contacts-list-item"><a href="tel:+380487881709"> тел.: +38 048 788 17 09</a></li>
                  <li class="contacts-list-item">
                      <a href="mailto:office.hordis@gmail.com"> E-Mail: office.hordis@gmail.com</a>
                  </li>
                  <li class="contacts-list-item">hordis.com.ua</li>
                </ul>
                  <div class="social_box contacts-social_box mt-3 justify-content-center justify-content-lg-start">
                      <a target="_blank" href="https://www.facebook.com/HordisVorota/" class="social_box_item">
                          <img
                                  src="img/facebook-logo.svg"
                                  alt=""
                                  class="social_box_item_img"
                          />
                      </a>
                      <a target="_blank" href="https://www.instagram.com/hordis_vorota_hormann/?hl=ru" class="social_box_item">
                          <img src="img/instagram.svg" alt="" class="social_box_item_img" />
                      </a>
                      <a target="_blank" href="https://msng.link/vi/380963201299" class="social_box_item">
                          <img src="img/viber-logo.svg" alt="" class="social_box_item_img" />
                      </a>
                  </div>
              </div>
            </div>
            <div class="map-place">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1375.9979446137534!2d30.705220704375616!3d46.38930659545932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c633ec8ddf14fb%3A0x497ea77695e94282!2sDecor+House!5e0!3m2!1sru!2sua!4v1557218490944!5m2!1sru!2sua"
                width="600"
                height="450"
                frameborder="0"
                style="border:0"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>
      </section>
      <?php include "footer.php"?>
      <div id="overlay"></div>
          
    <?php include "formTemplates/formContact.php" ?>
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?></div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
