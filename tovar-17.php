<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">Межкомнатные двери</p>
            <p class="goods_k_container_box_desc">
              Откройте для себя многочисленные преимущества. Секционные ворота
              открываются вертикально и в открытом положении находятся под
              потолком, экономя полезное пространство. Эта принципиальная
              особенность конструкции обеспечивает максимальное пространство как
              внутри, так и перед гаражом. Гаражные секционные ворота могут быть
              установлены в любые гаражные проёмы, обеспечивая на 14 см бóльшую
              ширину проезда, чем подъемно-поворотные ворота. Оптимальная
              герметичность секционных ворот обеспечивается за счет прочных и
              стойких к атмосферным воздействиям уплотнений со всех четырех
              сторон.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog/DveryMejkomnatniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/slide1.png') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/slide2.png') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/slide3.png') center/cover no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/slide1.png') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/slide2.png') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/slide3.png') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Улучшенная до 49 % теплоизоляция
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/fire-doors/advantages/csm_USP_Waermedaemmung49_1000x700_62f58c8986.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Хотите, чтобы теплоизоляция дверей в подвальных помещениях, а
                также теплоизоляция боковых дверей была такой же, как у обычной
                входной двери? <br />
                <br />
                Тогда Вам как нельзя лучше подойдут наши двери с защитой от
                взлома KSI Thermo, а также двери многоцелевого назначения MZ
                Thermo и MZ Thermo65! Двери многоцелевого назначения MZ Thermo65
                превзойдут Ваши самые смелые ожидания благодаря показателям
                теплоизоляции, улучшенным до 49 % по сравнению с обычными
                дверями многоцелевого назначения. <br />
                <br />
                *Теплоизоляция двери MZ Thermo65 улучшена до 49 % по сравнению с
                обычной дверью многоцелевого назначения
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Оснащение для защиты от взлома класса RC 2
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/fire-doors/advantages/csm_USP_Einbruchhemmung_Tuer_1000x700_251d32d87d.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Двери с защитой от взлома KSI / KSI Thermo, а также огнестойкую
                дверь и дверь с защитой от взлома WAT можно заказать в серийном
                исполнении с испытанным оснащением для защиты от взлома RC 2.
                Это обеспечит Вам ощущение большей безопасности в своих четырех
                стенах. Противовзломное оснащение для многофункциональной двери
                H3D можно заказать дополнительно.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Индивидуальное оформление
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/fire-doors/advantages/csm_USP_individuelleGestaltung_1000x700_01_7fd5ef5e8c.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Разработайте дизайн дверей самостоятельно, в соответствии с
                собственными представлениями и индивидуальными предпочтениями.
                Вы можете выбрать различные цвета и декоративную отделку, а
                также остекление.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Стальная облицовочная коробка VarioFix
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/fire-doors/advantages/csm_USP_Stahlfutterzarge_1000x700_7f4552d5cd.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Инновационная охватывающая коробка VarioFix – это идеальное
                решение для жилищного строительства: впервые при установке
                стальной коробки VarioFix используется тот же принцип, что и при
                монтаже деревянной коробки. Кроме этого, коробка производит
                благоприятное впечатление благодаря изящному дизайну и
                надёжности.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?>
    <div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
