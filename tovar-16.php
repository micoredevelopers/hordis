<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Category</title>
  <!-- <link rel="stylesheet" href="slick/slick.css">
  <link rel="stylesheet" href="slick/slick-theme.css">
  <link rel="stylesheet" href="css/category.css"> -->
  <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
          crossorigin="anonymous"
  />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="css/media.css" />
  <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
          rel="stylesheet"
  />
</head>
<body>
<?php include "header.php"?>

<section class="goods_k">
  <div class="wrapper_k">
    <div class="goods_k_container row m-0">
      <div class="goods_k_container_box order-1 order-lg-0 col-12 col-lg-6">
        <p class="goods_k_container_box_title">
          Огнестойкая дверь H8-5 от Европейского производителя
        </p>
        <p class="goods_k_container_box_desc">
          Бестселлером в Европе среди огнеупорных дверей по праву считается
          H85 от ТМ Hörmann. Она безупречно выполняет свои функции, и служит
          гарантией безопасности котельных и хранилищ жидкого топлива.
          Благодаря универсальной конструкции и профессионально подобранным
          материалам, дверь способна противостоять любому пожару. Внутренние
          противопожарные двери Hörmann – это качество, которому доверяют.
        </p>
      </div>
      <div class="goods_k_container_box order-0 order-lg-1 col-12 col-lg-6">
        <div class="goods_k_container_box_slider_big slider">
          <div class="slide">
            <div
                    class="big_slider_image"
                    style="background:url('img/fire-doors/main-1.jpg') center/cover no-repeat"
            ></div>
          </div>
        </div>
      </div>
      <div class="goods_k_container_box order-2 col-12">
          <p class="goods_k_container_box_desc">
              Огнестойкую дверь Hörmann в Одессе с противовзломным функционалом,
              можно заказать в компании “Hördis”, что также открыла ШОУ-РУМ в
              городе, где есть возможность не только увидеть, но и прикоснуться.
              Компетентный монтаж и сервисное обслуживание - гарантированы.
              Приветливые менеджеры ответят на все вопросы по номеру, что есть в
              контактах на сайте.
          </p>
      </div>
    </div>
  </div>
</section>
<section class="goods_k not_first">
  <div class="wrapper_k">
    <div class="goods_k_container row m-0">
      <div class="goods_k_container_box col-12 col-lg-6">
        <div class="goods_k_container_box_slider_big slider">
          <div class="slide">
            <div
                    class="big_slider_image"
                    style="background:url('img/fire-doors/main-2.jpg') center/cover no-repeat"
            ></div>
          </div>
        </div>
      </div>
      <div class="goods_k_container_box col-12 col-lg-6">
        <p class="goods_k_container_box_title">
          Противовзломная огнестойкая дверь WAT 40
        </p>
        <p class="goods_k_container_box_desc">
          Внутренняя дверь соединяющая гараж с домом в лучшем исполнении -
          это WAT 40. Благодаря высочайшим технологическим качествам и
          разнообразию отделки, она идеально выполняет свои функции, не
          нарушая гармонию общего дизайна дома. Hörmann позаботились о том,
          чтобы дверное полотно WAT обладало надежной степенью
          пожароустойчивости и при этом была оснащена защитой, от взлома.
          Повышенная безопасность и привлекательность делает двери Hörmann
          лучшим вариантом для оформления входа - изнутри дома в гараж.
        </p>
      </div>
    </div>
  </div>
</section>

<section class="advantage_k">
  <div class="wrapper_k">
    <p class="advantage_k_title">Почему Hörmann?</p>
    <div class="advantage_k_container">
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Дверное полотно с изоляционным слоем
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="active-arrow-tov advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Waermedaemmung49_1000x700_62f58c8986.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Дверь состоит из двухстенного стального полотна 45мм, при этом
            толщина одного листа достигает 1 мм. В качестве защитного
            изоляционного слоя применяется специальная минеральная вата.
            Модель отличается особой устойчивостью и надежностью, благодаря
            усилительной металлической конструкции и стальным запирающим
            болтам. Замок и набор ручек подходят для профильных цилиндров и
            Buntbartschlüssel (BB), ключ BB входит в комплект.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Скрытая противопожарная защита
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Einbruchhemmung_Tuer_1000x700_251d32d87d.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Дверь оснащена по всей площади высокотехнологичной огнестойкой
            изоляцией с внутренней и внешней стороны, которая
            профессионально скрыта отделочными материалами.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Идеальная герметичность по периметру
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_individuelleGestaltung_1000x700_01_7fd5ef5e8c.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Угловая рама толщиной 2 мм с уплотнением по всему дверному
            контуру - надежно герметизирует противопожарную дверь HRUS
            30A-1. В случае сплошного пола, нижний ограничитель можно легко
            снять.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Дымонепроницаемость двери
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Stahlfutterzarge_1000x700_7f4552d5cd.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Для каждой двери, кроме стандартной комплектации и
            соответственно своему предназначению, Hörmann предлагает
            дополнительное оснащение. Модель HRUS 30A-1 возможна в
            исполнении с верхними доводчиками или с дополнительным
            уплотнением от дыма и нижним упором.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include "formTemplates/formFooter.php" ?>

<?php include "footer.php"?>
    <div id="overlay"></div>

    
    <?php include "formTemplates/formContact.php" ?>

    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"
></script>
<script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>

