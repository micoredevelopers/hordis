<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row align-items-start">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">Скоростные ворота</p>
            <p class="goods_k_container_box_desc">
              Скоростные ворота Hörmann предназначены для внутренних и внешних
              ограждений в зонах с повышенной интенсивностью движения. Они не
              препятствуют активному транспортному потоку, помогают
              оптимизировать рабочий процесс, надежно защищают помещение от
              сквозняков, и в целом благоприятствуют сохранению оптимальной
              атмосферы в помещении. Ворота выпускаются с многофункциональной
              качественной системой управления, что позволяет легко регулировать
              динамичность работы. Профессиональные скоростные ворота компании
              Hörmann представлены в разных вариациях:
            </p>
          </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/Industrial/industrial_gates/csm_Schnelllauftore_1000x700_7fe6701871.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-12">
              <ul class="pl-4">
                  <li class="goods_k_container_box_desc mb-1">
                      - прозрачные с удобными гибкими подвесками, открывающиеся
                      горизонтально или вертикально;
                  </li>
                  <li class="goods_k_container_box_desc mb-1">
                      - в сочетании с рулонными или секционными дверными системами;
                  </li>
                  <li class="goods_k_container_box_desc ">
                      - спиральные с прочными профилями из алюминия.
                  </li>
              </ul>
              <p class="goods_k_container_box_desc ">
                  Купить скоростные ворота Hörmann в Одессе можно в ШОУ-РУМе
                  компании “Hördis”. Также организация занимается монтажом
                  конструкций, и последующим сервисным обслуживанием.
              </p>
              <div class="box_btns">
                  <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                  <a href="./catalog_prom/VorotaScorostnye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
              </div>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Серийное обеспечение безопасности
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_serienmaessige_Sicherheit_1000x700_21ddee9079.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                По бокам конструкции ворот установлена световая решетка, которая
                предназначена контролировать закрытие воротного полотна до 2,5
                метров. Такая бесконтактная система является гарантом
                безопасности, и соответствует принятым стандартам DIN EN
                13241-1.<br />
                Современная световая завеса максимально обеспечивает
                безопасность ворот и установка таких механизмов как
                фотоэлектрический барьер или предохранитель замыкающего контура,
                становится лишней. Продукция Hörmann отличается функциональной
                надежностью, очень удобна в обслуживании и монтаже, а также
                имеет соответствующие сертификаты безопасности.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Оптимизация производственных процессов
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_optimierte_betriebsablaeufe_1000x700_4c9a5a9364.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Блок управления, что содержит преобразователь частоты -
                гарантирует плавное открытие и замедление ворот, почти бесшумную
                работу, а также позволяет значительно уменьшить нагрузку на
                рабочие механизмы.
                <br />
                Тяжелые теплоизолированные ворота отлично работают за счет
                мощных пружин, а износостойкие противовесы обеспечивают
                основательное функционирование секционных скоростных ворот Speed
                с направляющими типа V и H. Благодаря отличному функционалу
                ворот, на предприятиях значительно улучшаются климатические
                условия, и повышается уровень производственных процессов.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Эффективная теплоизоляция
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_effizienteWaermedaemmung_1000x700_01_511f2cf44a.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Ворота состоят из двух качественных оцинкованных белых
                алюминиевых пластин (RAL 9006) – этот металл очень устойчив к
                воздействию сурового климата. Панели надежно закрепляются между
                собой, а образовавшаяся внутренняя пустота заполняется
                полиуретановой пеной. Изолирующий материал, саморасширяясь
                надежно заполняет все щели, проникая даже в самые недоступные
                уголки, что гарантирует жесткую конструкцию ворот, и высокую
                теплоизоляцию.
                <br />
                <br />
                Внутреннюю поверхность скоростных ворот, укрывает защитный слой
                изоляционного жароустойчивого материала, а внешняя сторона
                отлично декорирована поверхностью Micrograin.
                <br />
                <br />
                Дополнительную теплоизоляцию обеспечивает ThermoFrame. По
                периметру ворот, между стеной и крепежом, установлена
                герметизирующая прокладка из износостойкого каучука, которая
                предупреждает проникновение холода и потерю тепла. Уплотнение
                ThermoFrame легко устанавливается, и возможно как дополнительная
                функция к воротам с толщиной секций 4,2 см и 6,7 см. Скоростная
                дверная система Iso Speed Cold 100 уже серийно оснащена данной
                опцией.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Идеальная прозрачность стекол на долгий период пользования
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_dauerhaft_klare_durchsic.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Остекление с помощью пластмассовой поверхности Duratec
                гарантирует надежную износостойкость, максимальную защиту от
                мелких механических повреждений и безупречную прозрачность на
                протяжении всего эксплуатационного периода. Несмотря на
                интенсивные промышленные нагрузки, защитное покрытие сохраняет
                остекленную поверхность ворот в идеальном состоянии. При
                необходимости стекло легко можно заменить. Скоростные ворота с
                толщиной секций 6,7 см выпускаются с тройным остеклением, а
                модель с секциями толщиной 4,2 см производят с двойным
                остеклением.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/WDF8Ec2QV9Q/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              HS  7030  PU – Скоростные спиральные ворота
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/QTg25oyWX1g/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Напольный профиль SoftEdge – надежная защита от повреждений в
              результате столкновений
            </p>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>


   
    <div id="overlay"></div>
       
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
