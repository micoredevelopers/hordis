<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>


  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">Межкомнатные двери</p>
            <p class="goods_k_container_box_desc">
              Межкомнатные двери Hörmann из натурального дерева - это безусловно
              правильный выбор для себя, и своего дома в частности. Они подарят
              ощущение комфорта и уюта, потому как цельное дерево полотна,
              довольно органично обозначает престиж.<br />
              Качество Hörmann гарантирует надежность, изысканность и
              максимальный срок службы своей продукции. Разнообразие дизайна
              порадует - затейливыми узорами из нержавеющей стали, элементами
              остекления, и конечно же утонченной классикой и дерзкой
              современностью.<br />
              Компания “Hördis” предлагает огромный выбор дверей Hörmann в
              Одессе, которые порадуют самых привередливых ценителей
              прекрасного, и удовлетворят высокие требования к качеству.
            </p>
          </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-3 col-12">
                <p class="goods_k_container_box_desc">
                    По
                    номерам в разделе (контакты), можно заказать установку и
                    последующее сервисное обслуживание немецкой продукции. Также можно
                    увидеть образцы ТМ Hörmann в ШОУ-РУМ, адрес которого, тоже в
                    “контактах”.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a href="./catalog/DveryMejkomnatniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/base-line/base-line.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Элегантность и современность – BaseLine 749
            </p>
            <p class="goods_k_container_box_desc mb-0">
              Исключительный дизайн внутренних дверей BaseLine – подкупает своей
              элегантностью и современностью. Модели этой серии выполнены из
              цельного дверного полотна с фальцовкой, но есть и без нее, в
              комплексе с подобранной отделкой они безупречно подойдут к самому
              неординарному дизайну дома, либо квартиры
              <br />
              Межкомнатные деревянные дверные полотна BaseLine имеют 5 вариантов
              профессиональной отделки поверхности:
             
            </p>
          </div>
          <div class="goods_k_container_box col-12">
              <p class="goods_k_container_box_desc">
                - Duradecor - идеально гладкое дверное полотно, с ударопрочным
                эффектом. Представлены в 2 цветовых тонах, и 3 вариантах обработки
                под естественное дерево.<br />
                - Duradecor – поверхность структурная с природным оттенком, и
                состоит из 9 вариантов обработки “под дерево”.<br />
                - Synchron Duradecor – максимально матовое, структурное дверное
                полотно, совершенная имитация, в 3 вариантах обработки “под
                дерево”.
              <br>
                  - Натуральный шпон – неповторимость в 3 экспозициях декора.<br />
                  - Лакокрасочное покрытие – с блеском в 4 цветах или на выбор из
                  коллекции RAL.<br />
                  Благодаря межкомнатным дверям BaseLine - дом всегда будет наполнен
                  атмосферой уюта и комфорта.<br />

                  <br />
                  Все деревянные внутренние двери BaseLine поставляются с пятью
                  высококачественными вариантами отделки поверхности: <br />
                  - Гладкая поверхность Duradecor – особенно ударопрочная – 2 цвета
                  и 3 варианта отделки под дерево - Структурная поверхность
                  Duradecor – натуральный характер – 9 вариантов отделки под дерево
                  - Структурная поверхность Duradecor Synchron – идеальная имитация
                  – 3 варианта отделки под дерево - Натуральный шпон – уникальность
                  – 3 варианта шпона - Л/к покрытие – блестящее, 4 цвета или RAL по
                  выбору
              </p>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-1.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Структурная поверхность Duradecor с тиснением, базальтовый дуб
                  Вариант исполнения без фальца.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Структурная поверхность Duradecor с тиснением, базальтовый дуб
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-2.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Л/к покрытие белого цвета RAL 9010 Вариант исполнения с
                  фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Л/к покрытие белого цвета RAL 9010
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-3.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Л/к покрытие белого цвета RAL 9016 Вариант исполнения с
                  фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Л/к покрытие белого цвета RAL 9016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-4.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, французский бук Натуральный шпон «клен»
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Duradecor, французский бук</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-5.jpg') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, структурная поверхность «необработанный дуб»
                  Вариант исполнения с фальцем.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Duradecor, структурная поверхность «необработанный дуб»
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-6.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Натуральный шпон «клен» Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Натуральный шпон «клен»</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-7.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Натуральный шпон «клен» С вырезом под остекление LA 5 Вариант
                  исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Натуральный шпон «клен»</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/base-line/img-8.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, канадский клен С вырезом под остекление по DIN
                  Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Duradecor, канадский клен</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Тройная возможность -ConceptLine
            </p>
            <p class="goods_k_container_box_desc">
              Концепция пространства легко реализуется благодаря дверям из серии
              ConceptLine. В соответствии с критериями и пожеланиями владельца
              можно сочетать ультраматовость, структурность и блеск поверхности
              - с разнообразными цветовыми решениями (белый, оттенок «серого
              антрацита», или выбрать из светло-серых тонов). Вся межкомнатная
              продукция Hörmann - отличается стабильным качеством, удобством в
              уходе и практичностью.
              <br />
              - Структурная поверхность – полутона и оттенки на этой плоскости
              играют на контрастах, делая дверное полотно более интересным и
              оригинальным.<br />
              - Глянцевая поверхность – благородство цветовой гаммы поверхности
              с идеальным блеском, дает акцент на игру света и отражений.<br />
              - Матовая поверхность – дарит ощущение прагматичности, приятна для
              глаз и на ощупь, устойчива к загрязнениям и следам от пальцев рук.
              Ультраматовость Duradecor сочетает - бархат и шелк в одном
              полотне.<br />
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/concept/concept.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-11.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  НОВИНКА Исполнение с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Duradecor, ультраматовая поверхность цвета серого антрацита RAL
              7016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-12.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Глянцевая поверхность цвета серого антрацита RAL 7016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-13.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Duradecor с рифленой поверхностью цвета серого антрацита RAL 7016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-14.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Ультраматовая поверхность Duradecor белого цвета RAL 9016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-15.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Глянцевая поверхность белого цвета RAL 9016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-16.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Рифленая поверхность Duradecor белого цвета RAL 9016
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-17.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Ультраматовая поверхность Duradecor светло-серого цвета RAL 7035
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-18.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Глянцевая поверхность светло-серого цвета RAL 7035
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/concept/img-19.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">НОВИНКА Исполнение с фальцем</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Рифленая поверхность Duradecor светло-серого цвета RAL 7035
            </p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
        <!--<div class="estimation_k_container_box show_more">-->
        <!--<button class="estimation_k_container_btn">Показать еще</button>-->
        <!--</div>-->
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/design-line/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Мечты стают реальностью: DesignLine
            </p>
            <p class="goods_k_container_box_desc">
              Двери серии DesignLine превращают мечты в реальность. Сделать
              акцент в оформление межкомнатных дверей, возможно с помощью
              орнамента из нержавеющей стали или углубления линий рисунка. И
              несомненно прекрасно подойдут для оформления дома -
              ультрасовременные двери из стекла, эко-модели из отобранного
              шпона, или дверное полотно с ярким насыщенным цветным покрытием.
              <br />
              <br />
              Заказать межкомнатные двери DesignLine можно в разнообразных
              вариантах:<br />
              - Plain - с нанесенной аппликацией на всю поверхность дверного
              полотна;<br />
              - Steel - с частичным нанесением мотивов;<br />
              - Stripe - со стильными канавками тиснеными;<br />
              - Inlay - эксклюзивные лизены из натурального дерева, что
              размещены по всему полотну;<br />
              - Groove - с канавками углубленными;<br />
              - GrooveGlas - идеальное сочетание к дверям Groove - это
              пескоструйная отделка и шелкография;<br />
              - Rail - дерево в сочетании со стеклом, будет актуально к модели
              DesignLine.<br />
            </p>
          </div>
        </div>
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_desc">
              Современная классика - это серия DesignLine, что сможет раскрыть
              стиль, в абсолютно иной плоскости. Практичность и совершенство -
              это все в рамках немецкого производителя.<br />
              Подберите свою уникальную дверь для идеального благоустройства
              загородного дома:<br />
              - Georgia – профильная планка подчеркнет - аристократизм
              мотивов;<br />
              - Carolina - профилированное дверное полотно, обеспечит
              достаточной элегантностью;<br />
              - Virginia - филенчатые двери, подарят первоклассный роскошный
              дизайн.<br />
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/design-line/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-1.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, белого цвета RAL 9016 Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Steel 20</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-2.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, белого цвета RAL 9016 Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Plain 27-7</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-3.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Л/к покрытие белого цвета Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Carolina 2</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-4.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor, канадский клен Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Steel 27</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-4.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Duradecor «белый пергамент» Стекло с мотивом 2 с матовыми
                  поверхностями Исполнение с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Rail 1</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-5.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Натуральный шпон «клен» Стекло с мотивом 2 с матовыми
                  поверхностями Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Rail 2</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-6.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Поверхность Duradecor «ясень белый» Стекло Satinato Вариант
                  исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Rail 2</p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/design-line/door-7.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Натуральный шпон «белый дуб» Вариант исполнения с фальцем
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Georgia 2</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">GlassLine</p>
            <p class="goods_k_container_box_desc">
              Благородство, чувство изысканности и открытость – главные
              составляющие, при выборе дверей серии GlassLine. Подберите
              стеклянную дверь, которая подойдет под все ваши требования:
              раздвижная, распашная или с деревянной коробкой. Все модели
              соответствуют высшему качеству и отличному дизайну.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/between-doors/glass-line/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов</p>
        <div class="types_k_container row">
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/glass-line/door-1.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">Белый дуб Стекло с мотивом 1</p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Стеклянная дверь с коробкой, облицованной натуральным шпоном
            </p>
          </div>
          <div class="types_k_container_box col-xl-3 col-lg-4 col-md-6 col-12">
            <div
              class="types_k_container_box_image"
              style="background:url('img/between-doors/glass-line/door-2.png') center/cover no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Белый цвет RAL 9016 Стекло с мотивом 2
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">
              Стеклянная дверь с коробкой с поверхностью Duradecor
            </p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Разнообразие товарного ассортимента
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_Tuerblatt_Zargen_Ausfuehrungen_6e5abb74bb.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Сочетание дверного полотна с коробкой, у производителя Hörmann -
                разнообразно, что удовлетворит любой, самый изысканный и
                привередливый вкус. Наша команда поможет выбрать именно тот
                вариант, который будет соответствовать критериям и предпочтениям
                владельца. Для любителей классики создано дверное полотно с
                фальцем и коробкой с закругленными кромками.<br />
                Для почитателей строгого стиля, подойдут четкие линии, что
                представлены в таком же варианте, только с прямоугольными
                кромками. Вариант дверного полотна без фальца и коробкой с
                прямоугольной кромкой, будет несомненно стильным решением для
                современного дизайна.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Безупречная красота и долговечность: защита кромок 4Protect.
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_USP_4Protect_Kantenschutz_75ef827357.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                4Protect – превосходная серийная защита прямоугольных кромок у
                межкомнатных деревянных дверей Duradecor с несколькими
                вариантами цветовой гаммы («белый лак», серый цвет или оттенок
                серого антрацита). Несомненным преимуществом порадует 4-кратная
                защита кромок, которая делает дверное полотно:
                <br />
                <br />
                - устойчивым к ударам, высоким нагрузкам и механическим
                повреждениям;<br />
                - износостойким и надежным на долгие годы;<br />
                - элегантным и изящным, благодаря бесшовному и беззазорному
                дизайну.<br /><br />
                Теперь можно не волноваться о внешнем виде межкомнатных дверей
                из дерева. Они под надежной защитой от ежедневных нагрузок, и
                будут долго радовать своих домочадцев.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Изящность и натуральность: структурная поверхность Duradecor
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_USP_Duradecor_1000x700_8934f88a4f.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Тонкость фактуры структурной поверхности позволить ощутить то,
                что мы видим. Текстурная имитация дерева - в виде тиснения на
                дверном полотне Duradecor, позволит почувствовать фактуру
                древесины, ощутить ее шероховатость и натуральность.Текстура
                рисунка идентична декоративной отделке. Полная гармония
                структуры шлифованной поверхности с тиснением рисунка
                декоративной отделки, делает двери уникальными и неповторимыми.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Скажите “НЕТ” преградам: привод для дверей PortaMatic
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_PortaMatic_2sp_071b3f7a26.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Нужно больше свободы или есть ограничение в движении? С приводом
                PortaMatic открытие и закрытие дверей еще никогда не было таким
                легким. Для усовершенствования комфорта и большего удобства
                качества жизни вам необходимо установить на межкомнатные двери
                супер-инновационный привод для PortaMatic. Благодаря такому
                функционалу, управлять дверью можно при помощи выключателя или
                пульта ДУ с любого уголка комнаты.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Ударопрочная поверхность Duradecor
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_USP_stossfest_1000x700_d78c25a202.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Вы не останетесь равнодушны к высококачественной поверхности:
                <br />
                <br />
                С Duradecor теперь не стоит волноваться о внешнем виде
                поверхности двери. В сравнении с другими производителями
                дверного полотна из CPL-пластика, ударопрочность у Duradecor
                выше на 48%. Помимо всего этого, она имеет отменную
                термостойкость, практически не пачкающуюся поверхность, и не
                требует особого ухода.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Тройная концепция для пространства - ConceptLine
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_UPS_3_facheRaumkonzepte_9505904a98.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Дверь ConceptLine – это совершенно новое и стильное оформление
                помещения. В соответствии с критериями и пожеланиями можно
                сочетать структурную, глянцевую и ультра-матовую поверхности с
                разными цветовыми решениями (белый, бледно-серый и насыщенный
                «серый антрацит»).
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Нанопокрытие - стеклянные двери без пятен
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_USP_Ganzglas_NanoBeschichtung_c7d502a64a.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Стеклянные двери с нанопокрытием станут не только новой
                изюминкой в модном интерьере, но и наполнят дом светом и теплом.
                Несмотря на то, что двери имеют изящный и роскошный дизайн, уход
                за ними требует минимальных усилий. Пескоструйное покрытие с
                нанообработкой обезопасит стекло от отпечатков пальцев и
                различных загрязнений. И прекрасным завершением будет
                безупречная комбинация стеклянных и деревянных дверей. Компания
                предлагает возможность - заказать мотивы Groove для стеклянных
                дверей.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Регулируемая облицовочная коробка VarioFiх в стали
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/between-doors/advantages/csm_USP_Stahlfutterzarge_1000x700_7f4552d5cd.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Стальная облицовочная коробка VarioFix производителя Hörmann –
                настоящая находка для неровных стен с разницей до 20 мм. Разница
                в толщине выравнивается благодаря регулируемому по величине
                декоративному коробу. Подобный конструктаж идеально впишется в
                любую постройку, включая квартирные варианты. Для установки
                конструкции не требуется особых усилий и навыков, ведь все очень
                просто подгоняется. Безупречным преобладанием, станет
                долговечность и отменный дизайн. Только коробка VarioFix
                обладает высочайшей износостойкостью и безупречностью форм без
                видимых швов и загнутых краев.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <!--modal window-->

        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>
   

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
