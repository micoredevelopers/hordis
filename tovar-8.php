<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Category</title>
  <!-- <link rel="stylesheet" href="slick/slick.css">
  <link rel="stylesheet" href="slick/slick-theme.css">
  <link rel="stylesheet" href="css/category.css"> -->
  <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
          crossorigin="anonymous"
  />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="css/media.css" />
  <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
          rel="stylesheet"
  />
</head>
<body>

<?php include "header.php"?>


<section class="goods_k">
  <div class="wrapper_k">
    <div class="goods_k_container row m-0">
      <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
        <p class="goods_k_container_box_title">Двери ZK</p>
        <p class="goods_k_container_box_desc">
          Установить дверь во внутренние помещения с соответствием всем
          современным параметрам, но в то же время сберечь бюджет - помогут
          двери серии ZK, от немецкого производителя Hörmann.
          Энергоэффективность этой модели весьма высокая. Они обеспечивают
          безупречную защиту от сквозняков и протягов - как в жилых, так и
          технических помещениях.
          <br />
          Стильный и интересный дизайн отлично вписывается и гармонирует с
          современным интерьером. Дверь ZK отличается не только высокой
          прочностью, но и устойчивостью к деформациям и практически не
          пачкается, очень проста в уходе, и тем самым, довольно удобна в
          повседневной эксплуатации.
          <br />

        </p>
      </div>
      <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
        <div class="goods_k_container_box_slider_big slider">
          <div class="slide">
            <div
                    class="big_slider_image"
                    style="background:url('img/inner-doors/main-1.jpg') center/cover no-repeat"
            ></div>
          </div>
        </div>
      </div>
      <div class="goods_k_container_box order-3 col-12 ">
          <p class="goods_k_container_box_desc">
              Модификация ZK предусмотрена для - детских комнат, а также
              технических помещений. Подобная классификация была определена
              ввиду того, что именно эти М2 более подвержены непредвиденным, но
              более значимым повреждениям дверного полотна и коробки.
              <br />
              <br />
              ШОУ-РУМ в Одессе представляет образцы продукции компании Hörmann,
              среди которой можно увидеть двери серии ZK. Адрес есть на сайте -
              в разделе “контакты”.
          </p>
      </div>
    </div>
  </div>
</section>

<section class="advantage_k">
  <div class="wrapper_k">
    <p class="advantage_k_title">Почему Hörmann?</p>
    <div class="advantage_k_container">
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Стабильное дверное полотно
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="active-arrow-tov advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Waermedaemmung49_1000x700_62f58c8986.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Двустенное дверное полотно толщиной 40 мм с 3-сторонним толстым
            фальцем состоит из оцинкованного стального листа (0,6 мм) и
            сотового заполнения, проклеенного по всей поверхности, что
            делает его особо прочным и устойчивым к деформации.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Хорошее серийное оснащение
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Einbruchhemmung_Tuer_1000x700_251d32d87d.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Внутренняя дверь ZK поставляется серийно со встроенным замком и
            блестящими оцинкованными верхними частями дверных петель V 0020,
            прочно привинченными к усилительным элементам, но без гарнитура
            нажимных ручек.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Как отдельное дверное полотно или готовый к монтажу блок с
            коробкой
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_individuelleGestaltung_1000x700_01_7fd5ef5e8c.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Внутренняя дверь ZK поставляется по Вашему желанию либо как
            отдельное дверное полотно, либо как дверной блок, готовый к
            монтажу, т. е. в комплекте с гарнитуром нажимных ручек и
            специальной угловой коробкой белого цвета.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Легкое и бесшумное закрывание
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Stahlfutterzarge_1000x700_7f4552d5cd.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Дверь оснащена врезным замком с гарнитуром разных ручек по DIN
            18251, класс 3, с отверстием под профильный цилиндр (размер
            сердечника 55 мм). Серийное оснащение двери пластмассовой
            защелкой и стальным ригелем, вставкой замка со сложной бородкой
            и ключом, по заказу также с профильным цилиндром, гарантирует
            легкое и бесшумное закрывание двери ZK.
          </p>
        </div>
      </div>
      <div class="advantage_k_container_item">
        <div class="advantage_box">
          <img
                  src="img/shield.png"
                  alt=""
                  class="advantage_k_container_item_img"
          />
          <p class="advantage_k_container_item_text">
            Универсальность при монтаже
          </p>
          <img
                  src="img/arrow_adv.png"
                  alt=""
                  class="advantage_k_container_item_arrow"
          />
        </div>
        <div class="advantage_k_container_item_tabul">
          <img
                  src="img/fire-doors/advantages/csm_USP_Stahlfutterzarge_1000x700_7f4552d5cd.jpg"
                  alt=""
                  class="advantage_k_container_item_tabul_image"
          />
          <p class="advantage_k_container_item_tabul_text">
            Благодаря выравниванию по высоте и индивидуальному оформлению
            поверхности, двери ZK идеально подходят для ремонта любых
            помещений, от подвала до чердака. Эти двери могут быть с
            лёгкостью установлены в уже существующие стандартные коробки и,
            разумеется, в любые стальные коробки компании Hörmann
            (подготовленные под петли серии V 8000).
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include "formTemplates/formFooter.php" ?>

 <?php include "footer.php"?>
<!--modal window-->

    
<?php include "formTemplates/formContact.php" ?>
  
<?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"
></script>
<script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>

