<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Новости</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="news">
      <div class="container news-content-wrap">
        <h1 class="news-title">Новости</h1>
        <div class="row">
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/24var_.jpg" alt="" />
            </div>
            <h3 class="news-item-title">Новинка! <br> 24 поверхности Duragrain.</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news-1.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/planar_.jpg" alt="" />
            </div>
            <h3 class="news-item-title">Поверхности Planar – элегантность и безупречная гладкость!</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <!-- <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a>
          <a href="news.php" class="col-12  col-sm-6 news-content-item">
            <div class="img-cont">
              <img class="news-item-img" src="img/new_home1.png" alt="" />
            </div>
            <h3 class="news-item-title">Тут будет заголовок новости</h3>
            <p class="news-item-date">10 февраля, 2018</p>
            <span class="news-item-line"></span>
          </a> -->
        </div>
      </div>
    </section>
    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <div id="overlay"></div>
    
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
