<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
</head>
<body style="background-color: #000">
<?php include "header.php"?>
<!--slider section-->
<section class="main-slider-wrap">
    <div class="section_main">
        <div class="slider_cont">
            <!-- <div class="nav_cont prev"></div> -->
            <img src="img/arrow.svg" alt="" class="arrow_bg_slider">
            <div class="slide">
                <img src="img/2.png" alt="">
                <div class="section_main_box">
                    <div class="section_main_box_foot">
                        <p class="section_main_box_foot_title">Для частного жилья</p>
                        <p class="section_main_box_foot_desc">Гаражные и дверные элементы для современного дома - это инновационные системы, что приближают к форматам комфортной и адаптивной европейской жизни.</p>
                        <a href="category.php" class="section_main_box_foot_btn">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="slide active">
                <img src="img/category/industrial.jpg" alt="">
                <div class="section_main_box">
                    <div class="section_main_box_foot">
                        <p class="section_main_box_foot_title">Для промышленности и бизнеса</p>
                        <p class="section_main_box_foot_desc">Промышленные секционные, рулонные, скоростные ворота и перегрузочное оборудование в совмещении с передовыми немецкими технологиями - лучшее решение для оснащения коммерческих помещений.</p>
                        <a href="category-prom.php" class="section_main_box_foot_btn">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="nav_cont next"></div>
            <div class="section_main_box_head">
                <p class="section_main_box_head_title">Официальный партнёр немецкого концерна</p>
                <img src="img/Bitmap.png" alt="" class="section_main_box_head_logo">
            </div>
        </div>
    </div>
    <div class="main-slider">
        <div class="main-slider-item">
            <div class="row main-title-wrap">
                <div class="col-12 text-center">
                    <h1 class="main-slider-title">Официальный партнёр немецкого концерна</h1>
                    <div class="slider-img-box">
                        <img src="img/Bitmap.png" alt="logo">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <h3 class="content-box-title">Для частного жилья</h3>
                    <p class="content-box-text">Гаражные и дверные элементы для современного дома - это инновационные системы, что приближают к форматам комфортной и адаптивной европейской жизни.
                    </p>
                    <a href="category.php" class="content-box-link">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="main-slider-item second">
            <div class="row main-title-wrap">
                <div class="col-12 text-center">
                    <h1 class="main-slider-title">Официальный партнёр немецкого концерна</h1>
                    <div class="slider-img-box">
                        <img src="img/Bitmap.png" alt="logo">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <h3 class="content-box-title">Для промышленности и бизнеса</h3>
                    <p class="content-box-text">Гаражные системы, роллетные, секционные ворота, в совмещении с передовыми немецкими технологиями - лучшее решение для переоснащения коммерческих помещений. </p>
                    <a href="category-prom.php" class="content-box-link">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
    <div class="main-arrow-desc-wrap">
        <div class="main-arrow-next-wrap">
            <p class="content-box-text main-arrow-desc">Для промышленного</p>
            <span class="main-arrow-next"></span>
        </div>
    </div>
    <!-- <div class="main-arrow-next-desctop"></div> -->
    <!-- <div class="main-arrow-next-desctop"></div> -->
</section>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
<script src="js/slick.min.js"></script>
<script src="js/sequence.min.js"></script>
<script src="js/sequence-theme.intro.js"></script>
<script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>