<footer class="footer">
    <div class="wrapper_k">
        <p class="footer_item">hordis.com.ua 2019 (c) by<a href="https://comnd-x.com/" target="_blanck" class="footer_item"> command+x</a></p>

        <div class="social_box">
            <a target="_blank" href="https://www.facebook.com/HordisVorota/" class="social_box_item">
                <img
                    src="img/facebook-logo.svg"
                    alt=""
                    class="social_box_item_img"
                />
            </a>
            <a target="_blank" href="https://www.instagram.com/hordis_vorota_hormann/?hl=ru" class="social_box_item">
                <img src="img/instagram.svg" alt="" class="social_box_item_img" />
            </a>
            <a target="_blank" href="https://msng.link/vi/380963201299" class="social_box_item">
                <img src="img/viber-logo.svg" alt="" class="social_box_item_img" />
            </a>
        </div>
        <a class="footer_link">Связаться с нами</a>
    </div>
</footer>