
<div id="myModal" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title mar-auto h2" id="exampleModalLongTitle">Ваш запрос отправлен</h2>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="w-25 btn btn-secondary" data-dismiss="modal">ок</button>
            </div>
        </div>
    </div>
</div>
