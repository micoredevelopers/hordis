<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>О нас</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>
    <!--    About-us-content    -->
    <section>
      <div class="about-us-img-wrap">
        <img src="img/about-us-img.png" alt="" />
      </div>
      <div class="about-us">
        <div class="about-us-content my-container row">
          <div class="about-us-content-wrap col-lg-6">
            <h3 class="about-us-title">О нас</h3>
            <p class="about-us-text">
              Компания Hördis уже более 10 лет на рынке услуг по продаже
              секционных ворот, входных и межкомнатных дверей и автоматики к ним
              - от надежного производителя. Организация презентует в Одессе
              корпорацию Hörmann, что уже известна широкому кругу потребителей в
              Украине. Сегодня преобладает услуга - установка секционных ворот и
              переоснащение производственных, коммерческих и частных владений -
              новаторскими современными элементами. Компания Hördis всецело
              готова к подобным совершенствованиям, опыт и знания выполняют
              важнейшую составляющую успешного монтажа любой системы ТМ Hörmann.
            </p>
            <p class="about-us-text">
              В городе, компания открыла ШОУ-РУМ, где представлены помимо
              секционных ворот, гаражные, рулонные, к ним же - образцы
              межкомнатных и входных дверей, а также системы автоматики и
              комплектующих к ней. Можно ознакомиться с перегрузочным
              оборудованием и другими элементами современного переоснащения
              коммерческих и частных помещений.
            </p>
            <p class="about-us-text">
              Приветливые менеджеры компетентно ответят на вопросы
              заинтересованных посетителей и онлайн-пользователей. Сотрудники
              организации проходят профильное обучение, а также системное
              повышение квалификации.
            </p>
            <p class="about-us-text">
              Монтажные бригады укомплектованы профильными сотрудниками, что
              оснащены профессиональным инструментом. Мастера компетентно
              проводят установку, и дальнейшее сервисное обслуживание
              монтируемых элементов. Выезд замерщика по Одессе и пригороду
              всегда оперативен, и абсолютно бесплатен.
            </p>
            <p class="about-us-text">
              Миссия компании Hördis - Оснащение проема любой конфигурации,
              современными системами, в согласовании с интересами Заказчика.
            </p>
          </div>
          <div class="about-us-content-wrap col-lg-6">
            <h3 class="about-us-title">Концерн Hörmann</h3>
            <p class="about-us-text">
              Как и полагает зарубежным надежным брендам, что удостоены
              признания потребителей, компания Hörmann начинала свой путь - из
              далеких 50-х годов. Все начиналось с промышленного производства
              ворот Berry, это были оригинальные подъемно-поворотные
              конструкции. В 60-е открылись новые перспективные горизонты, после
              запуска компактных секционных ворот, что открывались вертикально.
              Продукция стала пользоваться спросом - у частных гаражей,
              производственных зданий и небольших мастерских.
            </p>
            <p class="about-us-text">
              С тех времен начался системный рост ассортимента продукции
              Hörmann. Сегодня классические модели наполняют модельный ряд,
              который пополнен - рулонными, откатными и скоростными воротами.
              Сюда же входят такие товары как:
            </p>
            <ul class="pl-4">
              <li class="about-us-text m-0">входные и внутренние двери;</li>
              <li class="about-us-text m-0">дверные коробки;</li>
              <li class="about-us-text m-0">приводы ворот и дверей;</li>
              <li class="about-us-text m-0">
                огнестойкие и дымонепроницаемые перегородки;
              </li>
              <li class="about-us-text m-0">
                системы автоматики для раздвижных дверей;
              </li>
              <li class="about-us-text m-0">перегрузочная техника;</li>
              <li class="about-us-text m-0">
                функционал управления при въезде/выезде автомобилей.
              </li>
            </ul>
            <p class="about-us-text mt-4">
              Результаты надежности - ТМ Hörmann приобрела благодаря наличию
              эффективности использования инновационных ресурсов, что
              приравниваются к стимулятору жизнедеятельности любого предприятия.
              Забота и внедрение новаторских систем - имеют первостепенное
              значение в развитии компании. В условиях сегодняшнего рынка, и
              стремительной смене требований современности, у Hörmann уже
              поставлены новые цели, которые реализуются в соответствии с
              надеждами потребителя.
            </p>
          </div>
        </div>
      </div>
    </section>
    <!--about-us slider-->
  <section class="about-us-slider-section-wrap">
      <div class="about-us-slider-section">
          <div class="about-us-slider-content-wrap my-container row">
              <div class="col-12 col-lg-6">
                  <h3 class="about-us-slider-content-title">Наш ШОУ-РУМ</h3>
                  <p class="about-us-slider-content-text pb-3">
              <span
              >Официальный партнер Hörmann в Одессе компания “Hördis”</span
              >
                      - это надежная и компетентная установка с гарантией и сервисным
                      обслуживанием по Одессе, и за ее пределами. <br />
                      В ШОУ-РУМе представлены образцы гаражных секционных ворот как для
                      частного владения, так и промышленного сектора; входные и
                      межкомнатные двери; автоматика для гаражных и въездных ворот и все
                      необходимые к ним аксессуары.
                  </p>
              </div>
              <div class="col-12 col-lg-6 text-center pad-0">
                  <div class="about-us-main-slider">
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/1.jpg" style="object-position: 40% 50%" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/2.jpg" style="object-position: 38% 50%" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/3.jpg" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/4.jpg" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/5.jpg" alt="" />
                      </div>

                  </div>
                  <div class="about-us-slider-arrows best_class_forever">
                      <a href="./gallery.php" class="category_k_box_btn">Больше работ</a>
                      <div class="arrow_k_block_about">
                          <div class="about-us-arrows about-us-arrow-prev"></div>
                          <div class="about-us-slider_modal_counter">
                              <span class="current"></span>
                              /
                              <span class="total"></span>
                          </div>
                          <div class="about-us-arrows about-us-arrow-next"></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    <div id="overlay"></div>
     
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
