<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>

    <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row align-items-start">
          <div class="goods_k_container_box col-lg-6 col-12 order-1 order-lg-0">
            <p class="goods_k_container_box_title">
              Рулонные ворота и Рулонные решетки
            </p>
            <p class="goods_k_container_box_desc mb-1">
              Рулонные ворота и рулонные решетки компании Hörmann - это
              идеальное решение для закрытия любого дверного проёма. Данная
              несложная конструкция стабильно подтверждает свою рентабельность,
              простоту применения, легкость и качество. Владельцу помещения или
              магазина можно максимально ограничить занимаемую площадь проема
              после установки рулонников. Ворота и решетки, настолько экономно
              занимают необходимое пространство, что ни в коем случае не будет
              занято лишнего места. Они практично сматываются, и совершенно не
              мешают в проеме, что обеспечивает свободный монтаж для самых малых
              помещений.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12 order-0 order-lg-1">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/Industrial/industrial_gates/csm_Rolltore2_1000x700_ba3e7bb697.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
            <div class="col-12 goods_k_container_box order-2">
                <p class="goods_k_container_box_desc">
                    Конструкция проста в применение и содержит минимальное количество
                    составляющих. Все компоненты изготовлены из сплава алюминия и
                    стали, что подтверждает их износостойкость и надежность на долгие
                    годы эксплуатации. Конструкции могут крепиться к наружной стене
                    здания, что делает их компактными, а также позволяет быть
                    энергосберегательными.
                    <br />
                    Рулонные ворота и решетки RollMatic довольно-таки легко
                    транспортируются, благодаря своей компактности. Монтаж и демонтаж
                    устройства настолько прост, что не займет много времени. RollMatic
                    - это идеальное решение для закрытия абсолютного любого дверного
                    проема, с применением ручного или механизированного способа.
                    <br />
                    Рулонные ворота и решетки RollMatic ТМ Hörmann в Одессе предлагает
                    компания “Hördis”. Организация открыла специализированный ШОУ-РУМ
                    в городе, где можно увидеть некоторые образцы, а также заказать
                    монтаж новаторских систем, с последующим сервисным обслуживанием.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a href="./catalog/VorotaRulonniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Инновационный функционал пружин растяжения
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_zugfedertechnik_rolltore_1000x700_3648cd0698.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Для качественной эксплуатации рулонных ворот задействуется
                система пружин. Это позволяет без особых усилий проводить
                манипуляции по открытию и закрытию рулонных ворот и решеток SB и
                TGT размером до 6000 х 4500 мм. вручную. Для еще более
                практичного и удобного пользования рулонными воротами и
                решетками, предоставляется возможность приобрести
                соответствующий привод. Это совершенно лишит проблемы с
                открыванием и закрыванием ворот, а также процедуру движения
                ворот можно будет осуществить механизированным способом на
                расстоянии. Подобная система удобна как на предприятиях, так и
                частного пользования.
                <br />
                Привод увеличит длительность времени эксплуатации ворот и
                решеток, за счет лояльного воздействия на механизмы.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Защита без участия датчиков на полотнах ворот
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_Sicherheit_Torbehang_1000x700_5d9ca24f10.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Во время работы рулонных ворот, обеспечивается полная
                защищенность, обусловленная наличием привода. В свою очередь -
                приводы оснащены ограничителем усилий, но только для серий - WA
                300 R S4 / WA 300 AR S4, ворота и решетки SB и TGT сочетающиеся
                с этими приводными сериями, создают оптимальные условия
                ограничения.
                <br />
                <br />
                В большинстве модельного ряда ворот установлено устройство
                ограничителя усиления, и такой функционал освобождает от
                оснащения предохранителем замыкающего контура и системы защиты
                от затягивания. Подобная новация не лишает возможности
                соответствовать стандартам качества по сертификации DIN EN
                12453.
                <br />
                <br />
                Отсутствие на рулонных воротах каких либо датчиков,
                предотвращает вероятность порчи проводов, увеличивает
                износостойкость, а также снижает периодичность обращения к
                сервисному обслуживанию.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Безопасные и динамические характеристики открытия ворот
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_sichere_toroeffnung_fu-antrieb_1000x700_4b9846a3aa.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Рулонные ворота обладают свойством безопасного и быстрого
                открывания и закрывания. Это обусловлено оптимизацией привода
                FU, набора принадлежностей S6 и блока управления, которые
                упрощают и делают безопасной работу механизмов рулонных ворот DD
                с полотном Decotherm. Использование ворот с полотном Decotherm,
                позволяет двигаться конструкции в среднем режиме - 1,1 м/с.
                Приобретая рулонные ворота RollMatic, существенно
                просматривается не только качество, удобство и комфорт, а и
                низкая ценовая политика. По сравнению с ценой приобретения
                скоростных ворот, рулонные RollMatic намного дешевле и
                компактных монтажных размеров.
                <br />
                <br />
                Средний режим движения рулонных ворот снижает нагрузку на
                механизмы, это позволяет увеличить срок эксплуатации и
                сэкономить деньги на техническое обслуживание системы движения
                конструкции.
                <br />
                <br />
                Уникальность и надежность воротам придает световая решетка,
                которая безупречно защищена от нежелательных внешних
                повреждений, что увеличивает работоспособность и срок
                эксплуатации рулонных ворот.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Длительный срок службы благодаря продуманной до деталей
                конструкции
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_sinnvolle_details_rolltore_1000x700_4666deea0b.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Компания позаботилась о мельчайших деталях конструкции рулонных
                ворот. Даже при попытке ограбления, ворота не смогут быть быстро
                взломаны, в связи с тем, что профиля конструкции очень сложно,
                да и практически невозможно вытащить с направляющих.
                <br />
                <br />
                В случае сложностей с погодными условиями и сильными ветрами,
                рулонные ворота не будут повреждены, в связи с оснащением
                полотна ворот ветровыми крюками. Кроме этого, ворота DD защищены
                приемной воронкой, которая играет немаловажную роль в
                амортизации рулонных ворот, в случаях замены воронки,
                потребность в трудовых и финансовых вложениях - исключается.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>

        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
