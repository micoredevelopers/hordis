<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Автоматика – внутренний «мир» дверей
            </p>
            <p class="goods_k_container_box_desc mb-0">
              Двери, оснащенные автоматическим приводом PotraMatik компании
              Hörmann, вносят в жизнь человека комфорт и простоту пользования
              конструкцией. <br />
              <br />
              Привод PotraMatik отражает что-то большее, чем обычная легкость
              закрытия или открытия двери, это способствует внесению
              оптимального комфорта и дополнительного удобства человеку нашего
              времени. Подобный функционал имеет отражение в заботе о людях,
              имеющих физические ограничения, а это дает базу для расширения
              границ. Работа автоматики осуществляется посредством пульта ДУ или
              простого выключателя. Автоматика дверей не требует большого
              потребления электроэнергии, поскольку оно минимальное и
              расходуется экономно, но моральное удовлетворение от такой
              новации, подымается выше.
            </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/inner/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-lg-12 col-12">
            <br />
            Аналогичная автоматика, других марок сегодня предлагается по более
            значимой цене, чем у Hörmann, что предлагает более выгодное
            предложение – порядка 25%. Отличительной особенностью среди других
            дверных автоматических приводов это то, что привод PotraMatik
            признан эксклюзивным и прошедшим испытание в Немецком сообществе
            геронто-техники (GGT). С этим уникальным инновационным решением
            поколению возрастного сегмента 50+ гарантирован комфорт, забота и
            существенное улучшение качества жизни.
            <br />
            <br />
            В офисе компании “Hördis” можно узнать больше об автоматическом
            приводе PotraMatik от Hörmann в Одессе, а также организация
            приглашает в ШОУ-РУМ, что тоже открыт в городе, для посетителей.
          </p>
          <div class="box_btns mt-4">
            <a id="go" href="#" class="box_btns_item">Узнать цену</a>
            <a href="./catalog/PrivodyVnutrenniye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
          </div>
        </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Полноценная жизнь без особых усилий
            </p>
            <p class="goods_k_container_box_desc">
              Автоматический привод для внутренних дверей PotraMatik – это,
              прежде всего свобода в передвижении и комфортное использование
              повседневных бонусов. Пришло то время, когда люди, имеющие
              физические ограничения могут без сторонней помощи передвигаться в
              своем жилище.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/inner/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/inner/main-3.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Полноценная жизнь без особых усилий
            </p>
            <p class="goods_k_container_box_desc">
              Автоматический привод для внутренних дверей PotraMatik – это,
              прежде всего свобода в передвижении и комфортное использование
              повседневных бонусов. Пришло то время, когда люди, имеющие
              физические ограничения могут без сторонней помощи передвигаться в
              своем жилище.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">Свобода без преград</p>
            <p class="goods_k_container_box_desc">
              Легкость и комфорт перемещения по своему жилью нужны и тем людям,
              которые не отягощены ограничениями своего здоровья. С
              автоматическим приводом для дверей PotraMatik нет нужды думать о
              предстоящих трудностях, заключающихся в открывании дверей, и даже
              в то время, если ваши обе руки заняты сумками или чем-то еще.
            </p>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/inner/main-4.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/drives/inner/main-5.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">Новый подход к чистоте</p>
            <p class="goods_k_container_box_desc">
              При контакте с ручками дверей, как в офисе, так и мед-центрах или
              других учреждениях у большинства людей возникают ощущения
              дискомфорта, которые вольно или невольно заставляют волноваться за
              свое здоровье. При этих вынужденных контактах существует
              повышенная опасность «поймать» какое-либо инфекционное
              заболевание.
              <br />
              <br />
              В данном случае можно быть абсолютно спокойным - бесконтактный
              выключатель автоматического дверного привода PotraMatik стоит на
              страже всеобщего здоровья, и гарантирует минимальный контакт с
              поверхностями общего пользования.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Безопасность без компромиссов
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/outter/advantages/csm_USP_Sicherer_Tuerbetrieb_PortaMatic_11690e0a2d.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Главный фундамент компании Hörmann – это безопасность и комфорт.
                Простота и легкость в управлении дверью достигла небывалых
                высот. Закрыть и открыть дверь благодаря приводу PotraMatik –
                это так просто, а плавность динамики движения дверей достигло
                идеальной бесшумности. Лишь легкое прикосновение к дверям,
                способствует мгновенной остановке их закрытия, чем
                обеспечивается высокий уровень безопасности.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Радиосистема – вершина инноваций
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/outter/advantages/csm_USP_Innovatives_Funksystem_PortaMatic_d2e0918b76.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Пульт ДУ и беспроводной выключатель ДУ существенно облегчает
                управление приводом. Эта инновационная радиосистема BiSecur дает
                возможность осуществлять запрос о том, в каком положении
                находится дверь, а потому, освобождается время на проверку
                положения дверей в доме, закрыты они или открыты, можно узнать в
                секунды. И еще одно неоспоримое удобство пульта ДУ и выключателя
                ДУ – это управление вмонтированным в привод освещением, которое
                осуществляется светодиодными лампами.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Проще простого: монтируем и подключаем
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/outter/advantages/csm_USP_Einfache_Montage_PortaMatic_353f5810ec.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Установка дверного привода PotraMatik - это несложное
                мероприятие, достаточно лишь основательно зафиксировать систему
                прямо на двери или же дверном коробе.<br />
                Сильная сторона монтажа привода – это допустимость монтирования
                с помощью обычного приклеивания шины на любую поверхность, а в
                случае надобности, можно просто снять привод. Клеевой след не
                останется, но если возникнет желание снова установить привод на
                то же место, то его можно использовать повторно. Это особо
                актуально, если монтаж привода осуществляется на идеально
                гладкой поверхности стеклянной двери или же при переезде на
                новое место жительства.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Оптико-акустические функции для комфорта
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/drives/outter/advantages/csm_USP_Komfortfunktionen_Portamatic_596efbec1e.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Функционалом привода PotraMatik производится обеспечение дверей
                акустическим сигналом с одновременным включением вмонтированных
                светодиодных элементов. Подобное нововведение обеспечивает покой
                владельцам и их гостям в темное время суток, или при очень
                плохом освещении.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/JW20xX0W04U/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Türantrieb PortaMatic - Barrierefreies und komfortables Wohnen
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/a4HZLCgRgiE/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Türantrieb PortaMatic - Barrierefreies und komfortables Wohnen
            </p>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>

     
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
