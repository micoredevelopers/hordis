<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css"> -->
    <!-- <link rel="stylesheet" href="slick/slick-theme.css"> -->
    <!-- <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section
      class="main_k"
      style="background:url('img/category/private.jpg') center/cover no-repeat"
    >
      <div class="wrapper_k">
        <div class="main_k_container">
          <p class="main_k_container_title">Частное жилье</p>
        </div>
      </div>
    </section>

    <section class="cost_k">
      <div class="wrapper_k">
        <div class="cost_k_container row">
          <div class="col-12 col-lg-6">
            <!--                cost_k_container_image-->
            <img src="img/cost.png" alt="" class=" w-100" />
          </div>
          <div class="col-12 col-lg-6 cost_k_container_box">
            <h2 class="box_title">
              Ворота и двери Hörmann - Передовые технологии для частных владений
            </h2>
            <p class="box_desc mb-1">
              Автоматические ворота Hörmann - это достойное оформление
              собственного частного владения. Продукция немецкого, крупнейшего в
              Европе производителя - уже получила признание на украинском рынке,
              в сфере ворот и дверей - для частных домовладений. Отменное
              качество и исключительный дизайн, отображаются во всех элементах,
              что предлагает поставщик - ворот, дверей, коробов, а также
              электроприводов, которые представлены в каталогах нашего сайта.
              <br>
              <br>
              Внешнее оформление фасада следует не только декорировать, но и
              гармонично внести современную систему управления. Hörmann
              предлагает - удобство, инновационный функционал и визуальную
              эстетику, а это именно то, что сегодня выбирает украинский
              потребитель.
            </p>
          </div>
          <div class="col-12 cost_k_container_box">
              <h2 class="box_title">Предприятия Hörmann - Понятный режим управления</h2>
            <p class="box_desc">
              Преображение обыденных вещей становится возможным - только
              благодаря новому видению, и стремлению к более высоким стандартам.
              Немецкий концерн предлагает сегодня гаражные двери из стали и
              дерева, в широчайшем исполнении, гаражные ворота
              подъемно-поворотные Berry, а также гаражные рулонные ворота.
              Управление автоматикой никогда ранее, не было таким простым и
              одновременно практичным.
                <br>
                <br>
                Технологии идут вперед, упрощая жизнь
              современному человечеству.
                <br>
                <br>
                Представитель Hörmann в Одессе компания
              “Hördis” - это надежная и компетентная установка с гарантией и
              сервисным обслуживанием по городу, и за его пределами. Узнать
              стоимость ворот или дверей, а также монтажа - можно у менеджеров
              офиса по номерам, что указаны на сайте или посетить в городе
              ШОУ-РУМ, что открыт организацией. Выезд по Украине обсуждается в
              индивидуальном порядке, и на взаимовыгодных условиях.
                <br>
                <br>
                Купить продукцию Hörmann можно также на нашем сайте - листайте каталог, и
              выбирайте идеальное внешнее оформление собственного частного
              домовладения.
            </p>
            <a href="#" id="go" class="box_btn">Узнать цену</a>
          </div>
        </div>
      </div>
    </section>

    <section class="property_k">
      <div class="wrapper_k">
        <p class="property_k_title">Hörmann для частных владений</p>
        <div class="property_k_container">
          <a
            href="pod-category.php"
            data-podcategory="#pod_1"
            class="property_k_container_box active"
          >
            <p class="property_k_container_box_text">Гаражные ворота</p>
            <div class="underline_k"></div>
          </a>
          <a
            href="pod-category-1.php"
            data-podcategory="#pod_2"
            class="property_k_container_box"
          >
            <p class="property_k_container_box_text">Двери</p>
            <div class="underline_k"></div>
          </a>
          <a
            href="pod-category-2.php"
            data-podcategory="#pod_3"
            class="property_k_container_box"
          >
            <p class="property_k_container_box_text">Приводы</p>
            <div class="underline_k"></div>
          </a>

          <div
            class="property_k_container_bg active"
            id="pod_1"
            style="background:url('img/category/garage-gate.jpg') center/cover no-repeat;"
          ></div>
          <div
            class="property_k_container_bg"
            id="pod_2"
            style="background:url('img/__door.jpg') center/cover no-repeat;"
          ></div>
          <div
            class="property_k_container_bg"
            id="pod_3"
            style="background:url('img/category/privods.jpg') center/cover no-repeat;"
          ></div>
        </div>

        <div class="property_k_slider">
          <div class="slider">
            <div class="slide">
              <a
                href="pod-category.php"
                class="box"
                style="background:url('img/category/garage-gate.jpg') center/cover no-repeat"
              >
                <div class="overlay"></div>
                <p class="box_text">Гаражные ворота</p>
              </a>
            </div>
            <div class="slide">
              <a
                href="pod-category-1.php"
                class="box"
                style="background:url('img/__door.jpg') center/cover no-repeat"
              >
                <div class="overlay"></div>
                <p class="box_text">Двери</p>
              </a>
            </div>
            <div class="slide">
              <a
                href="pod-category-2.php"
                class="box"
                style="background:url('img/category/privods.jpg') center/cover no-repeat"
              >
                <div class="overlay"></div>
                <p class="box_text">Приводы</p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>


  <section class="about-us-slider-section-wrap">
      <div class="about-us-slider-section">
          <div class="about-us-slider-content-wrap my-container row">
              <div class="col-12 col-lg-6">
                  <h3 class="about-us-slider-content-title">Наш ШОУ-РУМ</h3>
                  <p class="about-us-slider-content-text pb-3">
              <span
              >Официальный партнер Hörmann в Одессе компания “Hördis”</span
              >
                      - это надежная и компетентная установка с гарантией и сервисным
                      обслуживанием по Одессе, и за ее пределами. <br />
                      В ШОУ-РУМе представлены образцы гаражных секционных ворот как для
                      частного владения, так и промышленного сектора; входные и
                      межкомнатные двери; автоматика для гаражных и въездных ворот и все
                      необходимые к ним аксессуары.
                  </p>
              </div>
              <div class="col-12 col-lg-6 text-center pad-0">
                  <div class="about-us-main-slider">
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/1.jpg" style="object-position: 40% 50%" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/2.jpg" style="object-position: 38% 50%" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/3.jpg" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/4.jpg" alt="" />
                      </div>
                      <div class="about-us-main-slider-item">
                          <img src="./img/gallery2/5.jpg" alt="" />
                      </div>

                  </div>
                  <div class="about-us-slider-arrows best_class_forever">
                      <a href="./gallery.php" class="category_k_box_btn">Больше работ</a>
                      <div class="arrow_k_block_about">
                          <div class="about-us-arrows about-us-arrow-prev"></div>
                          <div class="about-us-slider_modal_counter">
                              <span class="current"></span>
                              /
                              <span class="total"></span>
                          </div>
                          <div class="about-us-arrows about-us-arrow-next"></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
    <?php include "formTemplates/formFooter.php" ?>

    <?php include "footer.php"?>
    <!--modal window-->

        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?>
  <div id="overlay"></div>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
