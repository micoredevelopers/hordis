<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>


    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container flex-column-reverse flex-lg-row row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Надежная входная дверь Thermo65 и Thermo46
            </p>
            <p class="goods_k_container_box_desc">
              Двери могут быть разными - показными, комфортными, или же просто
              достойными. Входная дверь из стали ТМ Hörmann Thermo-65 или
              Thermo-46 – это гарант отменной безупречности и безопасности.<br />
              Высококачественные материалы, из которых изготовлены двери,
              относятся к редкому классу. Они имеют замечательную теплоизоляцию
              и безупречную защиту от взлома. Немецкий производитель предлагает
              богатый набор мотивов, и это помогает каждому сделать свой выбор.
              <br /><br />
              Заказать входную дверь Hörmann в Одессе, согласовав модель и
              другие важные моменты, возможно в компании “Hördis”, в ШОУ-РУМе.
              Менеджеры предоставят необходимые атрибуты для более близкого
              знакомства с продукцией, и согласуют время реализации монтажа.
              Контакты - есть на сайте.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog/DveriThermo.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/main-3.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/main-1.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/main-2.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/main-3.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/thermo-65.jpg') center/contain no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/thermo65-2.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/thermo-65.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/thermo65-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Thermo65 - Без компромисса
            </p>
            <p class="goods_k_container_box_desc">
              Престижный и солидный экстерьер - увлекает многих. Двери Thermo65
              именно для ценителей - авторитетности и статуса.<br />
              Сплошное дверное полотно создает условия для отличной
              теплоизоляции, впрочим толщина его, всего лишь 65 мм. Пустоты
              заполнены твердым пенополиуретаном, что расположился в профиле
              створки из композитного материала. Входная дверь Thermo65 -
              идеально сохраняет тепло - достигая показателей 0,87 Вт/(м2·К). А
              противовзломный замок содержит 5-кратную вариацию закрывания, что
              подтверждает их охранный функционал, и в дополнение дверь
              оборудована системой защиты от взлома RC-2.
              <br /><br />
              Можно подобрать дверь под абсолютный симбиоз фасада дома из - 13
              различных мотивов, 16 цветовых решений и 5-и вариаций с обработкой
              поверхности в стиле Decograin.<br />
              Рекомендуемая цена модели дверей Thermo65 в Одессе - 43 400 грн.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <div class="types_k_container row m-0">
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-1.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                    Мотив 010
                    <br>
                    Переважний антрацитовий сірий колір RAL 7016,
                    <br>
                    <br>
                    дверна ручка з нержавіючої сталі HB 14-2 на сталевому заповненні,
                    <br>
                    коефіцієнт теплопровідності до 1,1 Вт/(м²·K)*
                    <br>
                    <br>
                    Боковий елемент/вікно верхнього світла: подвійне ізоляційне засклення, ззовні багатошарове безпечне скло VSG, зсередини ESG
                    <br>
                    <br>
                    * Залежно від розміру дверей. Указані значення для RAM 1230 × 2180 мм
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 010, рис. 461</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-2.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Мотив 010, рис. 460 Мал.: матовый белый цвет RAL 9016, с
                  декоративными вставками из нержавеющей стали, ручка из
                  нержавеющей стали HB 14-2, коэффициент теплопроводности до
                  0,87 Вт/(м²·K)*. <br />
                  <br />
                  Опция: боковой элемент/окно верхнего света с тройным
                  изоляционным остеклением, снаружи и внутри многослойное
                  безопасное стекло VSG, посредине Float. <br />
                  <br />
                  Все декоративные элементи для мотива 010 представлены в
                  каталоге «Входные двери в дом» на стр. 39.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 010, рис. 460</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-3.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Рис.: Decograin Winchester Oak <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении, с
                  накладным остеклением: тройное изоляционное остекление,
                  снаружи многослойное безопасное стекло VSG, посередине –
                  Float, подвергнутое пескоструйной обработке, внутри
                  однослойное безопасное стекло ESG, прозрачное, коэффициент
                  теплоизоляции UD до 1,0 Вт/(м²·K)* Боковой элемент / окно
                  верхнего света: тройное изоляционное остекление, снаружи VSG,
                  посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 600</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-4.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет серого алюминия RAL 9007 <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении,
                  сплошное остекление: тройное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, посередине – Float,
                  подвергнутое пескоструйной обработке, с прозрачными полосами,
                  внутри однослойное безопасное стекло ESG, прозрачное,
                  коэффициент теплоизоляции UD до 0,98 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 700</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-5.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  коэффициент теплоизоляции UD до 0,87 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG,
                  посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 010</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-6.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Titan Metallic CH 703 с аппликациями под нержавеющую
                  сталь <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  опционально с аппликациями под нержавеющую сталь, коэффициент
                  теплоизоляции UD до 0,87 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи VSG, посередине Float, внутри VSG,
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 015</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-7.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Dark Oak <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении, с
                  закругленным остеклением: тройное изоляционное остекление,
                  снаружи многослойное безопасное стекло VSG, посередине –
                  Float, подвергнутое пескоструйной обработке, с прозрачными
                  полосами, внутри многослойное безопасное стекло VSG,
                  прозрачное, коэффициент теплоизоляции UD до 0,96 Вт/(м²·K)*
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи VSG, посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 850</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-8.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении, с
                  закругленным остеклением: тройное изоляционное остекление,
                  снаружи многослойное безопасное стекло VSG, посередине –
                  Float, подвергнутое пескоструйной обработке, с прозрачными
                  полосами, внутри однослойное безопасное стекло ESG,
                  прозрачное, коэффициент теплоизоляции UD до 0,96 Вт/(м²·K)*
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи VSG, посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 900</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-9.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении, 3
                  прямоугольных остекления с прозрачными полосами по периметру,
                  с рамами из нержавеющей стали: тройное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG,
                  посередине – Float, подвергнутое пескоструйной обработке,
                  внутри однослойное безопасное стекло ESG, прозрачное,
                  коэффициент теплоизоляции UD до 0,99 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 810</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-10.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении,
                  сплошное остекление: тройное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, посередине – Float,
                  подвергнутое пескоструйной обработке, с прозрачными полосами,
                  внутри однослойное безопасное стекло ESG, прозрачное,
                  коэффициент теплоизоляции UD до 0,95 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 750</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-11.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Titan Metallic CH 703 <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении, 4
                  прямоугольных остекления с рамой из нержавеющей стали: тройное
                  изоляционное остекление, снаружи многослойное безопасное
                  стекло VSG, посередине – Float, подвергнутое пескоструйной
                  обработке, внутри многослойное безопасное стекло VSG,
                  прозрачное, коэффициент теплоизоляции UD до 0,93 Вт/(м²·K)*
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи VSG, посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 800</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-12.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении с
                  гофрами, коэффициент теплоизоляции UD до 0,87 Вт/(м²·K)*
                  <br />
                  <br />
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG,
                  посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 515</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-13.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет мха RAL 6005 <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении, с 6
                  полями с кассетами, коэффициент теплоизоляции UD до 0,87
                  Вт/(м²·K)*
                  <br />
                  Боковой элемент / окно верхнего света: тройное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG,
                  посередине Float, внутри VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 100</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-14.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  тройное изоляционное остекление, снаружи многослойное
                  безопасное стекло VSG, посередине – Float, подвергнутое
                  пескоструйной обработке, внутри многослойное безопасное стекло
                  VSG, прозрачное, коэффициент теплоизоляции UD до 1,0
                  Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 410</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-15.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  тройное изоляционное остекление, снаружи многослойное
                  безопасное стекло VSG, посередине – Float, подвергнутое
                  пескоструйной обработке, с прозрачными полосами, внутри
                  многослойное безопасное стекло VSG, прозрачное, коэффициент
                  теплоизоляции UD до 1,0 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 450</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo65/door-16.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  тройное изоляционное остекление, снаружи многослойное
                  безопасное стекло VSG, посередине – Float, подвергнутое
                  пескоструйной обработке, с прозрачными полосами, внутри
                  многослойное безопасное стекло VSG, прозрачное, коэффициент
                  теплоизоляции UD до 1,0 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 430</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
        <!--<div class="estimation_k_container_box show_more">-->
        <!--<button class="estimation_k_container_btn">Показать еще</button>-->
        <!--</div>-->
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box  col-lg-6 col-12">
            <p class="goods_k_container_box_title">Thermo46 - Творчество и индивидуальность</p>
            <p class="goods_k_container_box_desc">
                Модель входной двери Thermo46 - эффективнейшая теплоизоляция вашего дома с показателями до 1,1 Вт/(м2·К). Серийный надежный замок с 5-кратным функционалом - обеспечивает несомненную защиту, что практически исключает взлом. Для собственного покоя, возможно установить функцию дополнительной защиты от взлома класса RC-2.
                <br>
                <br>
                Когда важно не только качество, но и визуал, тогда Заказчик выбирает двери Thermo46. В модели присутствует классика, но есть из чего выбрать почитателям современности, что представлена в 13 мотивах. В этой модели есть также и индивидуальный заказ - от цветовой палитры и остекления, до боковых элементов и окон верхнего света. Можно создать беспроигрышную эксклюзивную входную дверь, которая подойдет владельцам, избирающим творческую нотку.
                <br>
                <br>
                Входная дверь Thermo46 доступна в Одессе от 26250 грн.
            </p>
          </div>
          <div class="goods_k_container_box  col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/thermo46.jpg') center/contain no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/thermo-46&65/thermo46-2.jpg') center/contain no-repeat"
                ></div>
              </div>
            </div>
            <div class="goods_k_container_box_slider_small slider">
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/thermo46.jpg') center/cover no-repeat"
                ></div>
              </div>
              <div class="slide">
                <div
                  class="small_slider_image"
                  style="background:url('img/thermo-46&65/thermo46-2.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="types_k">
      <div class="wrapper_k">
        <p class="types_k_title">Обзор самых популярных мотивов Thermo46</p>
        <div class="types_k_container row m-0">
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-1.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет серого антрацита RAL 7016 <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальной филенке,
                  коэффициент теплоизоляции UD до 1,1 Вт/ (м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG,
                  изнутри однослойное безопасное стекло ESG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 010</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-2.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Titan Metallic CH 703 <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении,
                  сплошное остекление: тройное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, посередине – Float,
                  подвергнутое пескоструйной обработке, с прозрачными полосами,
                  внутри однослойное безопасное стекло ESG, прозрачное,
                  коэффициент теплоизоляции UD до 1,3 Вт/(м²·K)* Боковой элемент
                  / окно верхнего света: двойное изоляционное остекление,
                  снаружи VSG, внутри VSG, Float с пескоструйной обработкой
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 700</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-3.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении с
                  гофрами, опционально с аппликациями под нержавеющую сталь,
                  коэффициент теплоизоляции UD до 1,1 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи многослойное безопасное стекло VSG, внутри
                  прозрачное VSG
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 015</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-4.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  сплошное остекление: двойное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, внутри однослойное
                  безопасное стекло ESG, Mastercarré, коэффициент теплоизоляции
                  UD до 1,4 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи VSG, внутри ESG, Mastercarré
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 020</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-5.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Golden Oak <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении, с
                  закругленным остеклением: тройное изоляционное остекление,
                  снаружи многослойное безопасное стекло VSG, посередине –
                  Float, подвергнутое пескоструйной обработке, с прозрачными
                  полосами, внутри однослойное безопасное стекло ESG,
                  прозрачное, коэффициент теплоизоляции UD до 1,2 Вт/(м²·K)*
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи VSG, внутри VSG, Float
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 900</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-6.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартний матовый белый цвет RAL 9016, ручка из нержавеющей
                  стали HB 14-2, двойное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, внутри ESG, литое с мелким
                  узором и накладными перекладинами, коэффициент
                  теплопроводности до 1,5 Вт/(м²·K)*
                  <br />
                  <br />
                  Опция: боковой элемент/окно верхнего света с двойным
                  изоляционным остеклением, снаружи многослойное безопасное
                  стекло VSG, внутри ESG, литое с мелким узором.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 400</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-7.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском,
                  с аппликациями под нержавеющую сталь <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении,
                  опционально с аппликациями под нержавеющую сталь, сплошное
                  остекление: двойное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, внутри однослойное
                  безопасное стекло ESG, Mastercarré, коэффициент теплоизоляции
                  UD до 1,4 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 025</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-8.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Decograin Titan Metallic CH 703 с аппликациями под нержавеющую
                  сталь <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении с
                  гофрами, коэффициент теплоизоляции UD до 1,1 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи VSG, внутри VSG, прозрачное <br />
                  <br />
                  * В зависимости от размера двери. Указанные значения для RAM
                  1230 × 2180 мм
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 515</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-9.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 38-2 на стальном заполнении,
                  сплошное остекление: тройное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, посередине – Float,
                  подвергнутое пескоструйной обработке, с прозрачными полосами,
                  внутри однослойное безопасное стекло ESG, прозрачное,
                  коэффициент теплоизоляции UD до 1,2 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 750</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-10.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет: светло-серый RAL 7040 Ручка из
                  нержавеющей стали HB 14-2 на стальном заполнении, сплошное
                  остекление: двойное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, внутри однослойное
                  безопасное стекло ESG, Mastercarré, коэффициент теплоизоляции
                  UD до 1,3 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи VSG, внутри ESG, Mastercarré
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 030</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-11.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартний матовый белый цвет RAL 9016, ручка из нержавеющей
                  стали HB 14-2, двойное изоляционное остекление, снаружи
                  многослойное безопасное стекло VSG, внутри ESG, литое с мелким
                  узором и накладными перекладинами, коэффициент
                  теплопроводности до 1,5 Вт/(м²·K)*
                  <br />
                  <br />
                  Опция: боковой элемент/окно верхнего света с двойным
                  изоляционным остеклением, снаружи многослойное безопасное
                  стекло VSG, внутри ESG, литое с мелким узором.
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 200</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-12.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Предпочтительный цвет кирпичный RAL 8003 Ручка из нержавеющей
                  стали HB 14-2 на стальном заполнении, с 6 полями с кассетами,
                  коэффициент теплоизоляции UD до 1,1 Вт/(м²·K)* <br />
                  <br />
                  Боковой элемент с заполнением панелями: 3 поля с кассетами
                  Боковой элемент / окно верхнего света: двойное изоляционное
                  остекление, снаружи VSG, внутри ESG, стекло Kathedral с мелким
                  узором
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 100</p>
          </div>
          <div
            class="types_k_container_box col-sm-6 col-md-6 col-lg-4 col-xl-3"
          >
            <div
              class="types_k_container_box_image"
              style="background:url('img/thermo-46&65/thermo46/door-13.png') center/contain no-repeat"
            >
              <div class="overlay">
                <p class="overlay_desc">
                  Стандартный цвет: белый RAL 9016 с шелковисто-матовым блеском
                  <br />
                  <br />
                  Ручка из нержавеющей стали HB 14-2 на стальном заполнении, с
                  круглым остеклением Ø 300 мм: двойное изоляционное остекление,
                  снаружи многослойное безопасное стекло VSG, внутри однослойное
                  безопасное стекло ESG, орнаментное стекло 504, коэффициент
                  теплоизоляции UD до 1,2 Вт/(м²·K)*
                </p>
              </div>
            </div>
            <p class="types_k_container_box_desc">Мотив 040</p>
          </div>
        </div>
        <div class="types_k_slider slider"></div>
        <div class="drag_box">
          <p class="drag_box_text">DRAG</p>
          <img src="img/drag.png" alt="" class="drag_box_icon" />
        </div>
        <!--<div class="estimation_k_container_box show_more">-->
        <!--<button class="estimation_k_container_btn">Показать еще</button>-->
        <!--</div>-->
      </div>
    </section>

    <section class="sales_k">
      <div class="sales_k_container">
        <img src="img/sale1.jpg" alt="" class="sales_k_container_image" />
        <div class="sales_k_container_box">
          <p class="sales_k_container_box_title">
            Акционное предложение от Hördis
          </p>
          <p class="sales_k_container_box_desc">
            Дом - это место покоя, безопасности и созданного его владельцем -
            комфорта. Истинное европейское качество ворот и дверей уже в Одессе
            с достойной скидкой до 30%.
          </p>
          <a href="stock.php" class="sales_k_container_box_btn">Посмотреть</a>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Профиль створки не заметен в дверном полотне
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-46&65/advantages/csm_USP_ohneFluegelprofilEingangstuer_d66a80b779.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Чувство эксклюзивности и благородства дизайна входной двери –
                возможно обрести только с Thermo65 и Thermo46, и нет никакой
                разницы, будь то главные парадные или боковые выходы на задний
                двор. Безупречный визуал сплошного дверного полотна с внутренней
                и внешней стороны, внутренняя часть которого содержит профиль
                створки, может удовлетворить любые, даже самые изысканные
                требования к оформлению. У производителя Hörmann всегда
                идеальнейшая гармония внешнего вида, поэтому входные отлично
                компонуют межкомнатным дверям в доме.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Теплоизоляция, что имеет значимость
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-46&65/advantages/csm_USP_hoheWaermedaemmungEingangstuer_3d1535c1d2.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Отменная теплоизоляция дома, с входной дверью Thermo46,
                достигается с помощью показателя UD до 1,1 Вт/(м2·К)*. Больше
                преимуществ в энергоэффективности входной двери Thermo65, у
                которой показатель UD доходит до отметки в 0,87 Вт/(м2·К)*.
                Сохранение тепла, важнейшее преимущество европейского качества.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                “Миссия невыполнима” - концепция против взлома
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-46&65/advantages/csm_UPS_einbruchhemmendeVerEingangstuer_dc34446989.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Входные двери Thermo65 и Thermo46 оснащены серийным замком с
                пяти-системной функцией закрытия. Такой замок дает ощущение
                надежности и защищенности владельцам частной усадьбы. Для тех,
                кто не признает исключений, возможно выбрать входные двери
                Thermo65 или Thermo46 с дополнительной функцией защиты от взлома
                класса RC-2.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Европейский интератив безопасности
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/thermo-46&65/advantages/csm_USP_sichereVerglasungEingangstuer_14317c1e1b.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Немецкий производитель входных дверей Hörmann внедрил высокую
                степень защиты от взлома и непредвиденных ситуаций. Изоляционное
                остекление многослойным стеклом (VSG), толщина которого всего
                8мм. создает абсолютную изоляцию помещения. Для того, чтобы
                разбить VSG стоит приложить немало усилий. Но даже в таком
                случае - осколки не крошаться, а фиксируются на внутренней
                специальной пленке. На сегодня - это мега-двери, где все детали
                продуманы до мелочей.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include "formTemplates/formFooter.php" ?>

     <?php include "footer.php"?>
    
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
