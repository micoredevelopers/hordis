<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Category</title>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <!-- <link rel="stylesheet" href="slick/slick.css"> -->
    <!-- <link rel="stylesheet" href="slick/slick-theme.css"> -->
    <!-- <link rel="stylesheet" href="css/category.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
</head>
<body>

<?php include "header.php"?>


<section class="podcategory_k">
    <div class="wrapper_k">
        <div class="podcategory_k_container">
            <!--<div class="podcategory_k_bac"></div>-->
            <!--<div class="podcategory_k_container_box">-->
                <!--<p class="podcategory_k_container_box_title">Промышленные ворота</p>-->
                <!--<p class="podcategory_k_container_box_desc">-->
                    <!--Благодаря широкому выбору возможных типов направляющих компактные системы ворот годятся для любых-->
                    <!--объектов промышленного строительства. Это обеспечивает надежность планирования в строительстве новых-->
                    <!--объектов и модернизации уже имеющихся площадей. Компания Hörmann предлагает Вам оптимальные решения-->
                    <!--для любых индивидуальных случаев: от ворот с калиткой без порога для удобного прохода до полностью-->
                    <!--остекленных ворот со стойким к царапинам остеклением Duratec.-->
                <!--</p>-->
            <!--</div>-->
            <div class="podcategory_k_container_image">
                <!--<img src="img/category/industrial-gates.jpg" alt="" class="podcategory_k_container_image_item">-->
                <div class="podcategory_k_background" data-back="0"></div>
                <div class="podcategory_k_background" data-back="1"></div>
                <div class="podcategory_k_background" data-back="2"></div>
                <div class="podcategory_k_background" data-back="3"></div>
                <div class="podcategory_k_slider">
                    <div class="slider">
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-12.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="0">Промышленные секционные ворота</p>
                                <img src="img/category/industrial-gates/csm_teaser_industrie_sectionaltor__neu_1920x768_020183bc02.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-13.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="1">Скоростные ворота</p>
                                <img src="img/category/industrial-gates/csm_teaser_schnelllauftore_1920x768_ce5a899eee.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-14.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="2">Рулонные ворота и рулонные решетки</p>
                                <img src="img/category/industrial-gates/csm_teaser_rolltore_rollgitter_neu_1920x768_e1c81b3a55.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-15.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="3">Перегрузочное оборудование</p>
                                <img src="img/category/industrial-gates/teaser_verladetechnik_2100x840.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="podcategory_k_scroll_more">-->
            <!--<p class="podcategory_k_scroll_more_text d-block d-xl-none">Подробнее</p>-->
            <!--<p class="podcategory_k_scroll_more_text d-none d-xl-block">скролл</p>-->
            <!--<img src="img/arrow_adv.png" alt="" class="scroll_arrow">-->
        <!--</div>-->
    </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
<script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>