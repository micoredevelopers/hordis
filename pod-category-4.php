<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Category</title>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <!-- <link rel="stylesheet" href="slick/slick.css"> -->
    <!-- <link rel="stylesheet" href="slick/slick-theme.css"> -->
    <!-- <link rel="stylesheet" href="css/category.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
</head>
<body>


<?php include "header.php"?>

<section class="podcategory_k">
    <div class="wrapper_k">
        <div class="podcategory_k_container">
            <!--<div class="podcategory_k_bac"></div>-->
            <!--<div class="podcategory_k_container_box">-->
                <!--<p class="podcategory_k_container_box_title">Двери</p>-->
                <!--<p class="podcategory_k_container_box_desc">-->
                    <!--Ваша новая входная дверь должна быть чем-то особенным. Она должна вписываться в стиль Вашего дома и-->
                    <!--быть представительной, чтобы стать настоящей визитной карточкой Вашего дома. Но, наряду с этим,-->
                    <!--входная дверь должна также соответствовать высоким требованиям безопасности и способствовать-->
                    <!--сокращению расходов за электроэнергию благодаря высоким показателям теплоизоляции.-->
                <!--</p>-->
            <!--</div>-->
            <div class="podcategory_k_container_image">
                <!--<img src="img/podcategory_k.png" alt="" class="podcategory_k_container_image_item">-->
                <div class="podcategory_k_background" data-back="0"></div>
                <div class="podcategory_k_background" data-back="1"></div>
                <div class="podcategory_k_background" data-back="2"></div>
                <div class="podcategory_k_slider">
                    <div class="slider">
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-16.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="0">Огнестойкая дверь</p>
                                <img src="img/category/doors/csm_Heizung1_Funktionstueren_1000x700_3f204d9555.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
<!--                        <div class="slide">-->
<!--                            <div class="podcategory_k_slider_box">-->
<!--                                <a href="tovar-17.php"></a>-->
<!--                                <p class="podcategory_k_slider_box_title" data-text="1">Звукоизоляционные двери</p>-->
<!--                                <img src="img/category/doors/csm_Nebentuer2_Funktionstueren_1000x700_d6e6647753.jpg" alt="" class="podcategory_k_slider_box_image">-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="slide">
                            <div class="podcategory_k_slider_box">
                                <a href="tovar-18.php"></a>
                                <p class="podcategory_k_slider_box_title" data-text="2">Внутренние двери ZK</p>
                                <img src="img/category/doors/csm_innentuer_zk_1000x700_3a9e92dd60.jpg" alt="" class="podcategory_k_slider_box_image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="podcategory_k_scroll_more">-->
            <!--<p class="podcategory_k_scroll_more_text d-block d-xl-none">Подробнее</p>-->
            <!--<p class="podcategory_k_scroll_more_text d-none d-xl-block">скролл</p>-->
            <!--<img src="img/arrow_adv.png" alt="" class="scroll_arrow">-->
        <!--</div>-->
    </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
<script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>