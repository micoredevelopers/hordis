<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
  <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row m-0">
          <div class="goods_k_container_box order-1 col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Вход всегда начинается с дверей - Гаражные боковые двери.
            </p>
            <p class="goods_k_container_box_desc mb-0">
              В гараже для авто, безусловно нужны ворота, но ведь порой бывает,
              что необходимо - просто войти. Большинство гаражных зданий всегда
              были оснащены дверьми, что отражается и в современности. Немецкая
              марка Hörmann предлагает боковые гаражные двери в идентичном
              дизайне с воротами - секционными, рулонными или
              подъемно-поворотными.
          </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/roll-matic/garage-3-1.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-12">
                <p class="goods_k_container_box_desc">
                    Для гаража, с предусмотренным входом - очень важно подобрать
                    гармонирующую комплектацию. ТМ Hörmann представила боковые
                    гаражные двери в разных расцветках и фактурах, которые максимально
                    точно подойдут к любому варианту ворот, такая исключительная
                    безупречность создает единство в общей конфигурации.
                    <br />
                    <br />
                    Заказать боковые гаражные двери Hörmann в Одессе с монтажом и
                    сервисным обслуживанием, есть возможность на нашем сайте. В
                    разделе (контакты) - рекомендуем выбрать - наиболее оптимальный
                    вариант связи. Или приглашаем посетить ШОУ-РУМ, адрес которого,
                    тоже в контактах.
                </p>
                <div class="box_btns">
                    <a id="go" href="#" class="box_btns_item">Узнать цену</a>
                    <a href="./catalog/VorotaSektcionnye&Bokoviye.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
                </div>
            </div>
        </div>
      </div>
    </section>
    <section class="gallery_section videos_k last_section">
      <div class="wrapper_k">
        <p class="videos_k_title">Галерея</p>
        <div class="gallery_slider slider">
          <div class="slide">
            <div class="image_container">
              <img src="img/roll-matic/gallery-image-2.jpg" alt="" />
            </div>
          </div>
          <div class="slide">
            <div class="image_container">
              <img src="img/roll-matic/gallery-image-2.jpg" alt="" />
            </div>
          </div>
          <div class="slide">
            <div class="image_container">
              <img src="img/roll-matic/gallery-image-2.jpg" alt="" />
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>
     <?php include "footer.php"?>
    <!--modal window-->

   
    <div id="overlay"></div>
      
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
<?php include "formTemplates/successForm.php"?><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
