<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Category</title>
    <!-- <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="css/category.css"> -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/media.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
  </head>
  <body>
    <!--navbar section-->
    <?php include "header.php"?>

    <section class="goods_k">
      <div class="wrapper_k">
        <div class="goods_k_container row align-items-start">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/Industrial/industrial_gates/csm_Verladetechnik_Ladebruecken_1000x700_8397bf946e.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">
              Перегрузочное оборудование
            </p>
            <p class="goods_k_container_box_desc mb-1">
              Перегрузочные ресурсы с движимой аппарелью
              <br />
              Перегрузочные платформы Hörmann – это промышленные объекты,
              которые позволяют максимально облегчить процесс погрузки и
              разгрузки груза, благодаря способности выравнивания платформы
              грузового транспорта и погрузочного моста, что позволяет провести
              работу в чётком горизонтальном положении. Устойчивое стальное
              оборудование, приспособленное к скручиванию, горизонтальной
              платформой выравнивает поперечный крен грузового транспорта, даже
              в том случае, если машина загружена совершенно неравномерно.

            </p>
          </div>
          <div class="col-12">
            <p class="goods_k_container_box_desc">
              Усиленная двухцилиндровая гидравлика и одноцилиндровая аппарель
              приспособлены для любой сложной работы. Конструкция элементарна в
              техническом обслуживании и совершенно безопасна в эксплуатации. В
              случае необходимости, автоматически сработают клапана аварийного
              отключения работы перегрузочного моста, находящиеся в цилиндрах
              механизма. Обладая предохранительными свойствами, клапана
              срабатывают моментально. Этот факт играет огромную роль в том
              случае, если транспорт резко пытается отъехать, а платформа, ещё
              полностью не освобождена. <br />
              Погрузо-разгрузочное оборудование Hörmann в Одессе представлено в
              каталогах, а также частично выставлено в ШОУ-РУМе компании
              “Hördis”. От менеджеров организации, можно получить детальную
              консультацию по всем интересующим вопросам. Компания проводит
              специализированный монтаж оборудования, с последующим сервисным
              обслуживанием.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box order-1 order-lg-0 col-lg-6 col-12">
            <p class="goods_k_container_box_title">Герметизаторы ворот</p>
            <p class="goods_k_container_box_desc m-0">
              Герметизаторы ворот – функциональное решение проблем<br />
              Герметизаторы ворот Hörmann позволяют уберечь товар от воздействия
              природных катаклизмов, не допускают возникновения сквозных
              продуваний, чем обеспечивает работоспособность персонала, избавляя
              их от больничных листов, а также обеспечивают экономию
              электроэнергии. Конструкции позволяют избежать оборудования
              козырьков и пристроек около рампы.
              <br />
              <br />
              Уникальностью тентовых герметизаторов ворот Hörmann является то,
              что они подходят практически к любому грузовому транспорту.
            </p>
          </div>
          <div class="goods_k_container_box order-0 order-lg-1 col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/Industrial/industrial_gates/csm_Verladetechnik_Torabdichtung_1000x700_c5398641dd.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box order-2 col-12">
                <p class="goods_k_container_box_desc">
                    Универсальные со всех сторон тенты, компонованные на оцинкованной
                    рамке, гарантируют и исключают рассоединение конструкции. Все
                    составляющие ворот состоят из отдельных конструкций, которые с
                    лёгкостью монтируются винтами. При необходимости замены
                    установленного элемента, исключены большие материальные затраты и
                    большое количество времени. Комплекс герметизаторов дополняет, для
                    форс-мажорных ситуаций, надувной герметизатор ворот.
                </p>
            </div>
        </div>
      </div>
    </section>

    <section class="goods_k not_first">
      <div class="wrapper_k">
        <div class="goods_k_container row">
          <div class="goods_k_container_box col-lg-6 col-12">
            <div class="goods_k_container_box_slider_big slider">
              <div class="slide">
                <div
                  class="big_slider_image"
                  style="background:url('img/Industrial/industrial_gates/csm_Verladetechnik_Vorsatzschleuse_2017b_1000x700_f0d294fc86.jpg') center/cover no-repeat"
                ></div>
              </div>
            </div>
          </div>
          <div class="goods_k_container_box col-lg-6 col-12">
            <p class="goods_k_container_box_title">Шлюз-тамбуры</p>
            <p class="goods_k_container_box_desc">
              Шлюз – тамбуры всесторонняя альтернативная защита<br />
              Шлюз – тамбур Hörmann позволяет максимально использовать хранилище
              помещения, посредством монтирования конструкции перед ним.<br />
              Тамбур Hörmann – это профессиональный выбор, обеспечение удобных
              условий работы, решение проблемы подъезда к эстакаде.<br />
              Только компания Hörmann гарантирует соответствующую цену и
              качество своего товара.
            </p>
            <div class="box_btns">
              <a id="go" href="#" class="box_btns_item">Узнать цену</a>
              <a href="./catalog_prom/VorotaPeregruz.pdf" target="_blank" class="box_btns_item">Скачать каталог</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="advantage_k">
      <div class="wrapper_k">
        <p class="advantage_k_title">Почему Hörmann?</p>
        <div class="advantage_k_container">
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Прочные перегрузочные мосты
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="active-arrow-tov advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/Industrial/industrial_gates/advantages/csm_USP_robuste_ladebrueckenl_1000x700_79d10b7109.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Работоспособность перегрузочных мостов превзошла все ожидания.
                При невероятной плотной ежедневной нагрузке, ресурсы безотказно
                служат владельцу. Весь секрет в том, что комплектующие
                конструкции произведены из стальных материалов.
                <br />
                <br />
                Немаловажным фактом есть то, что мосты и составляющие -
                сертифицированы - EN 1398.
                <br />
                <br />
                Долгосрочной работе ресурса также способствуют анкеры,
                предназначенные для вентиляции, которые расположенные по углам
                перегрузочного моста, что обеспечивает максимально надёжное
                присоединение к корпусу ангара. При правильной эксплуатации и
                погрузке/разгрузке допустимой нормы веса, перегрузочный мост
                будет надёжным помощником.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Энергосберегающие шлюз-тамбуры
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/Industrial/industrial_gates/advantages/csm_USP_energieeffiziente_vorsat.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Шлюз - тамбуры компании Hörmann обеспечивают перегрузочному
                мосту возможность - расположиться в удобном месте возле
                хранилища. Тамбур гарантирует энергосбережение, посредством
                плотного прилегания промышленных ворот к стенам склада.
                <br />
                При приобретении шлюз - тамбура, покупатель получит
                исчерпывающую информацию, касающуюся возможности выдержки
                тяжести снега и силы ветра. А вот погрешности не идеально
                ровного покрытия на полу, компенсируют и скорректируют подвижные
                опоры. Таким образом при перегрузке особого товара, можно не
                беспокоиться за его повреждения.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Гибкие герметизаторы ворот
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/Industrial/industrial_gates/advantages/csm_USP_flexible_torabdichtungenl_1000x700_c58462e871.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Целевое назначение герметизаторов ворот - это сохранить,
                обезопасить, не дать возможности влияния окружающей среды на
                стандартный и специфический груз, который перемещается с
                грузового автомобиля в морозильные контейнера, ангары, склады,
                промышленные помещения. Ворота настолько гибкие, что позволяют
                чувствовать себя спокойно даже в том случае, если грузовик не
                соответствует габаритам по отношению к платформе.
                <br />
                Вакуумность, создаваемая при соприкосновении со стенами
                настолько прочна, что позволяет сохранять точную температурную
                позицию, характерную для определенного склада.
                <br />
                Сопутствующим позитивом будет то, что тентовые герметизаторы
                настолько, прочные и надежные, что повреждения ресурса просто
                исключается. Рама герметизаторов выполнена из
                высококачественного оцинкованного сплава.
              </p>
            </div>
          </div>
          <div class="advantage_k_container_item">
            <div class="advantage_box">
              <img
                src="img/shield.png"
                alt=""
                class="advantage_k_container_item_img"
              />
              <p class="advantage_k_container_item_text">
                Совместимые системы управления
              </p>
              <img
                src="img/arrow_adv.png"
                alt=""
                class="advantage_k_container_item_arrow"
              />
            </div>
            <div class="advantage_k_container_item_tabul">
              <img
                src="img/industrial/industrial_gates/advantages/csm_USP_kompatible_steuerungssysteme_1000x700_551d3376fa.jpg"
                alt=""
                class="advantage_k_container_item_tabul_image"
              />
              <p class="advantage_k_container_item_tabul_text">
                Компания Hörmann предлагает не только перегрузочные мосты, но и
                блоки управления ворот. Эти товары максимально дополняют друг
                друга. Логичные параллельные разработки компании, обусловлены
                возможностью ресурсам иметь набор кабелей-близнецов.
                <br />
                <br />
                Соответствующим образом, это позволяет разместить блок
                управления перегрузочным мостом, идентично блоку управления
                ворот, а весь комплект управляемых кабелей можно сосредоточить в
                одной удобной позиции.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="videos_k">
      <div class="wrapper_k">
        <p class="videos_k_title">Видеоролики</p>
        <div class="videos_k_container row">
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/E6cv2eDDfF4/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Перегрузочная техника: BBS
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/9wcyNM_WZhU/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Перегрузочная техника. Шлюз-тамбуры: хорошая защита со всех сторон
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/uTf_9zPQ1Xw/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Перегрузочная техника: система DOBO − сначала пристыковка, затем
              открывание дверей
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/4VorV3E-eso/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Перегрузочная техника: оптимально согласованная система
            </p>
          </div>
          <div
            class="videos_k_container_card col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12"
          >
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/YAZtBrvxi9g/?rel=0&showinfo=0&enablejsapi=1"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <p class="videos_k_container_card_text">
              Перегрузочный мост с откидной аппарелью
            </p>
          </div>
        </div>
      </div>
    </section>

    <?php include "formTemplates/formFooter.php" ?>

    <?php include "footer.php"?>
        
    <?php include "formTemplates/formContact.php" ?>
  
    <?php include "formTemplates/formGetPrice.php" ?>
    <?php include "formTemplates/successForm.php"?><!--modal window-->

  
    <div id="overlay"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>
    <script src="js/slick.min.js"></script><script src="js/jquery.maskedinput.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>
